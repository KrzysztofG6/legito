use ProcessorCoordinator
delete from [ProcessorCoordinator].[pca].[ExamProcessingStatus]
delete from [ProcessorCoordinator].[pca].[DuplicateAnswers]
delete from [ProcessorCoordinator].[pca].WikipdiaUrlsWithScore
delete from [ProcessorCoordinator].[pca].[WikipediaProcessingResults]

DELETE FROM [pca].[ProcessingResults] WHERE pca.ProcessingResults.Id is not null

use LegitoBackoffice
DELETE FROM [LegitoBackoffice].[boa].[ProcessingResults] where id is not null
DELETE FROM [LegitoBackoffice].[boa].[DuplicateAnswers] where id is not null
delete from [LegitoBackoffice].[boa].WikipdiaUrlsWithScore
delete from [LegitoBackoffice].[boa].[WikipediaProcessingResults]

go