﻿using Microsoft.AspNetCore.Mvc;

namespace Legito.BackOffice.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/HealthCheck")]
    public class HealthCheckController : Controller
    {
        [HttpGet("Ping")]
        public IActionResult Ping()
        {
            return Ok("Pong");
        }
    }
}