﻿namespace Legito.BackOffice.Api.Controllers
{
    using System;
    using Core.Dto;
    using Core.Services;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Authorize]
    [Produces("application/json")]
    [Route("api/Processing")]
    public class ProcessingController : Controller
    {
        private readonly IProcessingService _processingService;

        public ProcessingController(IProcessingService processingService)
        {
            _processingService = processingService;
        }

        [Authorize(Policy = "Teacher")]
        [HttpPost("Exam")]
        public IActionResult Exam([FromBody]ExamIdDto model)
        {
            _processingService.ProcessExam(model.ExamId);

            return Ok();
        }

        [HttpPost("Result")]
        public IActionResult Result([FromBody] ProcessingResultDto model)
        {
            _processingService.StoreProcessingResults(model);

            return Ok();
        }
    }
}