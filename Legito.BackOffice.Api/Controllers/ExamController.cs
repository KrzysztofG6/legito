﻿namespace Legito.BackOffice.Api.Controllers
{
    using System;
    using Core.Dto;
    using Core.Services;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json;

    [Authorize]
    [Route("api/Exam")]
    public class ExamController : Controller
    {
        private readonly IExamService _examService;

        public ExamController(IExamService examService)
        {
            _examService = examService;
        }

        [Authorize(Policy = "Teacher")]
        [HttpPost("StoreCreatedExam")]
        public IActionResult StoreCreatedExam([FromBody]ExamDto model)
        {
            _examService.StoreCreatedExam(model.Exam);

            return Ok();
        }

        [Authorize(Policy = "Teacher")]
        [HttpGet("ExamTemplates/{teacherId}")]
        public IActionResult ExamTemplates(string teacherId)
        {
            var result = _examService.GetExamTemplatesJsonByTeacherId(teacherId);

            return Ok(result);
        }

        [HttpGet("ActiveExams/{studentId}")]
        public IActionResult ActiveExams(string studentId)
        {
            var result = _examService.GetActiveExamsByStudentId(studentId);

            return Ok(result);
        }

        [Authorize(Policy = "Teacher")]
        [HttpPut("ActivateExam")]
        public IActionResult ActivateExam([FromBody]IdDto model)
        {
            _examService.ActivateExam(model.Id);

            return Ok();
        }

        [Authorize(Policy = "Teacher")]
        [HttpPut("EndExam")]
        public IActionResult EndExam([FromBody] IdDto model)
        {
            _examService.EndExam(model.Id);

            return Ok();
        }

        [HttpPost("StartExam")]
        public IActionResult StartExam([FromBody]StartExamDto model)
        {
            if (model == null)
            {
                return NotFound();
            }

            var result = _examService.StartExamForStudent(model.ExamId, model.StudentId);

            return Ok(result);
        }

        [HttpPost("StoreStudentExam")]
        public ActionResult StoreStudentExam([FromBody] StoreStudentExamDto model)
        {
            _examService.StoreStudentExam(model.Exam);

            return Ok();
        }

        [HttpPost("StartExamTrigerred")]
        public IActionResult StartExamTrigerred([FromBody] StartExamTrigerredDto model)
        {
            _examService.ExamStartTriggered(model.ExamId, model.StudentId);

            return Ok();
        }

        [HttpPost("WasExamTriggeredBefore")]
        public IActionResult WasExamTriggeredBefore([FromBody] WasExamTriggeredBeforeDto model)
        {
            var wasExamTriggeredByStudent = _examService.CheckIfStudentAlreadyTriggeredExam(model.ExamId, model.StudentId);

            if (wasExamTriggeredByStudent)
            {
                return Forbid();
            }

            return Ok();
        }
    }
}