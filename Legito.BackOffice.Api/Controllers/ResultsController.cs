﻿using Microsoft.AspNetCore.Mvc;

namespace Legito.BackOffice.Api.Controllers
{
    using System;
    using Core.Services;
    using Microsoft.AspNetCore.Authorization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    [Authorize(Policy = "Teacher")]
    [Route("api/Results")]
    public class ResultsController : Controller
    {
        private readonly IResultsService _resultsService;

        public ResultsController(IResultsService resultsService)
        {
            _resultsService = resultsService;
        }

        [HttpGet("Processing/{examId}")]
        public IActionResult Processing(string examId)
        {
            var result = _resultsService.GetExamResults(Guid.Parse(examId));

            return Ok(JsonConvert.SerializeObject(result,
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }));
        }
    }
}