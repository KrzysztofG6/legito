﻿using Microsoft.AspNetCore.Mvc;

namespace Legito.BackOffice.Api.Controllers
{
    using System;
    using Core.Dto;
    using Core.Services;
    using Microsoft.AspNetCore.Authorization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    [Authorize]
    [Route("api/Settings")]
    public class SettingsController : Controller
    {
        private readonly ISettingsService _settingsService;

        public SettingsController(ISettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        [Authorize(Policy = "Teacher")]
        [HttpPost]
        [Route("AntiCheat")]
        public IActionResult AddOrUpdateAntiCheat([FromBody] AddOrUpdateAntiCheatDto model)
        {
            _settingsService.AddOrUpdateAntiCheatSettings(model);

            return Ok();
        }

        [HttpGet]
        [Route("AntiCheat/{examId}")]
        public IActionResult GetAntiCheatSettings(string examId)
        {
            var settings = _settingsService.GetSettings(Guid.Parse(examId));

            return Ok(JsonConvert.SerializeObject(settings,
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }));
        }

        [Authorize(Policy = "Teacher")]
        [HttpGet]
        [Route("GetOwnerAntiCheatSettings/{ownerId}")]
        public IActionResult GetOwnerAntiCheatSettings(string ownerId)
        {
            var settings = _settingsService.GetOwnerAntiCheatSettings(Guid.Parse(ownerId));

            return Ok(JsonConvert.SerializeObject(settings,
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }));
        }
    }
}