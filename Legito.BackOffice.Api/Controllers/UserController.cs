﻿using Microsoft.AspNetCore.Mvc;

namespace Legito.BackOffice.Api.Controllers
{
    using System;
    using Core.Dto;
    using Core.Services;
    using Microsoft.AspNetCore.Authorization;

    [Authorize(Policy = "Teacher")]
    [Route("api/User")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("RetriveUserId")]
        public IActionResult RetriveUserId([FromBody]RetriveUserIdDto model)
        {
            if (string.IsNullOrEmpty(model?.UserNameOrLogin))
            {
                return NotFound();
            }

            var result = _userService.GetUserIdByLoginOrEmail(model.UserNameOrLogin);

            if (result == Guid.Empty)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}