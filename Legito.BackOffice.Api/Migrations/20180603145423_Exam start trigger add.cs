﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Legito.BackOffice.Api.Migrations
{
    public partial class Examstarttriggeradd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ExamStartTriggers",
                schema: "boa",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ExamId = table.Column<Guid>(nullable: false),
                    StudentId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamStartTriggers", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExamStartTriggers",
                schema: "boa");
        }
    }
}
