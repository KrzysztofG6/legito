﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Legito.BackOffice.Api.Migrations
{
    public partial class Addedanticheatsettings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AntiCheatSettings",
                schema: "boa",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DeactivateBackButton = table.Column<bool>(nullable: false),
                    DeactivateCopyPasteCut = table.Column<bool>(nullable: false),
                    DeactivateRightClick = table.Column<bool>(nullable: false),
                    OwnerId = table.Column<Guid>(nullable: false),
                    TrackPageFocus = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AntiCheatSettings", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AntiCheatSettings",
                schema: "boa");
        }
    }
}
