﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Legito.BackOffice.Api.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "boa");

            migrationBuilder.CreateTable(
                name: "ProcessingResults",
                schema: "boa",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ExamId = table.Column<Guid>(nullable: false),
                    QuestionId = table.Column<Guid>(nullable: false),
                    StudentId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcessingResults", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DuplicateAnswers",
                schema: "boa",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ProcessingResultId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DuplicateAnswers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DuplicateAnswers_ProcessingResults_ProcessingResultId",
                        column: x => x.ProcessingResultId,
                        principalSchema: "boa",
                        principalTable: "ProcessingResults",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DuplicateAnswers_ProcessingResultId",
                schema: "boa",
                table: "DuplicateAnswers",
                column: "ProcessingResultId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DuplicateAnswers",
                schema: "boa");

            migrationBuilder.DropTable(
                name: "ProcessingResults",
                schema: "boa");
        }
    }
}
