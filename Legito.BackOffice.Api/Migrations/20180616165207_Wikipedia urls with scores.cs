﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Legito.BackOffice.Api.Migrations
{
    public partial class Wikipediaurlswithscores : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SimilarUrlsEncoded",
                schema: "boa",
                table: "WikipediaProcessingResults");

            migrationBuilder.CreateTable(
                name: "WikipdiaUrlsWithScore",
                schema: "boa",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Percentge = table.Column<double>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    WikipediaProcessingResultId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WikipdiaUrlsWithScore", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WikipdiaUrlsWithScore_WikipediaProcessingResults_WikipediaProcessingResultId",
                        column: x => x.WikipediaProcessingResultId,
                        principalSchema: "boa",
                        principalTable: "WikipediaProcessingResults",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WikipdiaUrlsWithScore_WikipediaProcessingResultId",
                schema: "boa",
                table: "WikipdiaUrlsWithScore",
                column: "WikipediaProcessingResultId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WikipdiaUrlsWithScore",
                schema: "boa");

            migrationBuilder.AddColumn<string>(
                name: "SimilarUrlsEncoded",
                schema: "boa",
                table: "WikipediaProcessingResults",
                nullable: true);
        }
    }
}
