﻿namespace Legito.BackOffice.Api.Modules
{
    using Autofac;
    using Domain.Database;
    using KSolution.ServiceDiscovery.Core.Constants;
    using Microsoft.EntityFrameworkCore;

    public class DatabaseModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            var contextOptionsBuilder = new DbContextOptionsBuilder<LegitoContext>();
            contextOptionsBuilder.UseSqlServer("Server=" + ServiceDiscoveryConstants.DockerHostMachineIpAddress + "\\SQL2017;Database=LegitoBackoffice;User Id=Dev;Password=dev12345!;");

            builder.Register(x => new LegitoContext(contextOptionsBuilder.Options))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}