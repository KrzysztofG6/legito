﻿namespace Legito.BackOffice.Api.Modules
{
    using System;
    using System.Net;
    using Autofac;
    using DnsClient;
    using KSolution.ServiceDiscovery.Core.Constants;
    using KSolution.ServiceDiscovery.Services;
    using Services;

    public class ServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register(x =>
                    new LookupClient(IPAddress.Parse(ServiceDiscoveryConstants.DockerHostMachineIpAddress), 8600)
                    {
                        EnableAuditTrail = false,
                        UseCache = true,
                        MinimumCacheTimeout = TimeSpan.FromSeconds(1)
                    })
                .As<IDnsQuery>()
                .SingleInstance();

            builder.RegisterType<ConsulService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<ProcessingService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<TokenProviderService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<ExamService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<ResultsService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<SettingsService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}