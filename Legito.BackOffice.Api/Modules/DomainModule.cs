﻿namespace Legito.BackOffice.Api.Modules
{
    using System.IO;
    using System.Net.Http;
    using Autofac;
    using AutoMapper;
    using Domain.Mapper;
    using Microsoft.Extensions.Configuration;
    using Serilog;

    public class DomainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            builder.RegisterType<HttpClient>()
                .AsSelf()
                .SingleInstance();

            builder.Register(x => new MapperFactory().Create())
                .As<IMapper>()
                .SingleInstance();

            builder.Register(x => new LoggerConfiguration()
                    .MinimumLevel.Warning()
                    .ReadFrom.Configuration(configuration).CreateLogger())
                .As<ILogger>()
                .InstancePerLifetimeScope();
        }
    }
}