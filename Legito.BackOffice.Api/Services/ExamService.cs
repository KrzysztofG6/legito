﻿namespace Legito.BackOffice.Api.Services
{
    using System;
    using Core.Dto;
    using Core.Services;
    using Domain.Commands;
    using Domain.Queries;
    using KSolution.Cqrs.Core;

    public class ExamService : IExamService
    {
        private readonly IMediator _mediator;

        public ExamService(IMediator mediator)
        {
            _mediator = mediator;
        }

        public void StoreCreatedExam(string exam)
        {
            _mediator.Send(new StoreCreatedExamCommand(exam));
        }

        public string GetExamTemplatesJsonByTeacherId(string id)
        {
            return _mediator.Fetch<GetExamTemplatesByTeacherIdQuery, string>(new GetExamTemplatesByTeacherIdQuery(id));
        }

        public ActiveExamDto[] GetActiveExamsByStudentId(string studentId)
        {
            return _mediator.Fetch<GetActiveExamsQuery, ActiveExamDto[]>(new GetActiveExamsQuery(studentId));
        }

        public void ActivateExam(string examId)
        {
            _mediator.Send(new ActivateExamTemlateByIdCommand(examId));
        }

        public void EndExam(string id)
        {
            _mediator.Send(new EndExamTemplateByIdCommand(id));
        }

        public string StartExamForStudent(string examId, string studentId)
        {
            return _mediator.Fetch<GetExamForStudentQuery, string>(new GetExamForStudentQuery(examId, studentId));
        }

        public void StoreStudentExam(string exam)
        {
            _mediator.Send(new StoreStudentExamCommand(exam));
        }

        public void ExamStartTriggered(Guid examId, Guid studentId)
        {
            _mediator.Send(new StoreExamTriggerCommand(examId, studentId));
        }

        public bool CheckIfStudentAlreadyTriggeredExam(Guid examId, Guid studentId)
        {
            return _mediator.Fetch<CheckIfStudentAlreadyTriggeredExamQuery, bool>(new CheckIfStudentAlreadyTriggeredExamQuery(examId, studentId));
        }
    }
}