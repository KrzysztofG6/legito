﻿namespace Legito.BackOffice.Api.Services
{
    using System;
    using Core.Services;
    using Domain.Queries;
    using KSolution.Cqrs.Core;

    public class UserService : IUserService
    {
        private readonly IMediator _mediator;

        public UserService(IMediator mediator)
        {
            _mediator = mediator;
        }

        public Guid GetUserIdByLoginOrEmail(string loginOrEmail)
        {
            var result = _mediator.Fetch<GetUserIdByLoginOrEmailQuery, string>(new GetUserIdByLoginOrEmailQuery(loginOrEmail));

            return !string.IsNullOrEmpty(result) ? Guid.Parse(result) : Guid.Empty;
        }
    }
}