﻿namespace Legito.BackOffice.Api.Services
{
    using AutoMapper;
    using Core.Dto;
    using Core.Entities;
    using Core.Services;
    using Domain.Commands;
    using KSolution.Cqrs.Core;
    using System;
    using Domain.Queries;

    public class SettingsService : ISettingsService
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public SettingsService(IMediator mediator,
            IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        public void AddOrUpdateAntiCheatSettings(AddOrUpdateAntiCheatDto model)
        {
            _mediator.Send(new AddOrUpdateAntiCheatSettingsCommand(_mapper.Map<AntiCheatSettings>(model)));
        }

        public GetAntiCheatSettingsDto GetSettings(Guid examId)
        {
            var settings = _mediator.Fetch<GetAntiCheatSettingByExamIdQuery, AntiCheatSettings>(new GetAntiCheatSettingByExamIdQuery(examId));

            return _mapper.Map<GetAntiCheatSettingsDto>(settings);
        }

        public GetOwnerAntiCheatSettingsDto GetOwnerAntiCheatSettings(Guid ownerId)
        {
            var settings = _mediator.Fetch<GetOwnerAntiCheatSettingsQuery, AntiCheatSettings>(new GetOwnerAntiCheatSettingsQuery(ownerId));

            return _mapper.Map<GetOwnerAntiCheatSettingsDto>(settings);
        }
    }
}