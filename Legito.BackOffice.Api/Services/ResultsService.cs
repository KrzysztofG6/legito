﻿namespace Legito.BackOffice.Api.Services
{
    using System;
    using Core.Dto;
    using Core.Services;
    using Domain.Queries;
    using KSolution.Cqrs.Core;

    public class ResultsService : IResultsService
    {
        private readonly IMediator _mediator;

        public ResultsService(IMediator mediator)
        {
            _mediator = mediator;
        }

        public ResultsWithProcessingDto GetExamResults(Guid examId)
        {
            return _mediator.Fetch<GetExamResultsWithProcessingQuery, ResultsWithProcessingDto>(new GetExamResultsWithProcessingQuery(examId));
        }
    }
}