﻿namespace Legito.BackOffice.Api.Services
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Core.Dto;
    using Core.Entities;
    using Core.Services;
    using Domain.Commands;
    using KSolution.Cqrs.Core;

    public class ProcessingService : IProcessingService
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public ProcessingService(
            IMediator mediator, 
            IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        public void ProcessExam(Guid examId)
        {
            _mediator.Send(new ProcessExamByIdCommand(examId));
        }

        public void StoreProcessingResults(ProcessingResultDto results)
        {
            _mediator.Send(new StoreProcessedExamResultsCommand(_mapper.Map<List<ProcessingResult>>(results.TextComparerResults),
                _mapper.Map<List<WikipediaProcessingResult>>(results.WikipediaResults)));
        }
    }
}