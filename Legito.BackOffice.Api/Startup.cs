﻿namespace Legito.BackOffice.Api
{
    using System;
    using Autofac;
    using Autofac.Extensions.DependencyInjection;
    using Domain.Database;
    using KSolution.Cqrs.Modules;
    using KSolution.ServiceDiscovery.Core.Enum;
    using KSolution.ServiceDiscovery.Core.Extensions;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Middleware;
    using Modules;
    using Newtonsoft.Json.Serialization;
    using Serilog;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddJsonOptions(opt => opt.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver());

            services.AddTransient<ILogger>(x => new LoggerConfiguration()
                .MinimumLevel.Warning()
                .ReadFrom.Configuration(Configuration).CreateLogger());

            services.AddDbContext<LegitoContext>(options =>
                options.UseSqlServer(this.Configuration.GetConnectionString("LegitoBackOfficeDb")));

            services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));

            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = "http://localhost:5000";
                    options.RequireHttpsMetadata = false;

                    options.ApiName = ServicesLookup.BackofficeApi.Description();
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("Teacher", policy => policy.RequireClaim("isTeacher"));
            });

            services.AddCors(options =>
            {
                options.AddPolicy("AngularApp", policy =>
                {
                    policy.WithOrigins("http://localhost:4200")
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });

            var containerBuilder = new ContainerBuilder();
            containerBuilder.Populate(services);

            containerBuilder.RegisterModule<CqrsModule>();
            containerBuilder.RegisterModule<DatabaseModule>();
            containerBuilder.RegisterModule<DomainModule>();
            containerBuilder.RegisterModule<ServicesModule>();
            
            var container = containerBuilder.Build();
            return new AutofacServiceProvider(container);
        }
        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseMiddleware(typeof(ErrorHandlingMiddleware));

            app.UseAuthentication();
            app.UseConsulServiceRegistration();
            app.UseCors("AngularApp");
            app.UseMvc();
        }
    }
}
