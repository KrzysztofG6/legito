﻿namespace Legito.BackOffice.Api.Domain.Commands
{
    using KSolution.Cqrs.Core.Markers;

    public class StoreCreatedExamCommand : ICommand
    {
        public StoreCreatedExamCommand(string exam)
        {
            this.Exam = exam;
        }

        public string Exam { get; }
    }
}