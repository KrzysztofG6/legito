﻿namespace Legito.BackOffice.Api.Domain.Commands
{
    using System;
    using KSolution.Cqrs.Core.Markers;
    public class ProcessExamByIdCommand : ICommand
    {
        public ProcessExamByIdCommand(Guid examId)
        {
            ExamId = examId;
        }

        public Guid ExamId { get; private set; }
    }
}