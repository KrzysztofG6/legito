﻿namespace Legito.BackOffice.Api.Domain.Commands
{
    using System;
    using KSolution.Cqrs.Core.Markers;

    public class StoreExamTriggerCommand : ICommand
    {
        public StoreExamTriggerCommand(Guid examId,
            Guid studentId)
        {
            this.ExamId = examId;
            this.StudentId = studentId;
        }

        public Guid ExamId { get; }

        public Guid StudentId { get; }
    }
}