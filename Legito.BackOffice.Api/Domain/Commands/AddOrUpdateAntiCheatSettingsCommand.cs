﻿namespace Legito.BackOffice.Api.Domain.Commands
{
    using Core.Entities;
    using KSolution.Cqrs.Core.Markers;

    public class AddOrUpdateAntiCheatSettingsCommand : ICommand
    {
        public AddOrUpdateAntiCheatSettingsCommand(AntiCheatSettings antiCheatSettings)
        {
            this.AntiCheatSettings = antiCheatSettings;
        }

        public AntiCheatSettings AntiCheatSettings { get; }
    }
}