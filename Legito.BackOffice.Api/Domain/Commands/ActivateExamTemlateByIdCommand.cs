﻿namespace Legito.BackOffice.Api.Domain.Commands
{
    using KSolution.Cqrs.Core.Markers;

    public class ActivateExamTemlateByIdCommand : ICommand
    {
        public ActivateExamTemlateByIdCommand(string id)
        {
            this.Id = id;
        }

        public string Id { get; }
    }
}