﻿namespace Legito.BackOffice.Api.Domain.Commands
{
    using KSolution.Cqrs.Core.Markers;

    public class StoreStudentExamCommand : ICommand
    {
        public StoreStudentExamCommand(string exam)
        {
            this.Exam = exam;
        }

        public string Exam { get; }
    }
}