﻿namespace Legito.BackOffice.Api.Domain.Commands
{
    using System.Collections.Generic;
    using Core.Entities;
    using KSolution.Cqrs.Core.Markers;

    public class StoreProcessedExamResultsCommand : ICommand
    {
        public StoreProcessedExamResultsCommand(List<ProcessingResult> textComparerResults,
            List<WikipediaProcessingResult> wikipediaProcessingResults)
        {
            this.TextComparerResults = textComparerResults;
            this.WikipediaProcessingResults = wikipediaProcessingResults;
        }

        public List<ProcessingResult> TextComparerResults { get; }

        public List<WikipediaProcessingResult> WikipediaProcessingResults { get; }
    }
}