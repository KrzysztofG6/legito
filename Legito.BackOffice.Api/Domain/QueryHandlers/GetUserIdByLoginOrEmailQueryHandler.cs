﻿namespace Legito.BackOffice.Api.Domain.QueryHandlers
{
    using System.Net.Http;
    using System.Runtime.InteropServices;
    using System.Text;
    using KSolution.Cqrs.Core;
    using KSolution.ServiceDiscovery.Core.Enum;
    using KSolution.ServiceDiscovery.Core.Services;
    using Newtonsoft.Json;
    using Queries;

    public class GetUserIdByLoginOrEmailQueryHandler : IQueryHandler<GetUserIdByLoginOrEmailQuery, string>
    {
        private readonly HttpClient _httpClient;
        private readonly IConsulService _consulService;
        private readonly ITokenProviderService _tokenProviderService;

        public GetUserIdByLoginOrEmailQueryHandler(HttpClient httpClient,
            IConsulService consulService,
            ITokenProviderService tokenProviderService)
        {
            _httpClient = httpClient;
            _consulService = consulService;
            _tokenProviderService = tokenProviderService;
        }

        public string Handle(GetUserIdByLoginOrEmailQuery query)
        {
            var postContent = JsonConvert.SerializeObject(query);
            
            _httpClient.SetBearerToken(_tokenProviderService.GetTokenForApi(ServicesLookup.BackofficeApi, ServicesLookup.AuthorizationServer).Result);
            var response = _httpClient.PostAsync($"http://{_consulService.GetServiceAddress(ServicesLookup.AuthorizationServer)}/api/User/RetriveId", new StringContent(postContent, Encoding.UTF8, "application/json")).Result;
            
            if (!response.IsSuccessStatusCode)
            {
                return string.Empty;
            }

            return response.Content.ReadAsStringAsync().Result;
        }
    }
}