﻿namespace Legito.BackOffice.Api.Domain.QueryHandlers
{
    using System;
    using System.Collections.Generic;
    using Core.Database;
    using Core.Dto;
    using KSolution.Cqrs.Core;
    using Queries;
    using System.Linq;
    using AutoMapper;
    using Core.Entities;
    using Core.Models;
    using Microsoft.EntityFrameworkCore;

    public class GetExamResultsWithProcessingQueryHandler : IQueryHandler<GetExamResultsWithProcessingQuery, ResultsWithProcessingDto>
    {
        private readonly ILegitoContext _legitoContext;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public GetExamResultsWithProcessingQueryHandler(ILegitoContext legitoContext,
            IMediator mediator,
            IMapper mapper)
        {
            _legitoContext = legitoContext;
            _mediator = mediator;
            _mapper = mapper;
        }

        public ResultsWithProcessingDto Handle(GetExamResultsWithProcessingQuery query)
        {
            var textComparerResults = _legitoContext.ProcessingResults.Where(x => x.ExamId == query.ExamId).Include(x => x.DuplicateAnswers).ToList();
            var wikipediaScanResults = _legitoContext.WikipediaProcessingResults.Where(x => x.ExamId == query.ExamId).Include(x => x.UrlsWithScore);
            var studentExams = _mediator.Fetch<GetExamsByExamIdQuery, ExamSerializedDto>(new GetExamsByExamIdQuery(query.ExamId));
            var studentNames = _mediator.Fetch<GetUserNamesByIdsQuery, List<UserIdAndNameDto>>(
                new GetUserNamesByIdsQuery(studentExams.Answers.Select(x => x.StudentId).Distinct().ToArray()));
            var questions = _mediator.Fetch<GetExamQuestionsByExamTemplateIdQuery, List<QuestionDto>>(
                    new GetExamQuestionsByExamTemplateIdQuery(query.ExamId));

            var groupedAnswers = new List<List<AnswerSerializedDto>>();
            this.GroupAnswersByStudentId(studentExams, groupedAnswers);

            var answersMapped = _mapper.Map<List<List<AnswerWithWikipediaUrl>>>(groupedAnswers);

            FillStudentNamesAndQuestionsIntoModel(answersMapped, studentNames, questions);

            var textComparerResultsMapped = MapTextComparerResults(textComparerResults, answersMapped, studentNames, questions);

            this.FillWikipediaUrls(answersMapped, wikipediaScanResults);

            var closedQuestionsResults = new List<ClosedQuestionsScoreDto>();

            this.VerifyClosedQuestionsAnswers(answersMapped, questions, closedQuestionsResults);

            return new ResultsWithProcessingDto
            {
                AnswersWithWikiScan = _mapper.Map<List<List<AnswerWithWikipediaUrlDto>>>(answersMapped),
                TextComparerResults = _mapper.Map<List<TextComparerProcessingResultDto>>(textComparerResultsMapped),
                ClosedQuestionsResults = closedQuestionsResults
            };
        }

        private void VerifyClosedQuestionsAnswers(List<List<AnswerWithWikipediaUrl>> answersMapped, List<QuestionDto> questions, List<ClosedQuestionsScoreDto> closedQuestionsResults)
        {
            foreach (var group in answersMapped)
            {
                var score = 0;
                var outOf = 0;

                foreach (var answer in @group)
                {
                    if (answer.Type != 2) continue;

                    outOf += 1;

                    foreach (var question in questions)
                    {
                        var questionTemplate = question.PossibleAnswers.FirstOrDefault(x => x.Answer == answer.StudentAnswer);

                        if (questionTemplate == null) continue;

                        if (questionTemplate.IsCorrect)
                        {
                            score += 1;
                        }
                    }
                }

                closedQuestionsResults.Add(new ClosedQuestionsScoreDto
                {
                    Score = score,
                    OutOf = outOf,
                    StudentName = @group.First().StudentName
                });
            }
        }

        private List<TextComparerProcessingResult> MapTextComparerResults(List<ProcessingResult> textComparerResults,
            List<List<AnswerWithWikipediaUrl>> answersMapped,
            List<UserIdAndNameDto> studentNames, List<QuestionDto> questions)
        {
            var textComparerResultsMapped = new List<TextComparerProcessingResult>();

            foreach (var result in textComparerResults)
            {
                var cheatingStudentsNames = new List<string>();

                foreach (var duplicateAnswer in result.DuplicateAnswers)
                {
                    foreach (var group in answersMapped)
                    {
                        var studentName = group.Where(x => x.Id == duplicateAnswer.Id).Select(x => x.StudentName).FirstOrDefault();

                        if (!string.IsNullOrEmpty(studentName))
                        {
                            cheatingStudentsNames.Add(studentName);
                            break;
                        }
                    }
                }

                textComparerResultsMapped.Add(new TextComparerProcessingResult
                {
                    SearchAgainstStudentId = result.StudentId,
                    SearchAgainstStudentName = studentNames.Where(x => x.Id == result.StudentId).Select(x => x.Name).Single(),
                    QuestionId = result.QuestionId,
                    Question = questions.Where(x => Guid.Parse(x.Id) == result.QuestionId).Select(x => x.Text).Single(),
                    StudentNames = cheatingStudentsNames
                });
            }

            return textComparerResultsMapped;
        }

        private void FillWikipediaUrls(List<List<AnswerWithWikipediaUrl>> answersMapped, IQueryable<WikipediaProcessingResult> wikipediaScanResults)
        {
            foreach (var mappedGroup in answersMapped)
            {
                foreach (var answer in mappedGroup)
                {
                    var wikiScanResult = wikipediaScanResults.Where(x => x.AnswerId == answer.Id).ToList();

                    if (!wikipediaScanResults.Any()) continue;

                    var wikiScanAggregated = new List<WikipediaUrlDto>();

                    foreach (var scanResult in wikiScanResult)
                    {
                        wikiScanAggregated.AddRange(_mapper.Map<List<WikipediaUrlDto>>(scanResult.UrlsWithScore));
                    }

                    answer.WikipediaUrls.AddRange(wikiScanAggregated);
                }
            }
        }

        private void FillStudentNamesAndQuestionsIntoModel(List<List<AnswerWithWikipediaUrl>> answersWithWikipediaUrls, List<UserIdAndNameDto> studentNames, List<QuestionDto> questions)
        {
            foreach (var group in answersWithWikipediaUrls)
            {
                foreach (var answer in @group)
                {
                    answer.StudentName = studentNames.Where(x => x.Id == answer.StudentId).Select(x => x.Name).Single();
                    answer.Question = questions.Where(x => Guid.Parse(x.Id) == answer.QuestionId).Select(x => x.Text).Single();
                }
            }
        }

        private void GroupAnswersByStudentId(ExamSerializedDto studentExams, List<List<AnswerSerializedDto>> groupedAnswers)
        {
            var studentIds = studentExams.Answers.Select(x => x.StudentId).Distinct();

            foreach (var studentId in studentIds)
            {
                groupedAnswers.Add(new List<AnswerSerializedDto>(studentExams.Answers.Where(x => x.StudentId == studentId)));
            }
        }
    }
}