﻿namespace Legito.BackOffice.Api.Domain.QueryHandlers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using Core.Database;
    using Core.Dto;
    using Core.Entities;
    using KSolution.Cqrs.Core;
    using KSolution.ServiceDiscovery.Core.Enum;
    using KSolution.ServiceDiscovery.Core.Services;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;
    using Queries;

    public class GetActiveExamsQueryHandler : IQueryHandler<GetActiveExamsQuery, ActiveExamDto[]>
    {
        private readonly HttpClient _httpClient;
        private readonly IConsulService _consulService;
        private readonly ITokenProviderService _tokenProviderService;
        private readonly ILegitoContext _context;

        public GetActiveExamsQueryHandler(HttpClient httpClient,
            IConsulService consulService,
            ITokenProviderService tokenProviderService,
            ILegitoContext context)
        {
            _httpClient = httpClient;
            _consulService = consulService;
            _tokenProviderService = tokenProviderService;
            _context = context;
        }

        public ActiveExamDto[] Handle(GetActiveExamsQuery query)
        {
            _httpClient.SetBearerToken(_tokenProviderService.GetTokenForApi(ServicesLookup.BackofficeApi, ServicesLookup.ClientApi).Result);
            var response = _httpClient.GetAsync($"http://{_consulService.GetServiceAddress(ServicesLookup.ClientApi)}/api/Exam/ActiveExams/{query.StudentId}").Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"Couldnt get active exams for student id: {query.StudentId} Response: {response}");
            }

            var content = response.Content.ReadAsStringAsync().Result;
            var deserialized = JsonConvert.DeserializeObject<List<ActiveExamDto>>(content);

            var triggeredExams = _context.ExamStartTriggers.Where(x => x.StudentId == Guid.Parse(query.StudentId)).ToList();

            var filtered = new List<ActiveExamDto>();

            this.FilterTriggeredExams(query, deserialized, filtered, triggeredExams);

            return filtered.ToArray();
        }

        private void FilterTriggeredExams(GetActiveExamsQuery query, List<ActiveExamDto> deserialized, List<ActiveExamDto> filtered, List<ExamStartTriggered> triggeredExams)
        {
            foreach (var activeExam in deserialized)
            {
                if (activeExam.IsCompleted)
                {
                    filtered.Add(activeExam);
                    continue;
                }

                var wasTriggered = triggeredExams.Any(triggered =>
                    (Guid.Parse(activeExam.Tag) == triggered.ExamId && triggered.StudentId == Guid.Parse(query.StudentId)));

                if (wasTriggered) continue;

                filtered.Add(activeExam);
            }
        }
    }
}