﻿namespace Legito.BackOffice.Api.Domain.QueryHandlers
{
    using System;
    using System.Linq;
    using Core.Database;
    using Core.Entities;
    using KSolution.Cqrs.Core;
    using Queries;

    public class GetAntiCheatSettingByExamIdQueryHandler : IQueryHandler<GetAntiCheatSettingByExamIdQuery, AntiCheatSettings>
    {
        private readonly ILegitoContext _context;
        private readonly IMediator _mediator;

        public GetAntiCheatSettingByExamIdQueryHandler(ILegitoContext context,
            IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public AntiCheatSettings Handle(GetAntiCheatSettingByExamIdQuery query)
        {
            var ownerId = _mediator.Fetch<GetExamOwnerIdByExamIdQuery, Guid>(new GetExamOwnerIdByExamIdQuery(query.ExamId));

            var defaultResult = new AntiCheatSettings
            {
                DeactivateBackButton = true,
                DeactivateCopyPasteCut = true,
                DeactivateRightClick = true,
                TrackPageFocus = true
            };

            if (ownerId == Guid.Empty)
            {
                return defaultResult;
            }

            return _context.AntiCheatSettings.FirstOrDefault(x => x.OwnerId == ownerId) ?? defaultResult;
        }
    }
}