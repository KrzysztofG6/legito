﻿namespace Legito.BackOffice.Api.Domain.QueryHandlers
{
    using System;
    using System.Net.Http;
    using Core.Dto;
    using KSolution.Cqrs.Core;
    using KSolution.ServiceDiscovery.Core.Enum;
    using KSolution.ServiceDiscovery.Core.Services;
    using Newtonsoft.Json;
    using Queries;

    public class GetExamOwnerIdByExamIdQueryHandler : IQueryHandler<GetExamOwnerIdByExamIdQuery, Guid>
    {
        private readonly HttpClient _httpClient;
        private readonly IConsulService _consulService;
        private readonly ITokenProviderService _tokenProviderService;

        public GetExamOwnerIdByExamIdQueryHandler(HttpClient httpClient,
            IConsulService consulService,
            ITokenProviderService tokenProviderService)
        {
            _httpClient = httpClient;
            _consulService = consulService;
            _tokenProviderService = tokenProviderService;
        }

        public Guid Handle(GetExamOwnerIdByExamIdQuery query)
        {
            _httpClient.SetBearerToken(_tokenProviderService.GetTokenForApi(ServicesLookup.BackofficeApi, ServicesLookup.ClientApi).Result);
            var response = _httpClient.GetAsync($"http://{_consulService.GetServiceAddress(ServicesLookup.ClientApi)}/api/Exam/GetExamOwnerId/{query.ExamId}").Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"Couldnt get owner id for exam: {query.ExamId} Response: {response}");
            }

            var result = response.Content.ReadAsStringAsync().Result;

            return JsonConvert.DeserializeObject<Guid>(result);
        }
    }
}