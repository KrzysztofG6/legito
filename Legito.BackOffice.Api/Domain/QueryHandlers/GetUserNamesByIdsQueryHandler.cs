﻿namespace Legito.BackOffice.Api.Domain.QueryHandlers
{
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;
    using Core.Dto;
    using KSolution.Cqrs.Core;
    using KSolution.ServiceDiscovery.Core.Enum;
    using KSolution.ServiceDiscovery.Core.Services;
    using Newtonsoft.Json;
    using Queries;

    public class GetUserNamesByIdsQueryHandler : IQueryHandler<GetUserNamesByIdsQuery, List<UserIdAndNameDto>>
    {
        private readonly HttpClient _httpClient;
        private readonly IConsulService _consulService;
        private readonly ITokenProviderService _tokenProviderService;

        public GetUserNamesByIdsQueryHandler(HttpClient httpClient,
            IConsulService consulService,
            ITokenProviderService tokenProviderService)
        {
            _httpClient = httpClient;
            _consulService = consulService;
            _tokenProviderService = tokenProviderService;
        }

        public List<UserIdAndNameDto> Handle(GetUserNamesByIdsQuery query)
        {
            _httpClient.SetBearerToken(_tokenProviderService.GetTokenForApi(ServicesLookup.BackofficeApi, ServicesLookup.AuthorizationServer).Result);
            var response = _httpClient.PostAsync($"http://{_consulService.GetServiceAddress(ServicesLookup.AuthorizationServer)}/api/User/RetrieveLoginsAndIds/", new StringContent(JsonConvert.SerializeObject(query.Ids), Encoding.UTF8, "application/json")).Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"Couldnt get names by login Response: {response}");
            }

            var result = response.Content.ReadAsStringAsync().Result;

            return JsonConvert.DeserializeObject<List<UserIdAndNameDto>>(result);
        }
    }
}