﻿namespace Legito.BackOffice.Api.Domain.QueryHandlers
{
    using System.Linq;
    using Core.Database;
    using Core.Entities;
    using KSolution.Cqrs.Core;
    using Queries;

    public class GetOwnerAntiCheatSettingsQueryHandler : IQueryHandler<GetOwnerAntiCheatSettingsQuery, AntiCheatSettings>
    {
        private readonly ILegitoContext _context;

        public GetOwnerAntiCheatSettingsQueryHandler(ILegitoContext context)
        {
            _context = context;
        }

        public AntiCheatSettings Handle(GetOwnerAntiCheatSettingsQuery query)
        {
            var result = _context.AntiCheatSettings.FirstOrDefault(x => x.OwnerId == query.OwnerId);

            if (result == null)
            {
                return new AntiCheatSettings
                {
                    OwnerId = query.OwnerId,
                    DeactivateBackButton = false,
                    DeactivateCopyPasteCut = false,
                    DeactivateRightClick = false,
                    TrackPageFocus = false
                };
            }

            return result;
        }
    }
}