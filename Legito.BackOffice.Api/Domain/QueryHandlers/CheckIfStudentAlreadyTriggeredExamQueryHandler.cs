﻿namespace Legito.BackOffice.Api.Domain.QueryHandlers
{
    using System.Linq;
    using Core.Database;
    using KSolution.Cqrs.Core;
    using Queries;

    public class CheckIfStudentAlreadyTriggeredExamQueryHandler : IQueryHandler<CheckIfStudentAlreadyTriggeredExamQuery, bool>
    {
        private readonly ILegitoContext _context;

        public CheckIfStudentAlreadyTriggeredExamQueryHandler(ILegitoContext context)
        {
            _context = context;
        }

        public bool Handle(CheckIfStudentAlreadyTriggeredExamQuery query)
        {
            return _context.ExamStartTriggers.Any(x => x.ExamId == query.ExamId && x.StudentId == query.StudentId);
        }
    }
}