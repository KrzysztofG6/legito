﻿namespace Legito.BackOffice.Api.Domain.QueryHandlers
{
    using System;
    using System.Net.Http;
    using System.Text;
    using KSolution.Cqrs.Core;
    using KSolution.ServiceDiscovery.Core.Enum;
    using KSolution.ServiceDiscovery.Core.Services;
    using Queries;

    public class GetExamTemplatesByTeacherIdQueryHandler : IQueryHandler<GetExamTemplatesByTeacherIdQuery, string>
    {
        private readonly HttpClient _httpClient;
        private readonly IConsulService _consulService;
        private readonly ITokenProviderService _tokenProviderService;

        public GetExamTemplatesByTeacherIdQueryHandler(HttpClient httpClient, 
            IConsulService consulService, 
            ITokenProviderService tokenProviderService)
        {
            _httpClient = httpClient;
            _consulService = consulService;
            _tokenProviderService = tokenProviderService;
        }

        public string Handle(GetExamTemplatesByTeacherIdQuery query)
        {
            _httpClient.SetBearerToken(_tokenProviderService.GetTokenForApi(ServicesLookup.BackofficeApi, ServicesLookup.ClientApi).Result);
            var response = _httpClient.GetAsync($"http://{_consulService.GetServiceAddress(ServicesLookup.ClientApi)}/api/Exam/ExamTemplates/{query.TeacherId}").Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"Couldnt get exam templates for teacher id: {query.TeacherId} Response: {response}");
            }

            return response.Content.ReadAsStringAsync().Result;
        }
    }
}