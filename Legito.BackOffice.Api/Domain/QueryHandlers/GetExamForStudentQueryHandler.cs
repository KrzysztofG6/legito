﻿namespace Legito.BackOffice.Api.Domain.QueryHandlers
{
    using System.Net.Http;
    using System.Text;
    using KSolution.Cqrs.Core;
    using KSolution.ServiceDiscovery.Core.Enum;
    using KSolution.ServiceDiscovery.Core.Services;
    using Newtonsoft.Json;
    using Queries;

    public class GetExamForStudentQueryHandler : IQueryHandler<GetExamForStudentQuery, string>
    {
        private readonly HttpClient _httpClient;
        private readonly IConsulService _consulService;
        private readonly ITokenProviderService _tokenProviderService;

        public GetExamForStudentQueryHandler(HttpClient httpClient,
            IConsulService consulService, 
            ITokenProviderService tokenProviderService)
        {
            _httpClient = httpClient;
            _consulService = consulService;
            _tokenProviderService = tokenProviderService;
        }

        public string Handle(GetExamForStudentQuery query)
        {
            var postContent = JsonConvert.SerializeObject(query);

            _httpClient.SetBearerToken(_tokenProviderService.GetTokenForApi(ServicesLookup.BackofficeApi, ServicesLookup.ClientApi).Result);
            var response = _httpClient.PostAsync($"http://{_consulService.GetServiceAddress(ServicesLookup.ClientApi)}/api/Exam/StartExam/", new StringContent(postContent, Encoding.UTF8, "application/json")).Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"Couldnt get and start exam id: {query.ExamId} student id: {query.StudentId} Response: {response}");
            }

            return response.Content.ReadAsStringAsync().Result;
        }
    }
}