﻿namespace Legito.BackOffice.Api.Domain.QueryHandlers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using Core.Dto;
    using KSolution.Cqrs.Core;
    using KSolution.ServiceDiscovery.Core.Enum;
    using KSolution.ServiceDiscovery.Core.Services;
    using Newtonsoft.Json;
    using Queries;

    public class GetExamQuestionsByExamTemplateIdQueryHandler : IQueryHandler<GetExamQuestionsByExamTemplateIdQuery, List<QuestionDto>>
    {
        private readonly HttpClient _httpClient;
        private readonly IConsulService _consulService;
        private readonly ITokenProviderService _tokenProviderService;

        public GetExamQuestionsByExamTemplateIdQueryHandler(HttpClient httpClient,
            IConsulService consulService,
            ITokenProviderService tokenProviderService)
        {
            _httpClient = httpClient;
            _consulService = consulService;
            _tokenProviderService = tokenProviderService;
        }

        public List<QuestionDto> Handle(GetExamQuestionsByExamTemplateIdQuery query)
        {
            _httpClient.SetBearerToken(_tokenProviderService.GetTokenForApi(ServicesLookup.BackofficeApi, ServicesLookup.ClientApi).Result);
            var response = _httpClient.GetAsync($"http://{_consulService.GetServiceAddress(ServicesLookup.ClientApi)}/api/Exam/GetExamQuestions/{query.ExamId}").Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"Couldnt get questions for exam id: {query.ExamId} Response: {response}");
            }

            var result = response.Content.ReadAsStringAsync().Result;

            return JsonConvert.DeserializeObject<List<QuestionDto>>(result);
        }
    }
}