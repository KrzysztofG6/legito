﻿namespace Legito.BackOffice.Api.Domain.Mapper
{
    using AutoMapper;
    using Profiles;

    public class MapperFactory
    {
        public IMapper Create()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<EntityProfile>();
                cfg.AddProfile<ModelsProfile>();
            });

            return config.CreateMapper();
        }
    }
}