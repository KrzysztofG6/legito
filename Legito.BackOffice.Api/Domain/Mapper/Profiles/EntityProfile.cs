﻿namespace Legito.BackOffice.Api.Domain.Mapper.Profiles
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Core.Dto;
    using Core.Entities;

    public class EntityProfile : Profile
    {
        public EntityProfile()
        {
            CreateMap<Guid, DuplicateAnswer>()
                .ForMember(x => x.Id, m => m.MapFrom(p => p));

            CreateMap<TextComparerResultsDto, ProcessingResult>()
                .ForMember(x => x.ExamId, m => m.MapFrom(p => p.ExamId))
                .ForMember(x => x.QuestionId, m => m.MapFrom(p => p.QuestionId))
                .ForMember(x => x.StudentId, m => m.MapFrom(p => p.StudentId))
                .AfterMap((src, dest) =>
                {
                    foreach (var duplicateAnswer in dest.DuplicateAnswers)
                    {
                        duplicateAnswer.ProcessingResult = dest;
                    }
                });

            CreateMap<WikipediaProcessingResultDto, WikipediaProcessingResult>()
                .AfterMap((src, dest) =>
                {
                    foreach (var urlWithScore in dest.UrlsWithScore)
                    {
                        urlWithScore.WikipediaProcessingResult = dest;
                    }
                });

            CreateMap<AddOrUpdateAntiCheatDto, AntiCheatSettings>();

            CreateMap<AntiCheatSettings, GetAntiCheatSettingsDto>();

            CreateMap<AntiCheatSettings, GetOwnerAntiCheatSettingsDto>();

            CreateMap<WikipediaUrlWithScore, WikipediaUrlDto>()
                .ForMember(x => x.Percentage, m => m.MapFrom(p => p.Percentge))
                .ForMember(x => x.Phrases, m => m.MapFrom(p => DecodeListInString(p.Phrases)));
            CreateMap<WikipediaUrlDto, WikipediaUrlWithScore>()
                .ForMember(x => x.Id, m => m.Ignore())
                .ForMember(x => x.Percentge, m => m.MapFrom(p => p.Percentage))
                .ForMember(x => x.WikipediaProcessingResult, m => m.Ignore())
                .ForMember(x => x.Phrases, m => m.MapFrom(p => string.Join(';', p.Phrases)));
        }

        private List<string> DecodeListInString(string encodedString)
        {
            return encodedString.Split(';').ToList();
        }
    }
}