﻿namespace Legito.BackOffice.Api.Domain.Mapper.Profiles
{
    using AutoMapper;
    using Core.Dto;
    using Core.Models;

    public class ModelsProfile : Profile
    {
        public ModelsProfile()
        {
            CreateMap<AnswerSerializedDto, AnswerWithWikipediaUrl>();

            CreateMap<AnswerWithWikipediaUrl, AnswerWithWikipediaUrlDto>()
                .ForMember(x => x.WikipediaUrls, m => m.MapFrom(p => p.WikipediaUrls.ToArray()));

            CreateMap<TextComparerProcessingResult, TextComparerProcessingResultDto>();
        }
    }
}