﻿namespace Legito.BackOffice.Api.Domain.CommandHandlers
{
    using System.Net.Http;
    using System.Runtime.InteropServices;
    using Commands;
    using KSolution.Cqrs.Core;
    using KSolution.ServiceDiscovery.Core.Enum;
    using KSolution.ServiceDiscovery.Core.Services;

    public class ProcessExamByIdCommandHandler : ICommandHandler<ProcessExamByIdCommand>
    {
        private readonly HttpClient _httpClient;
        private readonly IConsulService _consulService;
        private readonly ITokenProviderService _tokenProviderService;

        public ProcessExamByIdCommandHandler(
            HttpClient httpClient,
            IConsulService consulService,
            ITokenProviderService tokenProviderService)
        {
            _httpClient = httpClient;
            _consulService = consulService;
            _tokenProviderService = tokenProviderService;
        }

        public void Handle(ProcessExamByIdCommand command)
        {
            _httpClient.SetBearerToken(_tokenProviderService.GetTokenForApi(ServicesLookup.BackofficeApi, ServicesLookup.ProcessCoordinatorApi).Result);
            var result = _httpClient.GetAsync($"http://{_consulService.GetServiceAddress(ServicesLookup.ProcessCoordinatorApi)}/api/Processing/Exam/{command.ExamId}").Result;

            if (!result.IsSuccessStatusCode)
            {
                throw new ExternalException($"Could not request exam processing id:{command.ExamId}");
            }
        }
    }
}