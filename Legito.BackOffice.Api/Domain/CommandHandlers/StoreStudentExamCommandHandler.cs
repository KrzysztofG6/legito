﻿namespace Legito.BackOffice.Api.Domain.CommandHandlers
{
    using System.Net.Http;
    using System.Text;
    using Commands;
    using KSolution.Cqrs.Core;
    using KSolution.ServiceDiscovery.Core.Enum;
    using KSolution.ServiceDiscovery.Core.Services;
    using Newtonsoft.Json;

    public class StoreStudentExamCommandHandler : ICommandHandler<StoreStudentExamCommand>
    {
        private readonly HttpClient _httpClient;
        private readonly IConsulService _consulService;
        private readonly ITokenProviderService _tokenProviderService;

        public StoreStudentExamCommandHandler(HttpClient httpClient,
            IConsulService consulService,
            ITokenProviderService tokenProviderService)
        {
            _httpClient = httpClient;
            _consulService = consulService;
            _tokenProviderService = tokenProviderService;
        }

        public void Handle(StoreStudentExamCommand command)
        {
            _httpClient.SetBearerToken(_tokenProviderService.GetTokenForApi(ServicesLookup.BackofficeApi, ServicesLookup.ClientApi).Result);
            var response = _httpClient.PostAsync($"http://{_consulService.GetServiceAddress(ServicesLookup.ClientApi)}/api/Exam/StoreStudentExam", new StringContent(command.Exam, Encoding.UTF8, "application/json")).Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException("Request to client api failed on storing student exam");
            }
        }
    }
}