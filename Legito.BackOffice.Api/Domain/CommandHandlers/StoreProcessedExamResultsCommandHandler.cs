﻿namespace Legito.BackOffice.Api.Domain.CommandHandlers
{
    using Commands;
    using Core.Database;
    using KSolution.Cqrs.Core;
    public class StoreProcessedExamResultsCommandHandler : ICommandHandler<StoreProcessedExamResultsCommand>
    {
        private readonly ILegitoContext _legitoContext;

        public StoreProcessedExamResultsCommandHandler(ILegitoContext legitoContext)
        {
            _legitoContext = legitoContext;
        }

        public void Handle(StoreProcessedExamResultsCommand command)
        {
            _legitoContext.ProcessingResults.AddRange(command.TextComparerResults);
            _legitoContext.WikipediaProcessingResults.AddRange(command.WikipediaProcessingResults);

            _legitoContext.SaveChanges();
        }
    }
}