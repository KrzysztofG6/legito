﻿namespace Legito.BackOffice.Api.Domain.CommandHandlers
{
    using System.Linq;
    using Commands;
    using Core.Database;
    using KSolution.Cqrs.Core;

    public class AddOrUpdateAntiCheatSettingsCommandHandler : ICommandHandler<AddOrUpdateAntiCheatSettingsCommand>
    {
        private readonly ILegitoContext _context;

        public AddOrUpdateAntiCheatSettingsCommandHandler(ILegitoContext context)
        {
            _context = context;
        }

        public void Handle(AddOrUpdateAntiCheatSettingsCommand command)
        {
            var entity = _context.AntiCheatSettings.FirstOrDefault(x => x.OwnerId == command.AntiCheatSettings.OwnerId);

            if (entity == null)
            {
                _context.AntiCheatSettings.Add(command.AntiCheatSettings);
                _context.SaveChanges();
                return;
            }

            entity.DeactivateCopyPasteCut = command.AntiCheatSettings.DeactivateCopyPasteCut;
            entity.DeactivateBackButton = command.AntiCheatSettings.DeactivateBackButton;
            entity.DeactivateRightClick = command.AntiCheatSettings.DeactivateRightClick;
            entity.TrackPageFocus = command.AntiCheatSettings.TrackPageFocus;

            _context.AntiCheatSettings.Update(entity);
            _context.SaveChanges();
        }
    }
}