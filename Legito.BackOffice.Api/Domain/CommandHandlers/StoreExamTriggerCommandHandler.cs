﻿namespace Legito.BackOffice.Api.Domain.CommandHandlers
{
    using Commands;
    using Core.Database;
    using Core.Entities;
    using KSolution.Cqrs.Core;

    public class StoreExamTriggerCommandHandler : ICommandHandler<StoreExamTriggerCommand>
    {
        private readonly ILegitoContext _context;

        public StoreExamTriggerCommandHandler(ILegitoContext context)
        {
            _context = context;
        }

        public void Handle(StoreExamTriggerCommand command)
        {
            _context.ExamStartTriggers.Add(new ExamStartTriggered
            {
                ExamId = command.ExamId,
                StudentId = command.StudentId
            });

            _context.SaveChanges();
        }
    }
}