﻿namespace Legito.BackOffice.Api.Domain.Queries
{
    using System;
    using KSolution.Cqrs.Core.Markers;

    public class GetExamOwnerIdByExamIdQuery : IQuery<Guid>
    {
        public GetExamOwnerIdByExamIdQuery(Guid examId)
        {
            this.ExamId = examId;
        }

        public Guid ExamId { get; }
    }
}