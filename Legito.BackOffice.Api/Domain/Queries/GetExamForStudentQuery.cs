﻿namespace Legito.BackOffice.Api.Domain.Queries
{
    using KSolution.Cqrs.Core.Markers;

    public class GetExamForStudentQuery : IQuery<string>
    {
        public GetExamForStudentQuery(string examId, 
            string studentId)
        {
            this.ExamId = examId;
            this.StudentId = studentId;
        }

        public string ExamId { get; }

        public string StudentId { get; }
    }
}