﻿namespace Legito.BackOffice.Api.Domain.Queries
{
    using Core.Dto;
    using KSolution.Cqrs.Core.Markers;

    public class GetActiveExamsQuery : IQuery<ActiveExamDto[]>
    {
        public GetActiveExamsQuery(string studentId)
        {
            this.StudentId = studentId;
        }

        public string StudentId { get; }
    }
}