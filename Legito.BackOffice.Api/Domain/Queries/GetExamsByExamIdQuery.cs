﻿namespace Legito.BackOffice.Api.Domain.Queries
{
    using System;
    using Core.Dto;
    using KSolution.Cqrs.Core.Markers;

    public class GetExamsByExamIdQuery : IQuery<ExamSerializedDto>
    {
        public GetExamsByExamIdQuery(Guid examId)
        {
            ExamId = examId;
        }

        public Guid ExamId { get; }
    }
}