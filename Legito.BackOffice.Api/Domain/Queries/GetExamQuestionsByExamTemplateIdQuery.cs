﻿namespace Legito.BackOffice.Api.Domain.Queries
{
    using System;
    using System.Collections.Generic;
    using Core.Dto;
    using KSolution.Cqrs.Core.Markers;

    public class GetExamQuestionsByExamTemplateIdQuery : IQuery<List<QuestionDto>>
    {
        public GetExamQuestionsByExamTemplateIdQuery(Guid examId)
        {
            this.ExamId = examId;
        }

        public Guid ExamId { get; }
    }
}