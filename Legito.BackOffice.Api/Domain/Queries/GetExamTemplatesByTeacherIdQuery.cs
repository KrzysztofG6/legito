﻿namespace Legito.BackOffice.Api.Domain.Queries
{
    using KSolution.Cqrs.Core.Markers;

    public class GetExamTemplatesByTeacherIdQuery : IQuery<string>
    {
        public GetExamTemplatesByTeacherIdQuery(string teacherId)
        {
            this.TeacherId = teacherId;
        }

        public string TeacherId { get; }
    }
}