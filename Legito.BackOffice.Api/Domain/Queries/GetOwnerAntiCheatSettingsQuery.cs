﻿namespace Legito.BackOffice.Api.Domain.Queries
{
    using System;
    using Core.Entities;
    using KSolution.Cqrs.Core.Markers;

    public class GetOwnerAntiCheatSettingsQuery : IQuery<AntiCheatSettings>
    {
        public GetOwnerAntiCheatSettingsQuery(Guid ownerId)
        {
            this.OwnerId = ownerId;
        }

        public Guid OwnerId { get; }
    }
}