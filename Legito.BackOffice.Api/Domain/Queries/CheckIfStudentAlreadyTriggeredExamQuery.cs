﻿namespace Legito.BackOffice.Api.Domain.Queries
{
    using System;
    using KSolution.Cqrs.Core.Markers;

    public class CheckIfStudentAlreadyTriggeredExamQuery : IQuery<bool>
    {
        public CheckIfStudentAlreadyTriggeredExamQuery(Guid examId,
            Guid studentId)
        {
            this.ExamId = examId;
            this.StudentId = studentId;
        }

        public Guid ExamId { get; }

        public Guid StudentId { get; }
    }
}