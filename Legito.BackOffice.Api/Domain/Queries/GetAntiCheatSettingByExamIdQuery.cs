﻿namespace Legito.BackOffice.Api.Domain.Queries
{
    using System;
    using Core.Entities;
    using KSolution.Cqrs.Core.Markers;

    public class GetAntiCheatSettingByExamIdQuery : IQuery<AntiCheatSettings>
    {
        public GetAntiCheatSettingByExamIdQuery(Guid examId)
        {
            this.ExamId = examId;
        }

        public Guid ExamId { get; }
    }
}