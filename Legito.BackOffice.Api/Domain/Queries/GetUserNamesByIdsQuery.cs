﻿namespace Legito.BackOffice.Api.Domain.Queries
{
    using System;
    using System.Collections.Generic;
    using Core.Dto;
    using KSolution.Cqrs.Core.Markers;

    public class GetUserNamesByIdsQuery : IQuery<List<UserIdAndNameDto>>
    {
        public GetUserNamesByIdsQuery(Guid[] ids)
        {
            Ids = ids;
        }

        public Guid[] Ids { get; }
    }
}