﻿namespace Legito.BackOffice.Api.Domain.Queries
{
    using System;
    using Core.Dto;
    using KSolution.Cqrs.Core.Markers;

    public class GetExamResultsWithProcessingQuery : IQuery<ResultsWithProcessingDto>
    {
        public GetExamResultsWithProcessingQuery(Guid examId)
        {
            this.ExamId = examId;
        }

        public Guid ExamId { get; }
    }
}