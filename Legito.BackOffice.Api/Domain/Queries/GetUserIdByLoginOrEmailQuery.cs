﻿namespace Legito.BackOffice.Api.Domain.Queries
{
    using KSolution.Cqrs.Core.Markers;

    public class GetUserIdByLoginOrEmailQuery : IQuery<string>
    {
        public GetUserIdByLoginOrEmailQuery(string loginOrEmail)
        {
            this.LoginOrEmail = loginOrEmail;
        }

        public string LoginOrEmail { get; }
    }
}