﻿namespace Legito.BackOffice.Api.Domain.Database
{
    using System.Threading.Tasks;
    using Core.Database;
    using Core.Entities;
    using Microsoft.EntityFrameworkCore;

    public class LegitoContext : DbContext, ILegitoContext
    {
        public LegitoContext(DbContextOptions<LegitoContext> options) 
            : base(options)
        {
        }

        public DbSet<ProcessingResult> ProcessingResults { get; set; }

        public DbSet<DuplicateAnswer> DuplicateAnswers { get; set; }

        public DbSet<WikipediaProcessingResult> WikipediaProcessingResults { get; set; }

        public DbSet<AntiCheatSettings> AntiCheatSettings { get; set; }

        public DbSet<ExamStartTriggered> ExamStartTriggers { get; set; }

        public DbSet<WikipediaUrlWithScore> WikipdiaUrlsWithScore { get; set; }

        public Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("boa");

            modelBuilder.Entity<ProcessingResult>()
                .HasMany(x => x.DuplicateAnswers)
                .WithOne(x => x.ProcessingResult);

            modelBuilder.Entity<DuplicateAnswer>()
                .HasOne(x => x.ProcessingResult)
                .WithMany(x => x.DuplicateAnswers);

            modelBuilder.Entity<WikipediaProcessingResult>()
                .HasMany(x => x.UrlsWithScore)
                .WithOne(x => x.WikipediaProcessingResult);

            modelBuilder.Entity<WikipediaUrlWithScore>()
                .HasOne(x => x.WikipediaProcessingResult)
                .WithMany(x => x.UrlsWithScore);

            modelBuilder.Entity<AntiCheatSettings>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<ExamStartTriggered>()
                .HasKey(x => x.Id);
        }
    }
}