﻿namespace Legito.BackOffice.Api.Domain.Database
{
    using System.IO;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Design;
    using Microsoft.Extensions.Configuration;

    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<LegitoContext>
    {
        public LegitoContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            var builder = new DbContextOptionsBuilder<LegitoContext>();
            var connectionString = configuration.GetConnectionString("LegitoBackOfficeDb");
            builder.UseSqlServer(connectionString);
            return new LegitoContext(builder.Options);
        }
    }
}