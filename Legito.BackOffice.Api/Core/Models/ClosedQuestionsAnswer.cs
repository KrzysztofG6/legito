﻿namespace Legito.BackOffice.Api.Core.Models
{
    public class ClosedQuestionsAnswer
    {
        public string Answer { get; set; }

        public bool IsCorrect { get; set; }
    }
}