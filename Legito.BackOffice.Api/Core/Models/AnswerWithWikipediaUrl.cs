﻿namespace Legito.BackOffice.Api.Core.Models
{
    using System;
    using System.Collections.Generic;
    using Dto;

    public class AnswerWithWikipediaUrl
    {
        public AnswerWithWikipediaUrl()
        {
            WikipediaUrls = new List<WikipediaUrlDto>();
        }

        public Guid Id { get; set; }

        public Guid StudentId { get; set; }

        public string StudentName { get; set; }

        public Guid QuestionId { get; set; }

        public string Question { get; set; }

        public string StudentAnswer { get; set; }

        public int Type { get; set; }

        public List<WikipediaUrlDto> WikipediaUrls { get; set; }
    }
}