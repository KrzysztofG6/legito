﻿namespace Legito.BackOffice.Api.Core.Models
{
    using System;
    using System.Collections.Generic;
    using Entities;

    public class TextComparerProcessingResult
    {
        public Guid SearchAgainstStudentId { get; set; }

        public string SearchAgainstStudentName { get; set; }

        public Guid QuestionId { get; set; }

        public string Question { get; set; }

        public List<string> StudentNames { get; set; }
    }
}