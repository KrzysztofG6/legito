﻿namespace Legito.BackOffice.Api.Core.Database
{
    using System.Threading;
    using System.Threading.Tasks;
    using Entities;
    using Microsoft.EntityFrameworkCore;

    public interface ILegitoContext
    {
        DbSet<ProcessingResult> ProcessingResults { get; set; }

        DbSet<DuplicateAnswer> DuplicateAnswers { get; set; }

        DbSet<WikipediaProcessingResult> WikipediaProcessingResults { get; set; }

        DbSet<AntiCheatSettings> AntiCheatSettings { get; set; }

        DbSet<ExamStartTriggered> ExamStartTriggers { get; set; }

        DbSet<WikipediaUrlWithScore> WikipdiaUrlsWithScore { get; set; }

        int SaveChanges();
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}