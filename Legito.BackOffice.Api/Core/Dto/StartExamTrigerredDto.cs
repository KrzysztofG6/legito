﻿namespace Legito.BackOffice.Api.Core.Dto
{
    using System;

    public class StartExamTrigerredDto
    {
        public Guid ExamId { get; set; }

        public Guid StudentId { get; set; }
    }
}