﻿namespace Legito.BackOffice.Api.Core.Dto
{
    public class IdDto
    {
        public string Id { get; set; }
    }
}