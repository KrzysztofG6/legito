﻿namespace Legito.BackOffice.Api.Core.Dto
{
    public class GetOwnerAntiCheatSettingsDto
    {
        public bool DeactivateCopyPasteCut { get; set; }

        public bool DeactivateRightClick { get; set; }

        public bool TrackPageFocus { get; set; }

        public bool DeactivateBackButton { get; set; }
    }
}