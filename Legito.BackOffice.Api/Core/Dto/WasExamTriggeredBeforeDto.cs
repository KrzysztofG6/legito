﻿namespace Legito.BackOffice.Api.Core.Dto
{
    using System;

    public class WasExamTriggeredBeforeDto
    {
        public Guid ExamId { get; set; }

        public Guid StudentId { get; set; }
    }
}