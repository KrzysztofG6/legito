﻿namespace Legito.BackOffice.Api.Core.Dto
{
    public class ExamDto
    {
        public string Exam { get; set; }
    }
}