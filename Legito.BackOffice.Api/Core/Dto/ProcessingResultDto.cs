﻿namespace Legito.BackOffice.Api.Core.Dto
{
    using System;
    using System.Collections.Generic;

    public class ProcessingResultDto
    {
        public List<TextComparerResultsDto> TextComparerResults { get; set; }

        public List<WikipediaProcessingResultDto> WikipediaResults { get; set; }
    }
}