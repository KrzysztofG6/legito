﻿namespace Legito.BackOffice.Api.Core.Dto
{
    using System;

    public class AnswerSerializedDto
    {
        public Guid Id { get; set; }

        public Guid StudentId { get; set; }

        public Guid QuestionId { get; set; }

        public string StudentAnswer { get; set; }

        public int Type { get; set; }
    }
}