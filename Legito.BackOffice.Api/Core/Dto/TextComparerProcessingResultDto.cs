﻿namespace Legito.BackOffice.Api.Core.Dto
{
    using System.Collections.Generic;

    public class TextComparerProcessingResultDto
    {
        public string SearchAgainstStudentName { get; set; }

        public string Question { get; set; }

        public List<string> StudentNames { get; set; }
    }
}