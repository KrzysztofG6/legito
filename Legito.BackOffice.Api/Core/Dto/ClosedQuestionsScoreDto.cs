﻿namespace Legito.BackOffice.Api.Core.Dto
{
    public class ClosedQuestionsScoreDto
    {
        public int Score { get; set; }

        public int OutOf { get; set; }

        public string StudentName { get; set; }
    }
}