﻿namespace Legito.BackOffice.Api.Core.Dto
{
    using System.Collections.Generic;

    public class AnswerWithWikipediaUrlDto
    {
        public string StudentName { get; set; }

        public string Question { get; set; }

        public string StudentAnswer { get; set; }

        public WikipediaUrlDto[] WikipediaUrls { get; set; }
    }
}