﻿namespace Legito.BackOffice.Api.Core.Dto
{
    using System;

    public class AddOrUpdateAntiCheatDto
    {
        public Guid OwnerId { get; set; }

        public bool DeactivateCopyPasteCut { get; set; }

        public bool DeactivateRightClick { get; set; }

        public bool TrackPageFocus { get; set; }

        public bool DeactivateBackButton { get; set; }
    }
}