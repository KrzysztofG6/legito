﻿namespace Legito.BackOffice.Api.Core.Dto
{
    public class StoreStudentExamDto
    {
        public string Exam { get; set; }
    }
}