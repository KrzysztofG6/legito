﻿namespace Legito.BackOffice.Api.Core.Dto
{
    using System;
    using Models;

    public class QuestionDto
    {
        public string Id { get; set; }

        public string Text { get; set; }

        public ClosedQuestionsAnswer[] PossibleAnswers { get; set; }
    }
}