﻿namespace Legito.BackOffice.Api.Core.Dto
{
    using System.Collections.Generic;
    using Models;

    public class ResultsWithProcessingDto
    {
        public List<List<AnswerWithWikipediaUrlDto>> AnswersWithWikiScan { get; set; }

        public List<TextComparerProcessingResultDto> TextComparerResults { get; set; }

        public List<ClosedQuestionsScoreDto> ClosedQuestionsResults { get; set; }
    }
}