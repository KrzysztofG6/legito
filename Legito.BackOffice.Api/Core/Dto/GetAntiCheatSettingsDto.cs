﻿namespace Legito.BackOffice.Api.Core.Dto
{
    public class GetAntiCheatSettingsDto
    {
        public bool DeactivateCopyPasteCut { get; set; }

        public bool DeactivateRightClick { get; set; }

        public bool TrackPageFocus { get; set; }

        public bool DeactivateBackButton { get; set; }
    }
}