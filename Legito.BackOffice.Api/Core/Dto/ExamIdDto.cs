﻿namespace Legito.BackOffice.Api.Core.Dto
{
    using System;

    public class ExamIdDto
    {
        public Guid ExamId { get; set; }
    }
}