﻿namespace Legito.BackOffice.Api.Core.Entities
{
    using System;

    public class DuplicateAnswer
    {
        public Guid Id { get; set; }

        public ProcessingResult ProcessingResult { get; set; }
    }
}