﻿namespace Legito.BackOffice.Api.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class ProcessingResult
    {
        public ProcessingResult()
        {
            this.DuplicateAnswers = new List<DuplicateAnswer>();
        }

        [Key]
        public Guid Id { get; set; }

        public Guid ExamId { get; set; }

        public Guid StudentId { get; set; }

        public Guid QuestionId { get; set; }

        public List<DuplicateAnswer> DuplicateAnswers { get; set; }
    }
}