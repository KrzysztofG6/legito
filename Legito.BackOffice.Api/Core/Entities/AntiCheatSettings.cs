﻿namespace Legito.BackOffice.Api.Core.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class AntiCheatSettings
    {

        [Key]
        public Guid Id { get; set; }

        public Guid OwnerId { get; set; }

        public bool DeactivateCopyPasteCut { get; set; }

        public bool DeactivateRightClick { get; set; }

        public bool TrackPageFocus { get; set; }

        public bool DeactivateBackButton { get; set; }
    }
}