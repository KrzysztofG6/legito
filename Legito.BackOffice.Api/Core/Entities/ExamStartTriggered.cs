﻿namespace Legito.BackOffice.Api.Core.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class ExamStartTriggered
    {
        [Key]
        public Guid Id { get; set; }

        public Guid StudentId { get; set; }

        public Guid ExamId { get; set; }
    }
}