﻿namespace Legito.BackOffice.Api.Core.Services
{
    using Dto;
    using System;

    public interface ISettingsService
    {
        void AddOrUpdateAntiCheatSettings(AddOrUpdateAntiCheatDto model);

        GetAntiCheatSettingsDto GetSettings(Guid examId);

        GetOwnerAntiCheatSettingsDto GetOwnerAntiCheatSettings(Guid ownerId);
    }
}