﻿namespace Legito.BackOffice.Api.Core.Services
{
    using System;
    using System.Collections.Generic;
    using Dto;

    public interface IProcessingService
    {
        void ProcessExam(Guid examId);

        void StoreProcessingResults(ProcessingResultDto results);
    }
}