﻿namespace Legito.BackOffice.Api.Core.Services
{
    using System;
    using Dto;

    public interface IExamService
    {
        void StoreCreatedExam(string exam);

        string GetExamTemplatesJsonByTeacherId(string id);

        ActiveExamDto[] GetActiveExamsByStudentId(string studentId);

        void ActivateExam(string examId);

        void EndExam(string id);

        string StartExamForStudent(string examId, string studentId);

        void StoreStudentExam(string exam);

        void ExamStartTriggered(Guid examId, Guid studentId);

        bool CheckIfStudentAlreadyTriggeredExam(Guid examId, Guid studentId);
    }
}