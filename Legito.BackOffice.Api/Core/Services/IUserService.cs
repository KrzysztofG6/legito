﻿namespace Legito.BackOffice.Api.Core.Services
{
    using System;

    public interface IUserService
    {
        Guid GetUserIdByLoginOrEmail(string loginOrEmail);
    }
}