﻿namespace Legito.BackOffice.Api.Core.Services
{
    using System;
    using Dto;

    public interface IResultsService
    {
        ResultsWithProcessingDto GetExamResults(Guid examId);
    }
}