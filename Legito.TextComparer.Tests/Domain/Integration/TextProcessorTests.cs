﻿namespace Legito.TextComparer.Tests.Domain.Integration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Core;
    using Core.Model;
    using FluentAssertions;
    using TextComparer.Domain;
    using Xunit;

    public class TextProcessorTests
    {
        private ISimilarityComparer _similarityComparer;
        private TextProcessor _textProcessor;

        private StudentPapers papers;

        public TextProcessorTests()
        {
            _similarityComparer = new SimilarityComparer();
            _textProcessor = new TextProcessor(_similarityComparer);
        }

        [Fact]
        public void ShouldGroupTwoSameAnswersForTwoQuestions()
        {
            var examId = Guid.NewGuid();
            var question1Id = Guid.NewGuid();
            var question2Id = Guid.NewGuid();
            var answer1Id = Guid.NewGuid();
            var answer2Id = Guid.NewGuid();
            var answer3Id = Guid.NewGuid();
            var answer4Id = Guid.NewGuid();
            var student1Id = Guid.NewGuid();
            var student2Id = Guid.NewGuid();

            var answers = new List<Answer>
            {
                new Answer
                {
                    QuestionId = question1Id,
                    StudentId = student1Id,
                    Id = answer1Id,
                    StudentAnswer = "The answer to this question is pretty simple"
                },
                new Answer
                {
                    QuestionId = question1Id,
                    StudentId = student2Id,
                    Id = answer2Id,
                    StudentAnswer = "The answer to this question is pretty simple"
                },
                new Answer
                {
                    QuestionId = question2Id,
                    StudentId = student1Id,
                    Id = answer3Id,
                    StudentAnswer = "I dont know"
                },
                new Answer
                {
                    QuestionId = question2Id,
                    StudentId = student2Id,
                    Id = answer4Id,
                    StudentAnswer = "I dont know"
                }
            };

            papers = new StudentPapers
            {
                ExamId = examId,
                Answers = answers
            };

            var results = _textProcessor.ProcessAnswers(papers);

            results.Count().Should().Be(2);
            results.ElementAt(0).ElementAt(0).DuplicateAnswerIds.Count.Should().Be(1);

            results.ElementAt(0).ElementAt(0).SearchAgainst.Id.Should().Be(answer1Id);
            results.ElementAt(0).ElementAt(0).DuplicateAnswerIds.ElementAt(0).Should().Be(answer2Id);

            results.ElementAt(1).ElementAt(0).SearchAgainst.Id.Should().Be(answer3Id);
            results.ElementAt(1).ElementAt(0).DuplicateAnswerIds.ElementAt(0).Should().Be(answer4Id);
        }

        [Fact]
        public void ShouldNotGroupDifferentAnswersAndReturnedFilteredCountZero()
        {
            var examId = Guid.NewGuid();
            var question1Id = Guid.NewGuid();
            var answer1Id = Guid.NewGuid();
            var answer2Id = Guid.NewGuid();
            var student1Id = Guid.NewGuid();
            var student2Id = Guid.NewGuid();

            var answers = new List<Answer>
            {
                new Answer
                {
                    QuestionId = question1Id,
                    StudentId = student1Id,
                    Id = answer1Id,
                    StudentAnswer = "The answer to this question is pretty simple"
                },
                new Answer
                {
                    QuestionId = question1Id,
                    StudentId = student2Id,
                    Id = answer2Id,
                    StudentAnswer = "Totally different"
                }
            };

            papers = new StudentPapers
            {
                ExamId = examId,
                Answers = answers
            };

            var results = _textProcessor.ProcessAnswers(papers);

            results.Count().Should().Be(0);
        }

        [Fact]
        public void ShouldReturnOneCopyAndFilterResultsWithNoDuplicates()
        {
            var examId = Guid.NewGuid();
            var question1Id = Guid.NewGuid();
            var answer1Id = Guid.NewGuid();
            var answer2Id = Guid.NewGuid();
            var answer3Id = Guid.NewGuid();
            var student1Id = Guid.NewGuid();
            var student2Id = Guid.NewGuid();
            var student3Id = Guid.NewGuid();

            var answers = new List<Answer>
            {
                new Answer
                {
                    QuestionId = question1Id,
                    StudentId = student1Id,
                    Id = answer1Id,
                    StudentAnswer = "The answer to this question is pretty simple"
                },
                new Answer
                {
                    QuestionId = question1Id,
                    StudentId = student2Id,
                    Id = answer2Id,
                    StudentAnswer = "Totally different"
                },
                new Answer
                {
                    QuestionId = question1Id,
                    Id = answer3Id,
                    StudentId = student3Id,
                    StudentAnswer = "The answer to this question is pretty simple"
                }
            };

            papers = new StudentPapers
            {
                ExamId = examId,
                Answers = answers
            };

            var results = _textProcessor.ProcessAnswers(papers);

            results.Count().Should().Be(1);
            results.ElementAt(0).ElementAt(0).DuplicateAnswerIds.Count.Should().Be(1);
            results.ElementAt(0).Count.Should().Be(1);
        }
    }
}