﻿namespace KSolution.Cqrs.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Core;
    using Core.Markers;

    public class Mediator : IMediator
    {
        private readonly Func<Type, ICommandHandler> _commandHandlersFactory;
        private readonly Func<Type, IQueryHandler> _queryHandlersFactory;
        private readonly Func<Type, IEnumerable<IEventHandler>> _eventHandlersFactory;

        public Mediator(Func<Type, ICommandHandler> commandHandlersFactory,
            Func<Type, IQueryHandler> queryHandlersFactory,
            Func<Type, IEnumerable<IEventHandler>> eventHandlersFactory)
        {
            _commandHandlersFactory = commandHandlersFactory;
            _queryHandlersFactory = queryHandlersFactory;
            _eventHandlersFactory = eventHandlersFactory;
        }

        public void Send<TCommand>(TCommand command) where TCommand : ICommand
        {
            var handler = (ICommandHandler<TCommand>)_commandHandlersFactory(typeof(TCommand));
            handler.Handle(command);
        }

        public TResult Fetch<TQuery, TResult>(TQuery query) where TQuery : IQuery<TResult>
        {
            var handler = (IQueryHandler<TQuery, TResult>) _queryHandlersFactory(typeof(TQuery));
            return handler.Handle(query);
        }

        public void Publish<TEvent>(TEvent @event) where TEvent : IEvent
        {
            var handlers = _eventHandlersFactory(typeof(TEvent))
                .Cast<IEventHandler<TEvent>>();

            foreach (var handler in handlers)
            {
                handler.Handle(@event);
            }
        }
    }
}