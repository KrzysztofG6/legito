﻿namespace KSolution.Cqrs.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Autofac;
    using Core;
    using Core.Markers;
    using Domain;
    using Module = Autofac.Module;

    public class CqrsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            var assembly = Assembly.GetEntryAssembly();

            builder.RegisterAssemblyTypes(assembly)
                .Where(x => x.IsAssignableTo<ICommandHandler>())
                .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(assembly)
                .Where(x => x.IsAssignableTo<IQueryHandler>())
                .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(assembly)
                .Where(x => x.IsAssignableTo<IEventHandler>())
                .AsImplementedInterfaces();

            builder.Register<Func<Type, ICommandHandler>>(c =>
            {
                var ctx = c.Resolve<IComponentContext>();

                return t =>
                {
                    var handlerType = typeof(ICommandHandler<>).MakeGenericType(t);
                    return (ICommandHandler)ctx.Resolve(handlerType);
                };
            });

            builder.Register<Func<Type, IQueryHandler>>(c =>
            {
                var ctx = c.Resolve<IComponentContext>();
                var queryType = typeof(IQuery<>);

                return t =>
                {
                    var handlerType = typeof(IQueryHandler<,>).MakeGenericType(t, t.GetInterfaces().Single(x => x.Name == queryType.Name).GenericTypeArguments[0]);
                    return (IQueryHandler) ctx.Resolve(handlerType);
                };
            });

            builder.Register<Func<Type, IEnumerable<IEventHandler>>>(c =>
            {
                var ctx = c.Resolve<IComponentContext>();
                return t =>
                {
                    var handlerType = typeof(IEventHandler<>).MakeGenericType(t);
                    var handlersCollectionType = typeof(IEnumerable<>).MakeGenericType(handlerType);
                    return (IEnumerable<IEventHandler>)ctx.Resolve(handlersCollectionType);
                };
            });

            builder.RegisterType<Mediator>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}