﻿namespace KSolution.Cqrs.Core
{
    using Markers;

    public interface IQueryHandler<in TQuery, out TResult> : IQueryHandler where TQuery : IQuery<TResult>
    {
        TResult Handle(TQuery query);
    }
}