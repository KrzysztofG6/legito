﻿namespace KSolution.Cqrs.Core
{
    using Markers;

    public interface ICommandHandler<in T> : ICommandHandler where T : ICommand
    {
        void Handle(T command);
    }
}