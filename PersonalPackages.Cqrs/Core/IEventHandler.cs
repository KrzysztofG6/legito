﻿namespace KSolution.Cqrs.Core
{
    using Markers;

    public interface IEventHandler<in TEvent> : IEventHandler where TEvent : IEvent
    {
        void Handle(TEvent @event);
    }
}