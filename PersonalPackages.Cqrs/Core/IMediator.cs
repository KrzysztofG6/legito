﻿namespace KSolution.Cqrs.Core
{
    using Markers;

    public interface IMediator
    {
        void Send<TCommand>(TCommand command) where TCommand : ICommand;

        TResult Fetch<TQuery, TResult>(TQuery query) where TQuery : IQuery<TResult>;

        void Publish<TEvent>(TEvent @event) where TEvent : IEvent;
    }
}