﻿namespace KSolution.ServiceDiscovery.Services
{
    using System;
    using System.Security.Authentication;
    using System.Threading.Tasks;
    using Core.Enum;
    using Core.Extensions;
    using Core.Services;
    using IdentityModel.Client;

    public class TokenProviderService : ITokenProviderService
    {
        private readonly IConsulService consulService;

        public TokenProviderService(IConsulService consulService)
        {
            this.consulService = consulService;
        }

        public async Task<string> GetTokenForApi(ServicesLookup fromApi, ServicesLookup requestedApi)
        {
            var authServerAddress = $"http://{consulService.GetServiceAddress(ServicesLookup.AuthorizationServer)}";

            var discovery = new DiscoveryClient(authServerAddress)
            {
                Policy = new DiscoveryPolicy
                {
                    RequireHttps = false,
                    ValidateEndpoints = false
                }
            };

            var discoveryResult = await discovery.GetAsync();

            if (discoveryResult.IsError)
            {
                throw new NullReferenceException($"Discovery client error: {discoveryResult.Error}");
            }

            var tokenClient = new TokenClient(discoveryResult.TokenEndpoint, fromApi.Description(), "secret");
            var tokenResponse = await tokenClient.RequestClientCredentialsAsync(requestedApi.Description());

            if (tokenResponse.IsError)
            {
                throw new AuthenticationException($"Could not revice token for {requestedApi.Description()}: {tokenResponse.Error}");
            }

            return tokenResponse.AccessToken;
        }
    }
}