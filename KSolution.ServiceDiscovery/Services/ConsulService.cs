﻿namespace KSolution.ServiceDiscovery.Services
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Runtime.InteropServices;
    using System.Threading.Tasks;
    using Consul;
    using Core.Enum;
    using Core.Extensions;
    using Core.Services;
    using DnsClient;

    public class ConsulService : IConsulService
    {
        private readonly IDnsQuery _dnsQuery;

        public ConsulService(IDnsQuery dnsQuery)
        {
            _dnsQuery = dnsQuery;
        }

        public string GetServiceAddress(ServicesLookup service)
        {
            var result = _dnsQuery.ResolveServiceAsync("service.consul", service.Description()).Result;

            if (string.IsNullOrEmpty(result.First()?.HostName) || result.First()?.Port == 0)
            {
                throw new ConsulRequestException($"{service.Description()} address was not registered in consul", HttpStatusCode.NotFound);
            }

            if (result[0].HostName.Last() == '.')
            {
                return $"{result[0].HostName.Remove(result[0].HostName.Count() - 1)}:{result.First().Port}";
            }

            return $"{result.First().HostName}:{result.First().Port}";
        }
    }
}