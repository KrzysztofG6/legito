﻿namespace KSolution.ServiceDiscovery.Core.Constants
{
    using System;
    using System.Net;

    public static class ServiceDiscoveryConstants
    {
        // 192.168.65.2
        public static string DockerHostMachineIpAddress => Dns.GetHostAddresses(new Uri("http://docker.for.win.localhost").Host)[0].ToString();
    }
}