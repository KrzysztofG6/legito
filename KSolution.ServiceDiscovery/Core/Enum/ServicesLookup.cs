﻿namespace KSolution.ServiceDiscovery.Core.Enum
{
    using System.ComponentModel.DataAnnotations;

    public enum ServicesLookup
    {
        [Display(Description = "LegitoApi")]
        ClientApi,

        [Display(Description = "LegitoAuthorizationServer")]
        AuthorizationServer,

        [Display(Description = "LegitoBackOfficeApi")]
        BackofficeApi,

        [Display(Description = "LegitoErrorLoggerApi")]
        ErrorLoggerApi,

        [Display(Description = "LegitoProcessorCoordinatorApi")]
        ProcessCoordinatorApi
    }
}