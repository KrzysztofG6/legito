﻿namespace KSolution.ServiceDiscovery.Core.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Consul;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Options;
    using Model;

    public static class ConsulExtensions
    {
        public static IApplicationBuilder UseConsulServiceRegistration(this IApplicationBuilder app)
        {
            var appLife = app.ApplicationServices.GetRequiredService<IApplicationLifetime>() ?? throw new ArgumentException("Missing dependency", nameof(IApplicationLifetime));
            var serviceOptions = app.ApplicationServices.GetRequiredService<IOptions<ServiceDiscoveryOptions>>() ?? throw new ArgumentException("Missing dependency", nameof(IOptions<ServiceDiscoveryOptions>));
            var consul = app.ApplicationServices.GetRequiredService<IConsulClient>() ?? throw new ArgumentException("Missing dependency", nameof(IConsulClient));
            
            if (string.IsNullOrEmpty(serviceOptions.Value.ServiceName))
            {
                throw new ArgumentException("Service Name must be configured", nameof(serviceOptions.Value.ServiceName));
            }

            IEnumerable<Uri> addresses = null;
            if (serviceOptions.Value.Endpoints != null && serviceOptions.Value.Endpoints.Length > 0)
            {
                addresses = serviceOptions.Value.Endpoints.Select(p => new Uri(p));
            }
            else
            {
                throw new NullReferenceException("Endpoints for consul have not been set!");
            }

            foreach (var address in addresses)
            {
                var serviceId = $"{serviceOptions.Value.ServiceName}_{address.Host}:{address.Port}";

                var serviceChecks = new List<AgentServiceCheck>();

                if (!string.IsNullOrEmpty(serviceOptions.Value.HealthCheckAddress))
                {
                    var healthCheckUri = new Uri(serviceOptions.Value.HealthCheckAddress).OriginalString;
                    serviceChecks.Add(new AgentServiceCheck
                    {
                        Status = HealthStatus.Passing,
                        DeregisterCriticalServiceAfter = TimeSpan.FromMinutes(1),
                        Interval = TimeSpan.FromSeconds(5),
                        HTTP = healthCheckUri
                    });
                }

                var registration = new AgentServiceRegistration
                {
                    Checks = serviceChecks.ToArray(),
                    Address = address.Host,
                    ID = serviceId,
                    Name = serviceOptions.Value.ServiceName,
                    Port = address.Port
                };

                consul.Agent.ServiceRegister(registration).GetAwaiter().GetResult();

                appLife.ApplicationStopping.Register(() =>
                {
                    consul.Agent.ServiceDeregister(serviceId).GetAwaiter().GetResult();
                });
            }

            return app;
        }

        public static IServiceCollection AddServiceDiscovery(this IServiceCollection services, IConfiguration serviceOptionsConfiguration)
        {
            services.AddOptions();

            services.Configure<ServiceDiscoveryOptions>(serviceOptionsConfiguration);

            services.AddSingleton<IConsulClient>(p => new ConsulClient(cfg =>
            {
                var serviceConfiguration = p.GetRequiredService<IOptions<ServiceDiscoveryOptions>>().Value;

                if (!string.IsNullOrEmpty(serviceConfiguration.Consul.HttpEndpoint))
                {
                    cfg.Address = new Uri(serviceConfiguration.Consul.HttpEndpoint);
                }
                else
                {
                    throw new ArgumentNullException(serviceConfiguration.Consul.HttpEndpoint, "Consul HTTPEndpoint has to be set in configuration JSON!");
                }
            }));

            return services;
        }
    }
}