﻿namespace KSolution.ServiceDiscovery.Core.Services
{
    using Enum;

    public interface IConsulService
    {
        string GetServiceAddress(ServicesLookup service);
    }
}