﻿namespace KSolution.ServiceDiscovery.Core.Services
{
    using System.Threading.Tasks;
    using Enum;

    public interface ITokenProviderService
    {
        Task<string> GetTokenForApi(ServicesLookup fromApi, ServicesLookup requestedApi);
    }
}