﻿namespace KSolution.ServiceDiscovery.Core.Model
{
    public class ConsulOptions
    {
        public string HttpEndpoint { get; set; }
    }
}