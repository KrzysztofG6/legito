﻿namespace KSolution.ServiceDiscovery.Core.Model
{
    public class ServiceDiscoveryOptions
    {
        public string ServiceName { get; set; }

        public ConsulOptions Consul { get; set; }

        public string HealthCheckAddress { get; set; }

        public string[] Endpoints { get; set; }
    }
}