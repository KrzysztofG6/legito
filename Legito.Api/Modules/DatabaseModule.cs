﻿namespace Legito.Api.Modules
{
    using Autofac;
    using Domain.Database;

    public class DatabaseModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<LegitoContext>()
                .AsImplementedInterfaces()
                .SingleInstance();
        }
    }
}