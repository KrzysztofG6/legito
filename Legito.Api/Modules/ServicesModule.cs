﻿namespace Legito.Api.Modules
{
    using System;
    using System.Net;
    using Autofac;
    using DnsClient;
    using KSolution.ServiceDiscovery.Core.Constants;
    using KSolution.ServiceDiscovery.Services;
    using Services;

    public class ServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<ExamService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<ConsulService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.Register(x =>
                    new LookupClient(IPAddress.Parse(ServiceDiscoveryConstants.DockerHostMachineIpAddress), 8600)
                    {
                        EnableAuditTrail = false,
                        UseCache = true,
                        MinimumCacheTimeout = TimeSpan.FromSeconds(1)
                    })
                .As<IDnsQuery>()
                .SingleInstance();
        }
    }
}