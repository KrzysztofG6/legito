﻿namespace Legito.Api
{
    using System;
    using Autofac;
    using Autofac.Extensions.DependencyInjection;
    using KSolution.Cqrs.Modules;
    using KSolution.ServiceDiscovery.Core.Enum;
    using KSolution.ServiceDiscovery.Core.Extensions;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Middleware;
    using Modules;
    using Serilog;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddMvcCore()
                .AddAuthorization()
                .AddJsonFormatters();

            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(opt =>
                {
                    opt.Authority = "http://localhost:5000";
                    opt.RequireHttpsMetadata = false;

                    opt.ApiName = ServicesLookup.ClientApi.Description();
                });

            services.AddTransient<ILogger>(x => new LoggerConfiguration()
                .MinimumLevel.Warning()
                .ReadFrom.Configuration(Configuration).CreateLogger());

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAnyRequests", policy =>
                {
                    policy.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials();
                });
            });

            services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));

            var containerBuilder = new ContainerBuilder();
            containerBuilder.Populate(services);
            containerBuilder.RegisterModule<CqrsModule>();
            containerBuilder.RegisterModule<DatabaseModule>();
            containerBuilder.RegisterModule<DomainModule>();
            containerBuilder.RegisterModule<ServicesModule>();
            var container = containerBuilder.Build();
            return new AutofacServiceProvider(container);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseMiddleware(typeof(ErrorHandlingMiddleware));

            app.UseAuthentication();
            app.UseMvc();
            app.UseConsulServiceRegistration();
        }
    }
}
