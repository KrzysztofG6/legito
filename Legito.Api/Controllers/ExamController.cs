﻿namespace Legito.Api.Controllers
{
    using System;
    using Core.Dto;
    using Core.Services;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    [Produces("application/json")]
    [Route("api/Exam")]
    [Authorize]
    public class ExamController : Controller
    {
        private readonly IExamService _examService;

        public ExamController(IExamService examService)
        {
            _examService = examService;
        }

        [HttpGet("GetResultById/{examId}")]
        public ActionResult GetResultById(Guid examId)
        {
            var serializedExams = _examService.GetSerializedExamById(examId);

            return Ok(serializedExams);
        }

        [HttpPost("StoreCreatedExam")]
        public IActionResult StoreCreatedExam([FromBody]StoreCreatedExamDto model)
        {
            _examService.StoreCreatedExam(model.Exam);

            return Ok();
        }

        [HttpGet("ExamTemplates/{teacherId}")]
        public IActionResult ExamTemplates(string teacherId)
        {
            var result = _examService.GetExamTemplatesAsJsonByTeacherId(teacherId);

            return Ok(result);
        }

        [HttpGet("ActiveExams/{studentId}")]
        public IActionResult ActiveExams(string studentId)
        {
            var result = _examService.GetActiveExamsByStudentId(studentId);

            return Ok(result.ToArray());
        }

        [HttpPut("ActivateExam")]
        public IActionResult ActivateExam([FromBody]IdDto model)
        {
            _examService.ActivateExam(model.Id);

            return Ok();
        }

        [HttpPut("EndExam")]
        public IActionResult EndExam([FromBody] IdDto model)
        {
            _examService.EndExam(model.Id);

            return Ok();
        }

        [HttpPost("StartExam")]
        public IActionResult StartExam([FromBody]StartExamDto model)
        {
            var result = _examService.StartExamByStudent(model.StudentId, model.ExamId);

            return Ok(JsonConvert.SerializeObject(result,
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }));
        }

        [HttpPost("StoreStudentExam")]
        public IActionResult StoreStudentExam([FromBody] ExamDto model)
        {
            _examService.StoreStudentExam(model);

            return Ok();
        }

        [HttpGet("GetExamQuestions/{examId}")]
        public IActionResult GetExamQuestions(string examId)
        {
            var result = _examService.GetQuestionsByExamIdTemplate(Guid.Parse(examId));

            return Ok(result);
        }

        [HttpPut("SetEndOfProcessing")]
        public IActionResult SetEndOfProcessing([FromBody] SetEndOfProcessingDto model)
        {
            _examService.SetEndOfProcessing(Guid.Parse(model.ExamId));

            return Ok();
        }

        [HttpGet("GetExamOwnerId/{examId}")]
        public IActionResult GetExamOwnerId(string examId)
        {
            var result = _examService.GetExamOwner(Guid.Parse(examId));

            return Ok(result);
        }
    }
}