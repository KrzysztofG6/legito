﻿namespace Legito.Api.Services
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Core.Dto;
    using Core.Entity;
    using Core.Entity.ExamTemplate;
    using Core.Services;
    using Domain.Commands;
    using Domain.Queries;
    using Domain.QueryHandlers;
    using KSolution.Cqrs.Core;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    public class ExamService : IExamService
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public ExamService(IMediator mediator,
            IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        public ExamSerializedDto GetSerializedExamById(Guid examId)
        {
            return _mediator.Fetch<GetExamsByExamIdQuery, ExamSerializedDto>(new GetExamsByExamIdQuery(examId));
        }

        public void StoreCreatedExam(string exam)
        {
            _mediator.Send(new StoreCreatedExamCommand(exam));
        }

        public string GetExamTemplatesAsJsonByTeacherId(string id)
        {
            var result = _mediator.Fetch<GetExamTemplatesByTeacherIdQuery, List<ExamTemplate>>(
                    new GetExamTemplatesByTeacherIdQuery(id));

            return JsonConvert.SerializeObject(result.ToArray(),
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
        }

        public List<ActiveExamDto> GetActiveExamsByStudentId(string id)
        {
            var result =  _mediator.Fetch<GetActiveExamsQuery, List<ExamTemplate>>(new GetActiveExamsQuery(id));

            return _mapper.Map<List<ActiveExamDto>>(result);
        }

        public void ActivateExam(string id)
        {
            _mediator.Send(new ActivateExamTemlateByIdCommand(id));
        }

        public void EndExam(string id)
        {
            _mediator.Send(new EndExamTemplateByIdCommand(id));
        }

        public ExamForStudentDto StartExamByStudent(string studentId, string examId)
        {
            var result = _mediator.Fetch<GetExamForStudentQuery, ExamTemplate>(new GetExamForStudentQuery(examId, studentId));

            return _mapper.Map<ExamForStudentDto>(result);
        }

        public void StoreStudentExam(ExamDto exam)
        {
            _mediator.Send(new StoreStudentExamCommand(_mapper.Map<Exam>(exam)));
        }

        public QuestionDto[] GetQuestionsByExamIdTemplate(Guid examId)
        {
            return _mediator
                .Fetch<GetExamQuestionsByExamIdQuery, List<QuestionDto>>(new GetExamQuestionsByExamIdQuery(examId))
                .ToArray();
        }

        public void SetEndOfProcessing(Guid examId)
        {
            _mediator.Send(new SetFinishedProcessingByExamIdCommand(examId));
        }

        public Guid GetExamOwner(Guid examId)
        {
            return _mediator.Fetch<GetExamOwnerIdByExamIdQuery, Guid>(new GetExamOwnerIdByExamIdQuery(examId));
        }
    }
}