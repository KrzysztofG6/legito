﻿namespace Legito.Api.Core.Dto
{
    using System;
    using System.Collections.Generic;

    public class ExamSerializedDto
    {
        public ExamSerializedDto()
        {
            Answers = new List<AnswerSerializedDto>();
        }

        public Guid ExamId { get; set; }

        public List<AnswerSerializedDto> Answers { get; set; }
    }
}