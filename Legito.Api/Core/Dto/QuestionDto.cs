﻿namespace Legito.Api.Core.Dto
{
    using System;
    using Entity.ExamTemplate;

    public class QuestionDto
    {
        public Guid Id { get; set; }

        public string Text { get; set; }

        public ClosedQuestionsAnswer[] PossibleAnswers { get; set; }
    }
}