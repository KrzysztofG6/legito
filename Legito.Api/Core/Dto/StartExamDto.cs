﻿namespace Legito.Api.Core.Dto
{
    public class StartExamDto
    {
        public string ExamId { get; set; }

        public string StudentId { get; set; }
    }
}