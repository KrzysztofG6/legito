﻿namespace Legito.Api.Core.Dto
{
    using System;
    using System.Collections.Generic;

    public class ExamDto
    {
        public Guid ExamId { get; set; }

        public Guid StudentId { get; set; }

        public ICollection<AnswerDto> Answers { get; set; }
    }
}