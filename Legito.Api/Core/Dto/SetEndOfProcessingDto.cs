﻿namespace Legito.Api.Core.Dto
{
    public class SetEndOfProcessingDto
    {
        public string ExamId { get; set; }
    }
}