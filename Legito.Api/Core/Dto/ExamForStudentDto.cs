﻿namespace Legito.Api.Core.Dto
{
    using System;
    using Entity.ExamTemplate;

    public class ExamForStudentDto
    {
        public Guid Tag { get; set; }

        public string Title { get; set; }

        public ExamQuestion[] Questions { get; set; }
    }
}