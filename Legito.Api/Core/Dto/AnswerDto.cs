﻿namespace Legito.Api.Core.Dto
{
    using System;
    using Enum;

    public class AnswerDto
    {
        public Guid QuestionId { get; set; }

        public string StudentAnswer { get; set; }

        public AnswerType Type { get; set; }
    }
}