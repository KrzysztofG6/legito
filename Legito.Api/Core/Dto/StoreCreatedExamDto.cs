﻿namespace Legito.Api.Core.Dto
{
    public class StoreCreatedExamDto
    {
        public string Exam { get; set; }
    }
}