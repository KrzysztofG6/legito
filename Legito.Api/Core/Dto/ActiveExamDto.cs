﻿namespace Legito.Api.Core.Dto
{
    using System;

    public class ActiveExamDto
    {
        public string Tag { get; set; }

        public string Title { get; set; }

        public bool IsCompleted { get; set; }
    }
}