﻿namespace Legito.Api.Core.Dto
{
    using System;
    using Enum;

    public class AnswerSerializedDto
    {
        public Guid Id { get; set; }

        public Guid StudentId { get; set; }

        public Guid QuestionId { get; set; }

        public string StudentAnswer { get; set; }

        public AnswerType Type { get; set; }
    }
}