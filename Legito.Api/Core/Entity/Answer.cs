﻿namespace Legito.Api.Core.Entity
{
    using System;
    using Enum;

    public class Answer
    {
        public Guid Id { get; set; }

        public Guid QuestionId { get; set; }

        public string StudentAnswer { get; set; }

        public AnswerType Type { get; set; }
    }
}