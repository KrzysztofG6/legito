﻿namespace Legito.Api.Core.Entity
{
    using System;
    using System.Collections.Generic;

    public class Exam
    {
        public Exam()
        {
            Answers = new List<Answer>();
        }

        public Guid Id { get; set; }

        public Guid StudentId { get; set; }

        public List<Answer> Answers { get; set; }
    }
}