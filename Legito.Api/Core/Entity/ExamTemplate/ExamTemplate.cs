﻿namespace Legito.Api.Core.Entity.ExamTemplate
{
    using System;

    public class ExamTemplate
    {
        public Guid Tag { get; set; }

        public string Title { get; set; }

        public string[] StudentIds { get; set; }

        public ExamQuestion[] Questions { get; set; }

        public string OwnerId { get; set; }

        public bool IsActive { get; set; }

        public bool IsCompleted { get; set; }

        public bool IsProcessingCompleted { get; set; }
    }
}