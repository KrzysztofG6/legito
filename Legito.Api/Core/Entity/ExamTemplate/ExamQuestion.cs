﻿namespace Legito.Api.Core.Entity.ExamTemplate
{
    using System;

    public class ExamQuestion
    {
        public Guid Tag { get; set; }

        public string Text { get; set; }

        public int Type { get; set; }

        public ClosedQuestionsAnswer[] PossibleAnswers { get; set; }
    }
}