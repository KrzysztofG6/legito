﻿namespace Legito.Api.Core.Entity.ExamTemplate
{
    public class ClosedQuestionsAnswer
    {
        public string Answer { get; set; }

        public bool IsCorrect { get; set; }
    }
}