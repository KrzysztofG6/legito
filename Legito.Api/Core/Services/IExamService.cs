﻿namespace Legito.Api.Core.Services
{
    using System;
    using System.Collections.Generic;
    using Dto;
    using Entity;

    public interface IExamService
    {
        ExamSerializedDto GetSerializedExamById(Guid examId);

        void StoreCreatedExam(string exam);

        string GetExamTemplatesAsJsonByTeacherId(string id);

        List<ActiveExamDto> GetActiveExamsByStudentId(string id);

        void ActivateExam(string id);

        void EndExam(string id);

        ExamForStudentDto StartExamByStudent(string studentId, string examId);

        void StoreStudentExam(ExamDto exam);

        QuestionDto[] GetQuestionsByExamIdTemplate(Guid examId);

        void SetEndOfProcessing(Guid examId);

        Guid GetExamOwner(Guid examId);
    }
}