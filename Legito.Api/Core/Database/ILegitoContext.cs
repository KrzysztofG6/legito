﻿namespace Legito.Api.Core.Database
{
    using Raven.Client;

    public interface ILegitoContext
    {
        IDocumentStore Store { get; }
    }
}