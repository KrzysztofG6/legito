﻿namespace Legito.Api.Domain.Mapper.Profiles
{
    using AutoMapper;
    using Core.Dto;
    using Core.Entity;
    using Core.Entity.ExamTemplate;

    public class DtoProfile : Profile
    {
        public DtoProfile()
        {
            CreateMap<Answer, AnswerDto>();
            CreateMap<AnswerDto, Answer>();

            CreateMap<Exam, ExamDto>()
                .ForMember(x => x.Answers, m => m.MapFrom(p => p.Answers));
            CreateMap<ExamDto, Exam>()
                .ForMember(x => x.Id, m => m.MapFrom(p => p.ExamId))
                .ForMember(x => x.Answers, m => m.MapFrom(p => p.Answers));
            

            CreateMap<Answer, AnswerSerializedDto>();

            CreateMap<Exam, ExamSerializedDto>()
                .ForMember(x => x.Answers, m => m.MapFrom(p => p.Answers))
                .ForMember(x => x.ExamId, m => m.MapFrom(p => p.Id))
                .AfterMap((src, dest) =>
                {
                    foreach (var destAnswer in dest.Answers)
                    {
                        destAnswer.StudentId = src.StudentId;
                    }
                });

            CreateMap<ExamTemplate, ActiveExamDto>()
                .ForMember(x => x.Tag, m => m.MapFrom(p => p.Tag.ToString()))
                .ForMember(x => x.Title, m => m.MapFrom(p => p.Title))
                .ForMember(x => x.IsCompleted, m => m.MapFrom(p => p.IsCompleted));

            CreateMap<ExamTemplate, ExamForStudentDto>()
                .AfterMap((src, dest) =>
                {
                    foreach (var closedQuestion in dest.Questions)
                    {
                        if (closedQuestion.PossibleAnswers == null || closedQuestion.PossibleAnswers.Length == 0)
                        {
                            continue;
                        }

                        foreach (var closedQuestionPossibleAnswer in closedQuestion.PossibleAnswers)
                        {
                            closedQuestionPossibleAnswer.IsCorrect = false;
                        }
                    }
                });

            CreateMap<ExamQuestion, QuestionDto>()
                .ForMember(x => x.Id, m => m.MapFrom(p => p.Tag));
        }
    }
}