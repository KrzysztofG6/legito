﻿namespace Legito.Api.Domain.Mapper
{
    using AutoMapper;
    using Profiles;

    public class MapperFactory
    {
        public IMapper Create()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<DtoProfile>();
            });

            return config.CreateMapper();
        }
    }
}