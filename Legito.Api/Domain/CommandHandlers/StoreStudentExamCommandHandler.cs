﻿namespace Legito.Api.Domain.CommandHandlers
{
    using System;
    using Commands;
    using Core.Database;
    using KSolution.Cqrs.Core;
    public class StoreStudentExamCommandHandler : ICommandHandler<StoreStudentExamCommand>
    {
        private readonly ILegitoContext _legitoContext;

        public StoreStudentExamCommandHandler(ILegitoContext legitoContext)
        {
            _legitoContext = legitoContext;
        }

        public void Handle(StoreStudentExamCommand command)
        {
            using (var session = _legitoContext.Store.OpenSession())
            {
                foreach (var examAnswer in command.Exam.Answers)
                {
                    examAnswer.Id = Guid.NewGuid();
                }

                session.Store(command.Exam);
                session.SaveChanges();
            }
        }
    }
}