﻿namespace Legito.Api.Domain.CommandHandlers
{
    using System;
    using Commands;
    using Core.Database;
    using Core.Entity.ExamTemplate;
    using KSolution.Cqrs.Core;
    using Newtonsoft.Json;

    public class StoreCreatedExamCommandHandler : ICommandHandler<StoreCreatedExamCommand>
    {
        private readonly ILegitoContext _legitoContext;

        public StoreCreatedExamCommandHandler(ILegitoContext legitoContext)
        {
            _legitoContext = legitoContext;
        }

        public void Handle(StoreCreatedExamCommand command)
        {
            using (var session = _legitoContext.Store.OpenSession())
            {
                var deserialized = JsonConvert.DeserializeObject<ExamTemplate>(command.Exam);

                deserialized.Tag = Guid.NewGuid();
                deserialized.IsCompleted = false;
                deserialized.IsActive = false;

                foreach (var question in deserialized.Questions)
                {
                    question.Tag = Guid.NewGuid();
                }

                session.Store(deserialized);
                session.SaveChanges();
            }
        }
    }
}