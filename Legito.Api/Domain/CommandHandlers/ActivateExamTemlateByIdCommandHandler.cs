﻿namespace Legito.Api.Domain.CommandHandlers
{
    using System;
    using System.Linq;
    using Commands;
    using Core.Database;
    using Core.Entity.ExamTemplate;
    using KSolution.Cqrs.Core;
    using Microsoft.EntityFrameworkCore;

    public class ActivateExamTemlateByIdCommandHandler : ICommandHandler<ActivateExamTemlateByIdCommand>
    {
        private readonly ILegitoContext _context;

        public ActivateExamTemlateByIdCommandHandler(ILegitoContext context)
        {
            _context = context;
        }

        public void Handle(ActivateExamTemlateByIdCommand command)
        {
            using (var session = _context.Store.OpenSession())
            {
                var examTemplate = session.Load<ExamTemplate>(Guid.Parse(command.ExamId));

                if (examTemplate == null)
                {
                    throw new NullReferenceException($"Exam template for {command.ExamId} was not found!");
                }

                examTemplate.IsActive = true;

                session.SaveChanges();
            }
        }
    }
}