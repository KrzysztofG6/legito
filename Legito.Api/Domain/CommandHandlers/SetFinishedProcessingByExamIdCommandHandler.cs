﻿namespace Legito.Api.Domain.CommandHandlers
{
    using System;
    using Commands;
    using Core.Database;
    using Core.Entity.ExamTemplate;
    using KSolution.Cqrs.Core;

    public class SetFinishedProcessingByExamIdCommandHandler : ICommandHandler<SetFinishedProcessingByExamIdCommand>
    {
        private readonly ILegitoContext _context;

        public SetFinishedProcessingByExamIdCommandHandler(ILegitoContext context)
        {
            _context = context;
        }

        public void Handle(SetFinishedProcessingByExamIdCommand command)
        {
            using (var session = _context.Store.OpenSession())
            {
                var examTemplate = session.Load<ExamTemplate>(command.ExamId);

                if (examTemplate == null)
                {
                    throw new NullReferenceException($"Exam template for {command.ExamId} was not found!");
                }

                examTemplate.IsProcessingCompleted = true;

                session.SaveChanges();
            }
        }
    }
}