﻿namespace Legito.Api.Domain.CommandHandlers
{
    using System;
    using Commands;
    using Core.Database;
    using Core.Entity.ExamTemplate;
    using KSolution.Cqrs.Core;

    public class EndExamTemplateByIdCommandHandler : ICommandHandler<EndExamTemplateByIdCommand>
    {
        private readonly ILegitoContext _context;

        public EndExamTemplateByIdCommandHandler(ILegitoContext context)
        {
            _context = context;
        }

        public void Handle(EndExamTemplateByIdCommand command)
        {
            using(var session = _context.Store.OpenSession())
            {
                var examTemplate = session.Load<ExamTemplate>(Guid.Parse(command.Id));

                if (examTemplate == null)
                {
                    throw new NullReferenceException($"Exam template for {command.Id} was not found!");
                }

                examTemplate.IsActive = false;
                examTemplate.IsCompleted = true;

                session.SaveChanges();
            }
        }
    }
}