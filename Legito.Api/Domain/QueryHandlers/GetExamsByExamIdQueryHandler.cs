﻿namespace Legito.Api.Domain.QueryHandlers
{
    using System;
    using System.Linq;
    using AutoMapper;
    using Core.Database;
    using Core.Dto;
    using Core.Entity;
    using KSolution.Cqrs.Core;
    using Queries;
    using Raven.Client;
    using Raven.Client.Linq;

    public class GetExamsByExamIdQueryHandler : IQueryHandler<GetExamsByExamIdQuery, ExamSerializedDto>
    {
        private readonly ILegitoContext _context;
        private readonly IMapper _mapper;

        public GetExamsByExamIdQueryHandler(
            ILegitoContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ExamSerializedDto Handle(GetExamsByExamIdQuery query)
        {
            using (var session = _context.Store.OpenSession())
            {
                var exams = (
                    from exam in session.Query<Exam>()
                        .Include(x => x.Answers)
                    where exam.Id == query.ExamId
                    select exam).ToList();

                if (exams.Any())
                {
                    var serializedExam = new ExamSerializedDto
                    {
                        ExamId = query.ExamId
                    };
                    
                    foreach (var exam in exams)
                    {
                        serializedExam.Answers.AddRange(_mapper.Map<ExamSerializedDto>(exam).Answers.Where(x => !string.IsNullOrEmpty(x.StudentAnswer)));
                    }

                    return serializedExam;
                }

                throw new NullReferenceException($"Results for exam {query.ExamId} were not found");
            }
        }
    }
}