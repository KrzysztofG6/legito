﻿namespace Legito.Api.Domain.QueryHandlers
{
    using System.Collections.Generic;
    using System.Linq;
    using Core.Database;
    using Core.Entity.ExamTemplate;
    using KSolution.Cqrs.Core;
    using Queries;
    using Raven.Client;
    using Raven.Client.Linq;

    public class GetExamTemplatesByTeacherIdQueryHandler : IQueryHandler<GetExamTemplatesByTeacherIdQuery, List<ExamTemplate>>
    {
        private readonly ILegitoContext _context;

        public GetExamTemplatesByTeacherIdQueryHandler(ILegitoContext context)
        {
            _context = context;
        }

        public List<ExamTemplate> Handle(GetExamTemplatesByTeacherIdQuery query)
        {
            var templates = new List<ExamTemplate>();

            using (var session = _context.Store.OpenSession())
            {
                templates = (
                    from template in session.Query<ExamTemplate>()
                        .Include(x => x.Questions)
                        .Include(x => x.StudentIds)
                    where template.OwnerId == query.TeacherId
                    select template).ToList();
            }

            return templates;
        }
    }
}