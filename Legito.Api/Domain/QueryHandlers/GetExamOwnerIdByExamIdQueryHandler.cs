﻿namespace Legito.Api.Domain.QueryHandlers
{
    using System;
    using System.Linq;
    using Core.Database;
    using Core.Entity.ExamTemplate;
    using KSolution.Cqrs.Core;
    using Queries;
    using Raven.Client;
    using Raven.Client.Linq;

    public class GetExamOwnerIdByExamIdQueryHandler : IQueryHandler<GetExamOwnerIdByExamIdQuery, Guid>
    {
        private readonly ILegitoContext _context;

        public GetExamOwnerIdByExamIdQueryHandler(ILegitoContext context)
        {
            _context = context;
        }

        public Guid Handle(GetExamOwnerIdByExamIdQuery query)
        {
            using (var session = _context.Store.OpenSession())
            {
                var template = (
                    from exam in session.Query<ExamTemplate>()
                    where exam.Tag == query.ExamId
                    select exam).FirstOrDefault();

                if (template == null)
                {
                    throw new NullReferenceException($"Cannot find owner of exam {query.ExamId}");
                }

                return Guid.Parse(template.OwnerId);
            }
        }
    }
}