﻿namespace Legito.Api.Domain.QueryHandlers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Core.Database;
    using Core.Entity.ExamTemplate;
    using KSolution.Cqrs.Core;
    using Queries;
    using Raven.Client;
    using Raven.Client.Linq;

    public class GetExamForStudentQueryHandler : IQueryHandler<GetExamForStudentQuery, ExamTemplate>
    {
        public readonly ILegitoContext _context;

        public GetExamForStudentQueryHandler(ILegitoContext context)
        {
            _context = context;
        }

        public ExamTemplate Handle(GetExamForStudentQuery query)
        {
            using (var session = _context.Store.OpenSession())
            {
                var examTemplate = (
                    from template in session.Query<ExamTemplate>()
                        .Include(x => x.Questions)
                    where template.StudentIds.Contains(query.StudentId) && template.Tag == Guid.Parse(query.ExamId)
                    select template).FirstOrDefault();

                if (examTemplate == null)
                {
                    throw new NullReferenceException($"Couldnt find exam to start by id {query.ExamId} student id: {query.StudentId}");
                }

                return examTemplate;
            }
        }
    }
}