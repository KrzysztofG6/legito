﻿namespace Legito.Api.Domain.QueryHandlers
{
    using System.Collections.Generic;
    using System.Linq;
    using Core.Database;
    using Core.Entity.ExamTemplate;
    using KSolution.Cqrs.Core;
    using Queries;
    using Raven.Client;
    using Raven.Client.Linq;

    public class GetActiveExamsQueryHandler : IQueryHandler<GetActiveExamsQuery, List<ExamTemplate>>
    {
        private readonly ILegitoContext _context;

        public GetActiveExamsQueryHandler(ILegitoContext context)
        {
            _context = context;
        }

        public List<ExamTemplate> Handle(GetActiveExamsQuery query)
        {
            var templates = new List<ExamTemplate>();

            using (var session = _context.Store.OpenSession())
            {
                templates = (
                    from template in session.Query<ExamTemplate>()
                        .Include(x => x.StudentIds)
                    where template.StudentIds.Contains(query.StudentId) && template.IsActive
                    select template).ToList();
            }

            return templates;
        }
    }
}