﻿namespace Legito.Api.Domain.QueryHandlers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Core.Database;
    using Core.Dto;
    using Core.Entity.ExamTemplate;
    using KSolution.Cqrs.Core;
    using Queries;
    using Raven.Client;
    using Raven.Client.Linq;

    public class GetExamQuestionsByExamIdQueryHandler : IQueryHandler<GetExamQuestionsByExamIdQuery, List<QuestionDto>>
    {
        private readonly ILegitoContext _legitoContext;
        private readonly IMapper _mapper;

        public GetExamQuestionsByExamIdQueryHandler(ILegitoContext legitoContext,
            IMapper mapper)
        {
            _legitoContext = legitoContext;
            _mapper = mapper;
        }

        public List<QuestionDto> Handle(GetExamQuestionsByExamIdQuery query)
        {
            using (var session = _legitoContext.Store.OpenSession())
            {
                var examTemplate = (
                    from exam in session.Query<ExamTemplate>()
                        .Include(x => x.Questions)
                    where exam.Tag == query.ExamId
                    select exam).Single();

                if (examTemplate == null)
                {
                    throw new NullReferenceException($"Couldnt find questions for exam template id: {query.ExamId}");
                }

                return _mapper.Map<List<QuestionDto>>(examTemplate.Questions);
            }
        }
    }
}