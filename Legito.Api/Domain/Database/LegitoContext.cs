﻿namespace Legito.Api.Domain.Database
{
    using System;
    using System.IO;
    using System.Reflection;
    using Core.Database;
    using Microsoft.Extensions.Configuration;
    using Raven.Client;
    using Raven.Client.Document;

    public class LegitoContext : ILegitoContext
    {
        private static readonly Lazy<IDocumentStore> LazyStore =
            new Lazy<IDocumentStore>(() =>
            {
                var configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();

                var store = new DocumentStore
                {
                    Url = configuration.GetSection("RavenDatabase").GetSection("Address").Value,
                    DefaultDatabase = configuration.GetSection("RavenDatabase").GetSection("DatabaseName").Value
                };

                store.Conventions.FindIdentityProperty = memberInfo => memberInfo.Name == "Tag";
                store.Conventions.AllowQueriesOnId = true;

                return store.Initialize();
            });

        public IDocumentStore Store => LazyStore.Value;
    }
}