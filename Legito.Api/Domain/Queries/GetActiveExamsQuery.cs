﻿namespace Legito.Api.Domain.Queries
{
    using System.Collections.Generic;
    using Core.Entity.ExamTemplate;
    using KSolution.Cqrs.Core.Markers;

    public class GetActiveExamsQuery : IQuery<List<ExamTemplate>>
    {
        public GetActiveExamsQuery(string studentId)
        {
            this.StudentId = studentId;
        }

        public string StudentId { get; }
    }
}