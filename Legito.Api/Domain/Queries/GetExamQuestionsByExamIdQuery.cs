﻿namespace Legito.Api.Domain.Queries
{
    using System;
    using System.Collections.Generic;
    using Core.Dto;
    using KSolution.Cqrs.Core.Markers;

    public class GetExamQuestionsByExamIdQuery : IQuery<List<QuestionDto>>
    {
        public GetExamQuestionsByExamIdQuery(Guid examId)
        {
            this.ExamId = examId;
        }

        public Guid ExamId { get; }
    }
}