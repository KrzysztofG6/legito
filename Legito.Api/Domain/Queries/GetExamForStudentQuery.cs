﻿namespace Legito.Api.Domain.Queries
{
    using Core.Entity.ExamTemplate;
    using KSolution.Cqrs.Core.Markers;

    public class GetExamForStudentQuery : IQuery<ExamTemplate>
    {
        public GetExamForStudentQuery(string examId,
            string studentId)
        {
            this.ExamId = examId;
            this.StudentId = studentId;
        }

        public string ExamId { get; }

        public string StudentId { get; }
    }
}