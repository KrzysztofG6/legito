﻿namespace Legito.Api.Domain.Queries
{
    using System.Collections.Generic;
    using Core.Entity.ExamTemplate;
    using KSolution.Cqrs.Core.Markers;

    public class GetExamTemplatesByTeacherIdQuery : IQuery<List<ExamTemplate>>
    {
        public GetExamTemplatesByTeacherIdQuery(string teacherId)
        {
            this.TeacherId = teacherId;
        }

        public string TeacherId { get; }
    }
}