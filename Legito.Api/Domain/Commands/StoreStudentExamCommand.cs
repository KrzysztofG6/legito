﻿namespace Legito.Api.Domain.Commands
{
    using Core.Entity;
    using KSolution.Cqrs.Core.Markers;

    public class StoreStudentExamCommand : ICommand
    {
        public StoreStudentExamCommand(Exam exam)
        {
            Exam = exam;
        }

        public Exam Exam { get; private set; }
    }
}