﻿namespace Legito.Api.Domain.Commands
{
    using KSolution.Cqrs.Core.Markers;

    public class EndExamTemplateByIdCommand : ICommand
    {
        public EndExamTemplateByIdCommand(string id)
        {
            this.Id = id;
        }

        public string Id { get; }
    }
}