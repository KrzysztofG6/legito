﻿namespace Legito.Api.Domain.Commands
{
    using System;
    using KSolution.Cqrs.Core.Markers;

    public class SetFinishedProcessingByExamIdCommand : ICommand
    {
        public SetFinishedProcessingByExamIdCommand(Guid examId)
        {
            this.ExamId = examId;
        }

        public Guid ExamId { get; }
    }
}