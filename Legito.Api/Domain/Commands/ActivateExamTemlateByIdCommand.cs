﻿namespace Legito.Api.Domain.Commands
{
    using KSolution.Cqrs.Core.Markers;

    public class ActivateExamTemlateByIdCommand : ICommand
    {
        public ActivateExamTemlateByIdCommand(string examId)
        {
            this.ExamId = examId;
        }

        public string ExamId { get; }
    }
}