﻿using Microsoft.AspNetCore.Mvc;

namespace Legito.AuthorizationServer.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Core.Dto;
    using Core.Entities;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Newtonsoft.Json;

    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/User")]
    public class UserController : Controller
    {
        private readonly UserManager<User> _userManager;

        public UserController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        [HttpPost("RetriveId")]
        public async Task<IActionResult> RetriveUserId([FromBody]RetriveUserIdDto model)
        {
            var resultByName = await _userManager.FindByNameAsync(model.LoginOrEmail);

            if (resultByName != null)
            {
                return Ok(resultByName.Id);
            }

            var resultByEmail = await _userManager.FindByEmailAsync(model.LoginOrEmail);

            if (resultByEmail != null)
            {
                return Ok(resultByEmail.Id);
            }

            return NotFound();
        }

        [HttpPost("RetrieveLoginsAndIds")]
        public async Task<IActionResult> RetrieveLoginsByIds([FromBody]string[] ids)
        {
            var idsWithNames = new List<UserIdAndNameDto>();

            foreach (var id in ids)
            {
                var user = await _userManager.FindByIdAsync(id);

                idsWithNames.Add(new UserIdAndNameDto
                {
                    Id = Guid.Parse(id),
                    Name = user.UserName
                });
            }

            return Ok(JsonConvert.SerializeObject(idsWithNames));
        }
    }
}