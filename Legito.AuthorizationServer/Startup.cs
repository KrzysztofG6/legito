﻿namespace Legito.AuthorizationServer
{
    using System.Reflection;
    using AutoMapper;
    using Core.Configuration;
    using Core.Entities;
    using Domain.Database;
    using Domain.Mapper;
    using IdentityServer4.AccessTokenValidation;
    using IdentityServer4.AspNetIdentity;
    using IdentityServer4.Services;
    using KSolution.ServiceDiscovery.Core.Constants;
    using KSolution.ServiceDiscovery.Core.Enum;
    using KSolution.ServiceDiscovery.Core.Extensions;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Newtonsoft.Json.Serialization;
    using Services.ProfileService;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = "http://localhost:5000";
                    options.RequireHttpsMetadata = false;

                    options.ApiName = ServicesLookup.AuthorizationServer.Description();
                });

            services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));

            services.AddTransient<IMapper>(factory => new MapperFactory().Create());
            services.AddTransient<IProfileService, ProfileService>();
            
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            var connectionString = $"Server={ServiceDiscoveryConstants.DockerHostMachineIpAddress}\\SQL2017;Database=AuthorizationServer;User Id=Dev;Password=dev12345!;";

            services.AddDbContext<AuthorizationServerContext>(options =>
                options.UseSqlServer(connectionString));

            services.AddIdentity<User, IdentityRole>(opt =>
                {
                    opt.Password.RequireDigit = false;
                    opt.Password.RequireLowercase = false;
                    opt.Password.RequireNonAlphanumeric = false;
                    opt.Password.RequireUppercase = false;
                    opt.Password.RequiredLength = 5;
                })
                .AddEntityFrameworkStores<AuthorizationServerContext>()
                .AddDefaultTokenProviders();

            services.AddMvc()
                .AddJsonOptions(opt => opt.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver());

            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                //.AddConfigurationStore(options =>
                //{
                //    options.ConfigureDbContext = builder =>
                //        builder.UseSqlServer(connectionString,
                //            sql => sql.MigrationsAssembly(migrationsAssembly));
                //})
                //.AddOperationalStore(options =>
                //{
                //    options.ConfigureDbContext = builder =>
                //        builder.UseSqlServer(connectionString,
                //            sql => sql.MigrationsAssembly(migrationsAssembly));

                //    options.EnableTokenCleanup = true;
                //    options.TokenCleanupInterval = 30;
                //})
                .AddInMemoryApiResources(IdentityServerConfigurationData.GetApiResources())
                .AddInMemoryClients(IdentityServerConfigurationData.GetClients())
                .AddInMemoryIdentityResources(IdentityServerConfigurationData.GetIdentityResources())
                .AddCorsPolicyService<InMemoryCorsPolicyService>()
                .AddAspNetIdentity<User>()
                .AddProfileService<ProfileService>();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAnyRequests", policy =>
                {
                    policy.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials();
                });
            });
        }
        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseAuthentication();

            app.UseStaticFiles();
            app.UseConsulServiceRegistration();
            app.UseCors("AllowAnyRequests");

            app.UseIdentityServer();

            app.UseMvcWithDefaultRoute();
        }
    }
}
