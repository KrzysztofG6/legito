﻿namespace Legito.AuthorizationServer.Domain.Database
{
    using Core.Entities;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;

    public class AuthorizationServerContext : IdentityDbContext<User>
    {
        public DbSet<School> Schools { get; set; }

        public AuthorizationServerContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<School>()
                .HasKey(x => x.Id);
        }
    }
}