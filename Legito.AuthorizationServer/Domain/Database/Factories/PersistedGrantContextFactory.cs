﻿namespace Legito.AuthorizationServer.Domain.Database.Factories
{
    using System.IO;
    using System.Reflection;
    using IdentityServer4.EntityFramework.DbContexts;
    using IdentityServer4.EntityFramework.Options;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Design;
    using Microsoft.Extensions.Configuration;

    public class PersistedGrantContextFactory : IDesignTimeDbContextFactory<PersistedGrantDbContext>
    {
        public PersistedGrantDbContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            var builder = new DbContextOptionsBuilder<PersistedGrantDbContext>();
            var connectionString = configuration.GetConnectionString("IdentityServer");

            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            builder.UseSqlServer(connectionString, sql => sql.MigrationsAssembly(migrationsAssembly));
            
            var storeOptions = new OperationalStoreOptions
            {
                ConfigureDbContext = optionsBuilder =>
                    optionsBuilder.UseSqlServer(connectionString,
                        sql => sql.MigrationsAssembly(migrationsAssembly))
            };

            return new PersistedGrantDbContext(builder.Options, storeOptions);
        }
    }
}