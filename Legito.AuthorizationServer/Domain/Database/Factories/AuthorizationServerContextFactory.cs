﻿namespace Legito.AuthorizationServer.Domain.Database.Factories
{
    using System.IO;
    using System.Reflection;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Design;
    using Microsoft.Extensions.Configuration;

    public class AuthorizationServerContextFactory : IDesignTimeDbContextFactory<AuthorizationServerContext>
    {
        public AuthorizationServerContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            var builder = new DbContextOptionsBuilder<AuthorizationServerContext>();
            var connectionString = configuration.GetConnectionString("IdentityServer");
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            builder.UseSqlServer(connectionString, sql => sql.MigrationsAssembly(migrationsAssembly));

            return new AuthorizationServerContext(builder.Options);
        }
    }
}