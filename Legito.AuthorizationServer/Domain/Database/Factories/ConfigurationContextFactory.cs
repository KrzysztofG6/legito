﻿namespace Legito.AuthorizationServer.Domain.Database.Factories
{
    using System.IO;
    using System.Reflection;
    using IdentityServer4.EntityFramework.DbContexts;
    using IdentityServer4.EntityFramework.Options;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Design;
    using Microsoft.Extensions.Configuration;

    public class ConfigurationContextFactory : IDesignTimeDbContextFactory<ConfigurationDbContext>
    {
        public ConfigurationDbContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            var builder = new DbContextOptionsBuilder<ConfigurationDbContext>();
            var connectionString = configuration.GetConnectionString("IdentityServer");
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            builder.UseSqlServer(connectionString, sql => sql.MigrationsAssembly(migrationsAssembly));

            var storeOptions = new ConfigurationStoreOptions
            {
                ConfigureDbContext = optionsBuilder =>
                    optionsBuilder.UseSqlServer(connectionString,
                        sql => sql.MigrationsAssembly(migrationsAssembly))
            };

            return new ConfigurationDbContext(builder.Options, storeOptions);
        }
    }
}