﻿namespace Legito.AuthorizationServer.Domain.Database.Extensions
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using Core;
    using Core.Configuration;
    using Core.Entities;
    using IdentityModel;
    using IdentityServer4.EntityFramework.DbContexts;
    using IdentityServer4.EntityFramework.Mappers;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;

    public static class SeedExtensions
    {
        public static void SeedConfigurationDatabase(this IServiceProvider serviceProvider)
        {
            //todo validate if needed
            //Console.WriteLine("Seeding configuration db started...");
            //using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            //{
            //    scope.ServiceProvider.GetService<PersistedGrantDbContext>().Database.Migrate();

            //    var context = scope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();
            //    SeedData(context);
            //}
            SeedIdentityDatabase(serviceProvider);
            SeedAuthorizationServerContextDatabase(serviceProvider);
            Console.WriteLine("Seed completed");
        }

        private static void SeedContextDatabase(ConfigurationDbContext context)
        {
            if (!context.Clients.Any())
            {
                foreach (var client in IdentityServerConfigurationData.GetClients())
                {
                    context.Clients.Add(client.ToEntity());
                }
            }

            if (!context.ApiResources.Any())
            {
                foreach (var apiResource in IdentityServerConfigurationData.GetApiResources())
                {
                    context.ApiResources.Add(apiResource.ToEntity());
                }
            }

            if (!context.IdentityResources.Any())
            {
                foreach (var resource in IdentityServerConfigurationData.GetIdentityResources())
                {
                    context.IdentityResources.Add(resource.ToEntity());
                }
            }

            context.SaveChanges();
        }

        private static void SeedIdentityDatabase(IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var userMgr = scope.ServiceProvider.GetRequiredService<UserManager<User>>();

                var admin = userMgr.FindByNameAsync("admin").Result;
                if (admin == null)
                {
                    admin = new User
                    {
                        UserName = "admin"
                    };
                    var result = userMgr.CreateAsync(admin, "Admin123!").Result;
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }

                    result = userMgr.AddClaimsAsync(admin, new Claim[]
                    {
                        new Claim(JwtClaimTypes.Name, "admin"),
                        new Claim(JwtClaimTypes.GivenName, "admin"),
                        new Claim(JwtClaimTypes.FamilyName, "admin"),
                        new Claim(JwtClaimTypes.Email, "admin@legito.com"),
                        new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean),
                        new Claim(JwtClaimTypes.WebSite, "http://legito.com"),
                        new Claim(JwtClaimTypes.Address,
                            @"{ 'street_address': 'One Hacker Way', 'locality': 'Heidelberg', 'postal_code': 1337, 'country': 'Poland' }",
                            IdentityServer4.IdentityServerConstants.ClaimValueTypes.Json),
                        new Claim("isTeacher", true.ToString()), 
                    }).Result;
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }
                    Console.WriteLine("admin created");
                }
                else
                {
                    Console.WriteLine("admin already exists");
                }
            }
        }

        private static void SeedAuthorizationServerContextDatabase(IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<AuthorizationServerContext>();

                if (!context.Schools.Any())
                {
                    context.Schools.Add(new School
                    {
                        Name = "PJATK"
                    });
                }
            }
        }
    }
}