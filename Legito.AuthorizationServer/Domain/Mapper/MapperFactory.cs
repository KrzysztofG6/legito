﻿namespace Legito.AuthorizationServer.Domain.Mapper
{
    using AutoMapper;
    using Profiles;

    public class MapperFactory
    {
        public IMapper Create()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ViewModelProfile>();
            });

            return config.CreateMapper();
        }
    }
}