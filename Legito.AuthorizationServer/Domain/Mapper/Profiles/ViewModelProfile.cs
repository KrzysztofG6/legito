﻿namespace Legito.AuthorizationServer.Domain.Mapper.Profiles
{
    using AutoMapper;
    using Core.Entities;
    using Quickstart.Account;

    public class ViewModelProfile : Profile
    {
        public ViewModelProfile()
        {
            CreateMap<RegisterViewModel, User>()
                .ForMember(x => x.UserName, m => m.MapFrom(p => p.Login));
        }
    }
}