﻿namespace Legito.AuthorizationServer.Quickstart.Account
{
    public class RegisterViewModel
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public string VerifyPassword { get; set; }

        public string Email { get; set; }

        public string School { get; set; }

        public bool IsTeacher { get; set; }
    }
}