﻿namespace Legito.AuthorizationServer.Core.Configuration
{
    using System.Collections.Generic;
    using IdentityServer4;
    using IdentityServer4.Models;
    using KSolution.ServiceDiscovery.Core.Enum;
    using KSolution.ServiceDiscovery.Core.Extensions;

    public static class IdentityServerConfigurationData
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("LegitoApi", "LegitoApi"),
                new ApiResource("LegitoBackOfficeApi", "LegitoBackOfficeApi"),
                new ApiResource("LegitoProcessorCoordinatorApi", "LegitoProcessorCoordinatorApi"),
                new ApiResource("LegitoAuthorizationServer", "LegitoAuthorizationServer")
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = ServicesLookup.ProcessCoordinatorApi.Description(),
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedCorsOrigins = new[] {"http://localhost:8083"},
                    AllowedScopes =
                    {
                        "LegitoApi",
                        "LegitoBackOfficeApi",
                        "LegitoTextComparer"
                    }
                },
                new Client
                {
                    ClientId = ServicesLookup.BackofficeApi.Description(),
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedCorsOrigins = new[]
                        {"http://localhost:8082", "http://localhost:8083", "http://localhost:4200", "http://localhost:5000"},
                    AllowedScopes =
                    {
                        "LegitoApi",
                        "LegitoProcessorCoordinatorApi",
                        "LegitoAuthorizationServer"
                    }
                },
                new Client
                {
                    ClientId = ServicesLookup.ClientApi.Description(),
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedCorsOrigins = new[] {"http://localhost:8080"},
                    AllowedScopes =
                    {
                        "LegitoBackOfficeApi",
                        "LegitoProcessorCoordinatorApi"
                    }
                },
                new Client
                {
                    ClientId = "LegitoTextComparer",
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = {"LegitoProcessorCoordinatorApi"}
                },
                new Client
                {
                    ClientId = "LegitoBackOfficeClient",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,

                    RedirectUris = {"http://localhost:4200/"},
                    PostLogoutRedirectUris = {"http://localhost:4200/"},
                    AllowedCorsOrigins = {"http://localhost:4200/"},

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "LegitoBackOfficeApi"
                    },
                },
                new Client
                {
                    ClientId = "LegitoWikiTextComparer",
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = {"LegitoProcessorCoordinatorApi"}
                }
            };

        }
    }
}