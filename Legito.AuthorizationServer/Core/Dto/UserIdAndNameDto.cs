﻿namespace Legito.AuthorizationServer.Core.Dto
{
    using System;

    public class UserIdAndNameDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}