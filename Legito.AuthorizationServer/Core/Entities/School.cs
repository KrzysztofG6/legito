﻿namespace Legito.AuthorizationServer.Core.Entities
{
    using System;

    public class School
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}