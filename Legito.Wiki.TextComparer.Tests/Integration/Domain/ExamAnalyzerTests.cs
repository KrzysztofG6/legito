﻿namespace Legito.Wiki.TextComparer.Tests.Integration.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using Core.Model;
    using FluentAssertions;
    using TextComparer.Domain;
    using Xunit;

    public class ExamAnalyzerTests
    {
        private ExamAnalyzer examAnalyzer;
        private TextFormatter answerFormatter;
        private WikipediaSeeker wikipediaSeeker;

        public ExamAnalyzerTests()
        {
            answerFormatter = new TextFormatter();
            wikipediaSeeker = new WikipediaSeeker(new HttpClient());
            examAnalyzer = new ExamAnalyzer(answerFormatter, wikipediaSeeker);
        }

        [Fact]
        public void ShouldReturnOneResultForSqlServer()
        {
            var papers = new StudentPapers
            {
                Answers = new List<Answer>
                {
                    new Answer
                    {
                        Id = Guid.NewGuid(),
                        QuestionId = Guid.NewGuid(),
                        StudentAnswer = "system zarządzania bazą danych, wspierany i rozpowszechniany przez korporację Microsoft. Jest to główny produkt bazodanowy tej firmy, który charakteryzuje się tym, iż jako język zapytań używany jest przede wszystkim Transact-SQL, który stanowi rozwinięcie standardu ANSI/ISO. MS SQL Server jest platformą bazodanową typu klient-serwer. W stosunku do Microsoft Jet, który stosowany jest w programie MS Access, odznacza się lepszą wydajnością, niezawodnością i skalowalnością. Przede wszystkim są tu zaimplementowane wszelkie mechanizmy wpływające na bezpieczeństwo operacji(m.in.procedury wyzwalane).",
                        StudentId = Guid.NewGuid()
                    }
                },
                ExamId = Guid.NewGuid()
            };

            var results = examAnalyzer.AnalyzeExam(papers);

            //note this test is wikipedia dependent!!!
            results.ElementAt(0).StudentId.Should().Be(papers.Answers.First().StudentId);
            results.ElementAt(0).AnswerId.Should().Be(papers.Answers.First().Id);
            results.ElementAt(0).UrlsWithScore.Count.Should().Be(1);
            results.ElementAt(0).UrlsWithScore.ElementAt(0).Url.Should().Be("https://pl.wikipedia.org/wiki/Microsoft_SQL_Server");
            results.ElementAt(0).UrlsWithScore.ElementAt(0).Phrases.Count.Should().Be(23);
            results.ElementAt(0).UrlsWithScore.ElementAt(0).WikiTextNormalized.Should().NotBeEmpty();
        }

        [Fact]
        public void ShouldReturnNoneResultsForCustomAnswer()
        {
            var papers = new StudentPapers
            {
                Answers = new List<Answer>
                {
                    new Answer
                    {
                        Id = Guid.NewGuid(),
                        QuestionId = Guid.NewGuid(),
                        StudentAnswer = "system zarządzania bazą danych rozwijany i wspierany przez",
                        StudentId = Guid.NewGuid()
                    }
                },
                ExamId = Guid.NewGuid()
            };

            var results = examAnalyzer.AnalyzeExam(papers);

            //note this test is wikipedia dependent!!!
            results.ElementAt(0).StudentId.Should().Be(papers.Answers.First().StudentId);
            results.ElementAt(0).AnswerId.Should().Be(papers.Answers.First().Id);
            results.ElementAt(0).UrlsWithScore.Count.Should().Be(0);
        }
    }
}