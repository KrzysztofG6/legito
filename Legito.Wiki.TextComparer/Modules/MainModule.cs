﻿namespace Legito.Wiki.TextComparer.Modules
{
    using System.Net;
    using System.Net.Http;
    using Autofac;
    using DnsClient;
    using Domain;
    using Messaging;
    using Services;

    public class MainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<HttpClient>()
                .AsSelf()
                .SingleInstance();

            builder.RegisterType<WikipediaSeeker>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<TextFormatter>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<QueueManager>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<ExamAnalyzer>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<MessagingService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<ProcessorService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.Register(x => new LookupClient(IPAddress.Parse("192.168.65.2"), 8600))
                .As<IDnsQuery>()
                .SingleInstance();
        }
    }
}