﻿namespace Legito.Wiki.TextComparer.Messaging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Security.Authentication;
    using System.Text;
    using System.Threading.Tasks;
    using Core;
    using Core.Model;
    using DnsClient;
    using IdentityModel.Client;
    using Newtonsoft.Json;
    using RabbitMQ.Client;
    using RabbitMQ.Client.Events;

    public class QueueManager : IQueueManager
    {
        private const string QueueName = "WikiTextProcessingComparison";

        private readonly HttpClient _httpclient;
        private readonly IDnsQuery _dnsQuery;
        private readonly IProcessorService _processorService;

        public QueueManager(HttpClient httpclient,
            IDnsQuery dnsQuery,
            IProcessorService processorService)
        {
            _httpclient = httpclient;
            _dnsQuery = dnsQuery;
            _processorService = processorService;
        }

        public void Listen()
        {
            var factory = new ConnectionFactory
            {
                Uri = new Uri("amqp://guest:guest@192.168.65.2:5672/")
            };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(
                        QueueName,
                        durable: true,
                        exclusive: false,
                        autoDelete: false,
                        arguments: null);

                    channel.BasicQos(0, 1, false);

                    var consumer = new EventingBasicConsumer(channel);

                    Console.WriteLine("Waiting for messages...");

                    consumer.Received += (model, ea) =>
                    {
                        Console.WriteLine("Exam has been recived.");
                        var body = ea.Body;
                        var message = Encoding.UTF8.GetString(body);

                        var deserializedObject = JsonConvert.DeserializeObject<StudentPapers>(message);

                        var results = _processorService.ProcessExam(deserializedObject);

                        PostResultsToCoordinator(results, deserializedObject.ExamId);

                        channel.BasicAck(ea.DeliveryTag, false);
                    };
                    channel.BasicConsume(QueueName, false, consumer);

                    Console.ReadLine();
                }
            }
        }

        private void PostResultsToCoordinator(List<WikipediaAnalyzeResult> processingResults, Guid examId)
        {
            var isCheatingFound = IsCheatingFound(processingResults);

            var resolvedService = _dnsQuery.ResolveServiceAsync("service.consul", "LegitoProcessorCoordinatorApi").Result;

            var address = resolvedService.First().HostName;
            var port = resolvedService.First().Port;

            var postData = JsonConvert.SerializeObject(new
            {
                ExamId = examId,
                ProcessorType = 2,
                IsCheatingFound = isCheatingFound,
                WikipediaAnalyzeResults = isCheatingFound ? processingResults : null
            });

            var token = GetTokenForApi("LegitoWikiTextComparer", "LegitoProcessorCoordinatorApi").Result;
            _httpclient.SetBearerToken(token);
            var result = _httpclient.PostAsync($"http://{address}:{port}/api/PostProcessing/Answers", new StringContent(postData, Encoding.UTF8, "application/json")).Result;

            Console.WriteLine($"Exam {examId} was sent after processing");
        }

        private async Task<string> GetTokenForApi(string @from, string requestedApi)
        {
            var resolvedService = _dnsQuery.ResolveServiceAsync("service.consul", "LegitoAuthorizationServer").Result;

            var hostname = resolvedService.First().HostName.Last() == '.' ? resolvedService.First().HostName.Remove(resolvedService.First().HostName.Count() - 1) : resolvedService.First().HostName;

            var port = resolvedService.First().Port;

            var authServerAddress = $"http://{hostname}:{resolvedService.First().Port}";

            var discovery = new DiscoveryClient(authServerAddress)
            {
                Policy = new DiscoveryPolicy
                {
                    RequireHttps = false,
                    ValidateEndpoints = false
                }
            };

            var discoveryResult = await discovery.GetAsync();

            if (discoveryResult.IsError)
            {
                throw new NullReferenceException($"Discovery client error: {discoveryResult.Error}");
            }

            var tokenClient = new TokenClient(discoveryResult.TokenEndpoint, @from, "secret");
            var tokenResponse = await tokenClient.RequestClientCredentialsAsync(requestedApi);

            if (tokenResponse.IsError)
            {
                throw new AuthenticationException($"Could not revice token for {requestedApi}: {tokenResponse.Error}");
            }

            return tokenResponse.AccessToken;
        }

        private bool IsCheatingFound(IEnumerable<WikipediaAnalyzeResult> processingResults)
        {
            return processingResults.Any(x => x.UrlsWithScore.Any());
        }
    }
}