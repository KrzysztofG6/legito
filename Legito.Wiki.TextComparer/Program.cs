﻿namespace Legito.Wiki.TextComparer
{
    using System;
    using System.Collections.Generic;
    using Autofac;
    using Core;
    using Core.Model;
    using Modules;

    class Program
    {
        static void Main(string[] args)
        {
            var container = BuildContainer();

            using (var scope = container.BeginLifetimeScope())
            {
                var messagingService = scope.Resolve<IMessagingService>();

                messagingService.Listen();
            }
        }

        private static IContainer BuildContainer()
        {
            var containerbuilder = new ContainerBuilder();
            containerbuilder.RegisterModule<MainModule>();
            return containerbuilder.Build();
        }
    }
}
