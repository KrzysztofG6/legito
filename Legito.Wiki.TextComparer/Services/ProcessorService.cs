﻿namespace Legito.Wiki.TextComparer.Services
{
    using System.Collections.Generic;
    using Core;
    using Core.Model;

    public class ProcessorService : IProcessorService
    {
        private readonly IExamAnalyzer _examAnalyzer;

        public ProcessorService(IExamAnalyzer examAnalyzer)
        {
            _examAnalyzer = examAnalyzer;
        }

        public List<WikipediaAnalyzeResult> ProcessExam(StudentPapers exam)
        {
            return _examAnalyzer.AnalyzeExam(exam);
        }
    }
}