﻿namespace Legito.Wiki.TextComparer.Services
{
    using System.Runtime.CompilerServices;
    using Core;

    public class MessagingService : IMessagingService
    {
        private readonly IQueueManager _queueManager;

        public MessagingService(IQueueManager queueManager)
        {
            _queueManager = queueManager;
        }

        public void Listen()
        {
            _queueManager.Listen();
        }
    }
}