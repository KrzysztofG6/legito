﻿namespace Legito.Wiki.TextComparer.Core
{
    using System.Collections.Generic;

    public interface IWikipediaSeeker
    {
        string SearchPhrase(string phrase);

        string GetArticleUrlByPageId(double pageId);

        List<double> ExtractPageNoFromResponse(string response);

        string GetWikiPageText(string title, double pageId);

        string GetArticleTitleByPageId(double pageId);
    }
}