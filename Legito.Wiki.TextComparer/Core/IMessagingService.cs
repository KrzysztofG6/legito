﻿namespace Legito.Wiki.TextComparer.Core
{
    public interface IMessagingService
    {
        void Listen();
    }
}