﻿namespace Legito.Wiki.TextComparer.Core
{
    public interface IQueueManager
    {
        void Listen();
    }
}