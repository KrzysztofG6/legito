﻿namespace Legito.Wiki.TextComparer.Core
{
    using System.Collections.Generic;

    public interface ITextFormatter
    {
        List<string> GetSearchPhrases(string answer, int phraseLength);

        string NormalizeText(string text);

        string TruncateAtWord(string value, int length);
    }
}