﻿namespace Legito.Wiki.TextComparer.Core
{
    using System.Collections.Generic;
    using Model;

    public interface IProcessorService
    {
        List<WikipediaAnalyzeResult> ProcessExam(StudentPapers exam);
    }
}