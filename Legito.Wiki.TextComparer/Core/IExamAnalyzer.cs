﻿namespace Legito.Wiki.TextComparer.Core
{
    using System.Collections.Generic;
    using Model;

    public interface IExamAnalyzer
    {
        List<WikipediaAnalyzeResult> AnalyzeExam(StudentPapers exam);
    }
}