﻿namespace Legito.Wiki.TextComparer.Core.Model
{
    using System.Collections.Generic;

    public class UrlWithHitPercentage
    {
        public UrlWithHitPercentage()
        {
            Phrases = new List<string>();
        }

        public string Url { get; set; }

        public double Percentage { get; set; }

        public List<string> Phrases { get; set; }

        public string WikiTextNormalized { get; set; }
    }
}