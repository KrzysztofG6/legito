﻿namespace Legito.Wiki.TextComparer.Core.Model
{
    using System;
    using System.Collections.Generic;

    public class WikipediaAnalyzeResult
    {
        public Guid AnswerId { get; set; }

        public Guid StudentId { get; set; }

        public List<UrlWithHitPercentage> UrlsWithScore { get; set; }
    }
}