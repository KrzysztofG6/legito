﻿namespace Legito.Wiki.TextComparer.Core.Model
{
    using System;
    using System.Collections.Generic;

    public class StudentPapers
    {
        public Guid ExamId { get; set; }

        public IList<Answer> Answers { get; set; }
    }
}