﻿namespace Legito.Wiki.TextComparer.Domain
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Text.RegularExpressions;
    using Core;

    public class TextFormatter : ITextFormatter
    {
        private static readonly List<string> NormalizationCharHelpers = new List<string>
        {
            ",", ".", "/", "<", ">", ";", ":", "\"", "\'", "?",
            "[", "]", "{", "}", "|", "\\", "!", "@", "#", "$",
            "%", "^", "&", "*", "(", ")", "-", "_", "~", "`", "–"
        };

        private static readonly List<string> NormalizationPhraseHelper = new List<string>
        {
            "np",
            "m.in",
            "itd",
            "itp",
            "j.w"
        };

        public List<string> GetSearchPhrases(string text, int phraseLength)
        {
            var results = new List<string>();

            while (text.Length > 0)
            {
                var extractedPhrase = TruncateAtWord(text, phraseLength);
                
                results.Add(extractedPhrase.Trim());
                text = text.Remove(0, extractedPhrase.Length);
            }

            return results;
        }

        public string NormalizeText(string text)
        {
            var result = text;

            foreach (var phrase in NormalizationPhraseHelper)
            {
                result = result.Replace(phrase, " ");
            }

            foreach (var character in NormalizationCharHelpers)
            {
                result = result.Replace(character, "");
            }

            result = result.Replace("  ", " ");
            
            return Regex.Replace(result, @"\t|\n|\r", " ");
        }

        [SuppressMessage("ReSharper", "StringIndexOfIsCultureSpecific.2")]
        public string TruncateAtWord(string value, int length)
        {
            if (value == null || value.Length < length || value.IndexOf(' ', length) == -1)
                return value;

            return value.Substring(0, value.IndexOf(' ', length));
        }
    }
}