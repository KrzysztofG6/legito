﻿namespace Legito.Wiki.TextComparer.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net.Http;
    using System.Text.Encodings.Web;
    using System.Web;
    using Core;
    using Newtonsoft.Json.Linq;

    public class WikipediaSeeker : IWikipediaSeeker
    {
        private readonly HttpClient _httpClient;

        public WikipediaSeeker(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public string SearchPhrase(string phrase)
        {
            var queryResult = _httpClient.GetAsync($"https://pl.wikipedia.org/w/api.php?action=query&list=search&srsearch={phrase}&srwhat=text&format=json").Result;
            
            return queryResult.Content.ReadAsStringAsync().Result;
        }

        public string GetArticleUrlByPageId(double pageId)
        {
            var queryResult = _httpClient.GetAsync($"https://pl.wikipedia.org/w/api.php?action=query&prop=info&pageids={pageId}&inprop=url&format=json").Result;

            var parsedResponse = JObject.Parse(queryResult.Content.ReadAsStringAsync().Result);

            var url = parsedResponse["query"]["pages"][pageId.ToString(CultureInfo.InvariantCulture)]["fullurl"];
            
            return url == null ? string.Empty : url.ToString();
        }

        public List<double> ExtractPageNoFromResponse(string response)
        {
            var result = new List<double>();

            var parsed = JObject.Parse(response);
            var searchResults = parsed["query"]["search"];

            foreach (var searchResult in searchResults)
            {
                result.Add(Convert.ToDouble(searchResult["pageid"]));
            }

            return result;
        }

        public string GetArticleTitleByPageId(double pageId)
        {
            var queryResult = _httpClient.GetAsync($"https://pl.wikipedia.org/w/api.php?action=query&prop=info&pageids={pageId}&inprop=url&format=json").Result;

            var parsedResponse = JObject.Parse(queryResult.Content.ReadAsStringAsync().Result);

            var title = parsedResponse["query"]["pages"][pageId.ToString(CultureInfo.InvariantCulture)]["title"];

            return string.IsNullOrEmpty(title.ToString()) ? string.Empty : title.ToString();
        }

        public string GetWikiPageText(string title, double pageId)
        {
            var queryResult = _httpClient.GetAsync($"https://pl.wikipedia.org/w/api.php?action=query&prop=extracts&format=json&explaintext=&exsectionformat=plain&exlimit=1&titles={title}").Result;

            var parsedResponse = JObject.Parse(queryResult.Content.ReadAsStringAsync().Result);

            var pageText = parsedResponse["query"]["pages"][pageId.ToString(CultureInfo.InvariantCulture)]["extract"];

            return pageText == null ? string.Empty : pageText.ToString();
        }
    }
}