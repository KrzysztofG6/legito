﻿namespace Legito.Wiki.TextComparer.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Core;
    using Core.Model;

    public class ExamAnalyzer : IExamAnalyzer
    {
        private readonly ITextFormatter _textFormatter;
        private readonly IWikipediaSeeker _wikipediaSeeker;

        public ExamAnalyzer(ITextFormatter textFormatter,
            IWikipediaSeeker wikipediaSeeker)
        {
            _textFormatter = textFormatter;
            _wikipediaSeeker = wikipediaSeeker;
        }

        public List<WikipediaAnalyzeResult> AnalyzeExam(StudentPapers exam)
        {
            var analyzeResults = new List<WikipediaAnalyzeResult>();

            foreach (var examAnswer in exam.Answers)
            {
                if (string.IsNullOrEmpty(examAnswer.StudentAnswer)) continue;

                var phrasesToSearch = _textFormatter.GetSearchPhrases(examAnswer.StudentAnswer, 250);

                analyzeResults.Add(new WikipediaAnalyzeResult
                {
                    AnswerId = examAnswer.Id,
                    StudentId = examAnswer.StudentId,
                    UrlsWithScore = GetWikiUrlsByPhrasesWithHitPercentages(phrasesToSearch)
                });
            }

            return analyzeResults;
        }

        private List<UrlWithHitPercentage> GetWikiUrlsByPhrasesWithHitPercentages(List<string> phrasesToSearch)
        {
            var linkWithPercentages = new List<UrlWithHitPercentage>();

            foreach (var phrase in phrasesToSearch)
            {
                var response = _wikipediaSeeker.SearchPhrase(phrase);
                var pageNumbers = _wikipediaSeeker.ExtractPageNoFromResponse(response);

                PopulateUrlResultsWithCorrespodingSearchPercentages(pageNumbers, phrase, linkWithPercentages);
            }

            var distinctUrls = linkWithPercentages.Select(x => x.Url).Distinct();

            return AggregateUrlPercentageResults(distinctUrls, linkWithPercentages);
        }

        private List<UrlWithHitPercentage> AggregateUrlPercentageResults(IEnumerable<string> distinctUrls, List<UrlWithHitPercentage> linkWithPercentages)
        {
            var aggregatedResults = new List<UrlWithHitPercentage>();

            foreach (var url in distinctUrls)
            {
                double percentages = 0;
                var itemCount = 0;
                var phrasesAggregated = new List<string>();
                var wikiTextNormalized = string.Empty;

                foreach (var linkWithPercentage in linkWithPercentages)
                {
                    if (linkWithPercentage.Url == url)
                    {
                        percentages += linkWithPercentage.Percentage;
                        itemCount += 1;
                        phrasesAggregated.AddRange(linkWithPercentage.Phrases);
                        wikiTextNormalized = string.IsNullOrEmpty(wikiTextNormalized)
                            ? linkWithPercentage.WikiTextNormalized
                            : wikiTextNormalized;
                    }
                }

                var aggregatedPercentage = Math.Round(percentages / itemCount, 2);

                if(aggregatedPercentage <= 0.3) continue; // we dont want results below 30% hit as its most probably false positive

                aggregatedResults.Add(new UrlWithHitPercentage
                {
                    Url = url,
                    Percentage = aggregatedPercentage,
                    Phrases = phrasesAggregated,
                    WikiTextNormalized = wikiTextNormalized
                });
            }

            return aggregatedResults;
        }

        private void PopulateUrlResultsWithCorrespodingSearchPercentages(List<double> pageNumbers, string phrase, List<UrlWithHitPercentage> linkWithPercentages)
        {
            foreach (var pageNumber in pageNumbers)
            {
                var pageTitle = _wikipediaSeeker.GetArticleTitleByPageId(pageNumber);
                var wikiText = _wikipediaSeeker.GetWikiPageText(pageTitle, pageNumber);

                if(string.IsNullOrEmpty(pageTitle) || string.IsNullOrEmpty(wikiText)) continue;

                var wikiTextNormalized = _textFormatter.NormalizeText(wikiText);
                var phraseNormalized = _textFormatter.NormalizeText(phrase);

                var splitPhrases = _textFormatter.GetSearchPhrases(phraseNormalized, 20);

                var hitCount = 0;

                foreach (var part in splitPhrases)
                {
                    if(!part.Contains(" ")) continue; // if its just one word - skip

                    var isFound = wikiTextNormalized.Contains(part);

                    if (isFound)
                    {
                        hitCount += 1;
                    }
                }

                var hitPercentage = (double)hitCount / splitPhrases.Count;

                linkWithPercentages.Add(new UrlWithHitPercentage
                {
                    Url = _wikipediaSeeker.GetArticleUrlByPageId(pageNumber),
                    Percentage = hitPercentage,
                    Phrases = splitPhrases,
                    WikiTextNormalized = wikiTextNormalized
                });
            }
        }
    }
}