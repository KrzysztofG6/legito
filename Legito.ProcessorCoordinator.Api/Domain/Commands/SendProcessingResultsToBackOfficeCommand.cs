﻿namespace Legito.ProcessorCoordinator.Api.Domain.Commands
{
    using System;
    using KSolution.Cqrs.Core.Markers;

    public class SendProcessingResultsToBackOfficeCommand : ICommand
    {
        public SendProcessingResultsToBackOfficeCommand(Guid examId)
        {
            ExamId = examId;
        }

        public Guid ExamId { get; private set; }
    }
}