﻿namespace Legito.ProcessorCoordinator.Api.Domain.Commands
{
    using Core.Entities;
    using KSolution.Cqrs.Core.Markers;

    public class CheckExamProcessingStatusCommand : ICommand
    {
        public CheckExamProcessingStatusCommand(ExamProcessingStatus examProcessingStatus)
        {
            ExamProcessingStatus = examProcessingStatus;
        }

        public ExamProcessingStatus ExamProcessingStatus { get; private set; }
    }
}