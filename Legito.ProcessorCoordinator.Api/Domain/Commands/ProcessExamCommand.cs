﻿namespace Legito.ProcessorCoordinator.Api.Domain.Commands
{
    using System;
    using KSolution.Cqrs.Core.Markers;

    public class ProcessExamCommand : ICommand
    {
        public ProcessExamCommand(Guid examId)
        {
            this.ExamId = examId;
        }

        public Guid ExamId { get; private set; }
    }
}