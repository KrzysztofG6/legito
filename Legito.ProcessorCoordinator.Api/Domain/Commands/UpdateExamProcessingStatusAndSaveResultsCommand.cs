﻿namespace Legito.ProcessorCoordinator.Api.Domain.Commands
{
    using Core.Dto;
    using KSolution.Cqrs.Core.Markers;

    public class UpdateExamProcessingStatusAndSaveResultsCommand : ICommand
    {
        public UpdateExamProcessingStatusAndSaveResultsCommand(PostProcessingDto postProcessing)
        {
            this.PostProcessing = postProcessing;
        }

        public PostProcessingDto PostProcessing { get; private set; }
    }
}