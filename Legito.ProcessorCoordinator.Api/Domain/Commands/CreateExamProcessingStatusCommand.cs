﻿namespace Legito.ProcessorCoordinator.Api.Domain.Commands
{
    using System;
    using KSolution.Cqrs.Core.Markers;

    public class CreateExamProcessingStatusCommand : ICommand
    {
        public CreateExamProcessingStatusCommand(Guid examId)
        {
            this.ExamId = examId;
        }

        public Guid ExamId { get; private set; }
    }
}