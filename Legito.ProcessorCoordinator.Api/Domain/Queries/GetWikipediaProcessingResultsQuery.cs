﻿namespace Legito.ProcessorCoordinator.Api.Domain.Queries
{
    using System;
    using System.Collections.Generic;
    using Core.Entities;
    using KSolution.Cqrs.Core.Markers;

    public class GetWikipediaProcessingResultsQuery : IQuery<List<WikipediaProcessingResult>>
    {
        public GetWikipediaProcessingResultsQuery(Guid examId)
        {
            ExamId = examId;
        }

        public Guid ExamId { get; }
    }
}