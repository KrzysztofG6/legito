﻿namespace Legito.ProcessorCoordinator.Api.Domain.Queries
{
    using System;
    using Core.Entities;
    using KSolution.Cqrs.Core.Markers;
    public class GetExamProcessingStatusByExamIdQuery : IQuery<ExamProcessingStatus>
    {
        public GetExamProcessingStatusByExamIdQuery(Guid examId)
        {
            ExamId = examId;
        }

        public Guid ExamId { get; private set; }
    }
}