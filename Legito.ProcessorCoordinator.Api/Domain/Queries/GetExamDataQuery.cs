﻿namespace Legito.ProcessorCoordinator.Api.Domain.Queries
{
    using System;
    using Core.Models;
    using KSolution.Cqrs.Core.Markers;

    public class GetExamDataQuery : IQuery<Exam>
    {
        public GetExamDataQuery(Guid examId)
        {
            this.ExamId = examId;
        }

        public Guid ExamId { get; private set; }
    }
}