﻿namespace Legito.ProcessorCoordinator.Api.Domain.Queries
{
    using System;
    using System.Collections.Generic;
    using Core.Entities;
    using KSolution.Cqrs.Core.Markers;
    public class GetProcessingResultsByExamIdQuery : IQuery<IList<ProcessingResult>>
    {
        public GetProcessingResultsByExamIdQuery(Guid examId)
        {
            ExamId = examId;
        }

        public Guid ExamId { get; private set; }
    }
}