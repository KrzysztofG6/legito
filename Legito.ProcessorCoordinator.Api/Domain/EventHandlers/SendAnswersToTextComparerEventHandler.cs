﻿namespace Legito.ProcessorCoordinator.Api.Domain.EventHandlers
{
    using Core.Messaging;
    using Core.Models;
    using Events;
    using KSolution.Cqrs.Core;
    using Microsoft.Extensions.Configuration;

    public class SendAnswersToTextComparerEventHandler : IEventHandler<SendAnswersToTextProcessorsEvent>
    {
        private readonly IMessagesQueueManager<Exam> textMessagesQueueManager;
        private readonly IConfiguration configuration;

        public SendAnswersToTextComparerEventHandler(
            IMessagesQueueManager<Exam> textMessagesQueueManager,
            IConfiguration configuration)
        {
            this.textMessagesQueueManager = textMessagesQueueManager;
            this.configuration = configuration;
        }

        public void Handle(SendAnswersToTextProcessorsEvent @event)
        {
            textMessagesQueueManager.Publish(configuration.GetSection("MessagingQueueStrings").GetSection("TextComparer").Value, @event.Exam);
        }
    }
}