﻿namespace Legito.ProcessorCoordinator.Api.Domain.EventHandlers
{
    using System.Linq;
    using Core.Messaging;
    using Core.Models;
    using Events;
    using KSolution.Cqrs.Core;
    using Microsoft.Extensions.Configuration;

    public class SendAnswersToWikipediaTextComparerEventHandler : IEventHandler<SendAnswersToTextProcessorsEvent>
    {
        private readonly IMessagesQueueManager<Exam> _textMessagesQueueManager;
        private readonly IConfiguration _configuration;

        public SendAnswersToWikipediaTextComparerEventHandler(IMessagesQueueManager<Exam> textMessagesQueueManager,
            IConfiguration configuration)
        {
            _textMessagesQueueManager = textMessagesQueueManager;
            _configuration = configuration;
        }

        public void Handle(SendAnswersToTextProcessorsEvent @event)
        {
            _textMessagesQueueManager.Publish(_configuration.GetSection("MessagingQueueStrings").GetSection("WikipediaTextComparer").Value, @event.Exam);
        }
    }
}