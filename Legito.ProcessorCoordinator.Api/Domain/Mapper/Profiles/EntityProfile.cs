﻿namespace Legito.ProcessorCoordinator.Api.Domain.Mapper.Profiles
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Core.Dto;
    using Core.Entities;
    using Core.Models;

    public class EntityProfile : Profile
    {
        public EntityProfile()
        {
            CreateMap<TextAnswerComparison, ProcessingResult>()
                .ForMember(x => x.Id, m => m.MapFrom(p => p.SearchAgainst.Id))
                .ForMember(x => x.QuestionId, m => m.MapFrom(p => p.SearchAgainst.QuestionId))
                .ForMember(x => x.StudentId, m => m.MapFrom(p => p.SearchAgainst.StudentId));

            CreateMap<ProcessingResult, TextComparerResultsDto>()
                .ForMember(x => x.DuplicateAnswers, opt => opt.MapFrom(p => new List<Guid>()))
                .AfterMap((src, dest) =>
                {
                    foreach (var duplicate in src.DuplicateAnswers)
                    {
                        dest.DuplicateAnswers.Add(duplicate.Id);
                        dest.DuplicateAnswers.Add(duplicate.ProcessingResult.Id);
                    }
                });

            CreateMap<WikipdiaUrlWithScore, WikipediaUrlDto>()
                .ForMember(x => x.Percentage, m => m.MapFrom(p => p.Percentge))
                .ForMember(x => x.Phrases, m => m.MapFrom(p => DecodeListInString(p.Phrases)));
            CreateMap<WikipediaUrlDto, WikipdiaUrlWithScore>()
                .ForMember(x => x.Id, m => m.Ignore())
                .ForMember(x => x.Percentge, m => m.MapFrom(p => p.Percentage))
                .ForMember(x => x.WikipediaProcessingResult, m => m.Ignore())
                .ForMember(x => x.Phrases, m => m.MapFrom(p => string.Join(';', p.Phrases)));

            CreateMap<WikipediaAnalyzeResult, WikipediaProcessingResult>()
                .ForMember(x => x.UrlsWithScore, m => m.MapFrom(p => p.UrlsWithScore))
                .AfterMap((src, dest) =>
                {
                    foreach (var url in dest.UrlsWithScore)
                    {
                        url.WikipediaProcessingResult = dest;
                    }
                });

            CreateMap<WikipediaProcessingResult, WikipediaAnalyzeResult>()
                .ForMember(x => x.UrlsWithScore, m => m.MapFrom(p => p.UrlsWithScore));
            
            CreateMap<WikipediaProcessingResult, WikipediaProcessingResultDto>();
        }

        private List<string> DecodeListInString(string encodedString)
        {
            return encodedString.Split(';').ToList();
        }
    }
}