﻿namespace Legito.ProcessorCoordinator.Api.Domain.Mapper
{
    using AutoMapper;
    using Profiles;

    public class MapperFactory
    {
        public IMapper Create()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<EntityProfile>();
            });

            return config.CreateMapper();
        }
    }
}