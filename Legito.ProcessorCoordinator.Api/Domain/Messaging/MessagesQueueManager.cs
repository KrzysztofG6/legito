﻿namespace Legito.ProcessorCoordinator.Api.Domain.Messaging
{
    using System.Text;
    using Core.Messaging;
    using KSolution.ServiceDiscovery.Core.Constants;
    using Microsoft.Extensions.Configuration;
    using Newtonsoft.Json;
    using RabbitMQ.Client;

    public class MessagesQueueManager<T> : IMessagesQueueManager<T>
    {
        private readonly IConfiguration configuration;

        public MessagesQueueManager(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public void Publish(string queueName, T exam)
        {
            var factory = new ConnectionFactory()
            {
                HostName = ServiceDiscoveryConstants.DockerHostMachineIpAddress
            };
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: queueName,
                        durable: true,
                        exclusive: false,
                        autoDelete: false,
                        arguments: null);

                    var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(exam));

                    var properties = channel.CreateBasicProperties();
                    properties.Persistent = true;

                    channel.BasicPublish(exchange: "",
                        routingKey: queueName,
                        basicProperties: properties,
                        body: body);
                }
            }
        }
    }
}