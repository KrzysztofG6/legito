﻿namespace Legito.ProcessorCoordinator.Api.Domain.QueryHandlers
{
    using System.Collections.Generic;
    using System.Linq;
    using Core.Database;
    using Core.Entities;
    using KSolution.Cqrs.Core;
    using Microsoft.EntityFrameworkCore;
    using Queries;

    public class GetWikipediaProcessingResultsQueryHandler : IQueryHandler<GetWikipediaProcessingResultsQuery, List<WikipediaProcessingResult>>
    {
        private readonly IProcessorCoordinatorContext _processorCoordinatorContext;

        public GetWikipediaProcessingResultsQueryHandler(IProcessorCoordinatorContext processorCoordinatorContext)
        {
            _processorCoordinatorContext = processorCoordinatorContext;
        }

        public List<WikipediaProcessingResult> Handle(GetWikipediaProcessingResultsQuery query)
        {
            return _processorCoordinatorContext.WikipediaProcessingResults.Where(x => x.ExamId == query.ExamId).Include(x => x.UrlsWithScore).ToList();
        }
    }
}