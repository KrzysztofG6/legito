﻿namespace Legito.ProcessorCoordinator.Api.Domain.QueryHandlers
{
    using System;
    using System.Linq;
    using Core.Database;
    using Core.Entities;
    using KSolution.Cqrs.Core;
    using Queries;

    public class GetExamProcessingStatusByExamIdQueryHandler : IQueryHandler<GetExamProcessingStatusByExamIdQuery, ExamProcessingStatus>
    {
        private readonly IProcessorCoordinatorContext _processorCoordinatorContext;

        public GetExamProcessingStatusByExamIdQueryHandler(IProcessorCoordinatorContext processorCoordinatorContext)
        {
            _processorCoordinatorContext = processorCoordinatorContext;
        }

        public ExamProcessingStatus Handle(GetExamProcessingStatusByExamIdQuery query)
        {
            var result = _processorCoordinatorContext.ExamProcessingStatuses.FirstOrDefault(x => x.ExamId == query.ExamId);

            if (result == null)
            {
                throw new NullReferenceException($"There was no exam processing status found for exam: {query.ExamId}");
            }

            return result;
        }
    }
}