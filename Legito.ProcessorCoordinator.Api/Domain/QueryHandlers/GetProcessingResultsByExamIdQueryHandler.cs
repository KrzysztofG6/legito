﻿namespace Legito.ProcessorCoordinator.Api.Domain.QueryHandlers
{
    using System.Collections.Generic;
    using System.Linq;
    using Core.Database;
    using Core.Entities;
    using KSolution.Cqrs.Core;
    using Microsoft.EntityFrameworkCore;
    using Queries;

    public class GetProcessingResultsByExamIdQueryHandler : IQueryHandler<GetProcessingResultsByExamIdQuery, IList<ProcessingResult>>
    {
        private readonly IProcessorCoordinatorContext _processorCoordinatorContext;

        public GetProcessingResultsByExamIdQueryHandler(IProcessorCoordinatorContext processorCoordinatorContext)
        {
            _processorCoordinatorContext = processorCoordinatorContext;
        }

        public IList<ProcessingResult> Handle(GetProcessingResultsByExamIdQuery query)
        {
            return _processorCoordinatorContext.ProcessingResults.Where(x => x.ExamId == query.ExamId).Include(y => y.DuplicateAnswers).ToList();
        }
    }
}