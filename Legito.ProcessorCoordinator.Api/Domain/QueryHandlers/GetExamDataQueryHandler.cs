﻿namespace Legito.ProcessorCoordinator.Api.Domain.QueryHandlers
{
    using System;
    using System.Net.Http;
    using Core.Models;
    using KSolution.Cqrs.Core;
    using KSolution.ServiceDiscovery.Core.Enum;
    using KSolution.ServiceDiscovery.Core.Services;
    using Newtonsoft.Json;
    using Queries;

    public class GetExamDataQueryHandler : IQueryHandler<GetExamDataQuery, Exam>
    {
        private readonly IConsulService _consulService;
        private readonly HttpClient _httpClient;
        private readonly ITokenProviderService _tokenProviderService;

        public GetExamDataQueryHandler(IConsulService consulService,
            HttpClient httpClient, 
            ITokenProviderService tokenProviderService)
        {
            _consulService = consulService;
            _httpClient = httpClient;
            _tokenProviderService = tokenProviderService;
        }

        public Exam Handle(GetExamDataQuery query)
        {
            _httpClient.SetBearerToken(_tokenProviderService.GetTokenForApi(ServicesLookup.ProcessCoordinatorApi, ServicesLookup.ClientApi).Result);
            var response = _httpClient.GetAsync($"http://{_consulService.GetServiceAddress(ServicesLookup.ClientApi)}/api/Exam/GetResultById/" + query.ExamId).Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"Failed to get reponse from {ServicesLookup.ClientApi} for exam: {query.ExamId}");
            }

            var result = response.Content.ReadAsStringAsync().Result;

            if (string.IsNullOrEmpty(result))
            {
                throw new NullReferenceException($"Data of exam {query.ExamId} was empty");
            }

            return JsonConvert.DeserializeObject<Exam>(result);
        }
    }
}