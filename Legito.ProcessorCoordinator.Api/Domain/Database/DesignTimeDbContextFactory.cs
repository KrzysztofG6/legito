﻿namespace Legito.ProcessorCoordinator.Api.Domain.Database
{
    using System.IO;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Design;
    using Microsoft.Extensions.Configuration;

    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<ProcessorCoordinatorContext>
    {
        public ProcessorCoordinatorContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            var builder = new DbContextOptionsBuilder<ProcessorCoordinatorContext>();
            var connectionString = configuration.GetConnectionString("ProcessorCoordinator");
            builder.UseSqlServer(connectionString);
            return new ProcessorCoordinatorContext(builder.Options);
        }
    }
}