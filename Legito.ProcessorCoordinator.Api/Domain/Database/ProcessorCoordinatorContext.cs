﻿namespace Legito.ProcessorCoordinator.Api.Domain.Database
{
    using System;
    using System.Threading.Tasks;
    using Core.Database;
    using Core.Entities;
    using Microsoft.EntityFrameworkCore;

    public class ProcessorCoordinatorContext : DbContext, IProcessorCoordinatorContext
    {
        public ProcessorCoordinatorContext(DbContextOptions<ProcessorCoordinatorContext> options)
            : base(options)
        {
        }

        public DbSet<ExamProcessingStatus> ExamProcessingStatuses { get; set; }

        public DbSet<ProcessingResult> ProcessingResults { get; set; }

        public DbSet<DuplicateAnswer> DuplicateAnswers { get; set; }

        public DbSet<WikipediaProcessingResult> WikipediaProcessingResults { get; set; }

        public DbSet<WikipdiaUrlWithScore> WikipdiaUrlsWithScore { get;set; }

        public Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }

        public bool IsDatabaseReachable()
        {
            try
            {
                this.Database.OpenConnection();
                this.Database.CloseConnection();
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("pca");

            modelBuilder.Entity<ExamProcessingStatus>()
                .ToTable("ExamProcessingStatus")
                .HasKey(x => x.ExamId);

            modelBuilder.Entity<ProcessingResult>()
                .HasMany(x => x.DuplicateAnswers)
                .WithOne(x => x.ProcessingResult);

            modelBuilder.Entity<DuplicateAnswer>()
                .HasOne(x => x.ProcessingResult)
                .WithMany(x => x.DuplicateAnswers);

            modelBuilder.Entity<WikipdiaUrlWithScore>()
                .HasOne(x => x.WikipediaProcessingResult)
                .WithMany(x => x.UrlsWithScore);
                

            modelBuilder.Entity<WikipediaProcessingResult>()
                .HasMany(x => x.UrlsWithScore)
                .WithOne(x => x.WikipediaProcessingResult);
        }
    }
}