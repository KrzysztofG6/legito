﻿namespace Legito.ProcessorCoordinator.Api.Domain.Events
{
    using Core.Models;
    using KSolution.Cqrs.Core.Markers;

    public class SendAnswersToTextProcessorsEvent : IEvent
    {
        public SendAnswersToTextProcessorsEvent(Exam exam)
        {
            this.Exam = exam;
        }

        public Exam Exam { get; private set; }
    }
}