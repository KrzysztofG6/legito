﻿namespace Legito.ProcessorCoordinator.Api.Domain.PostProcessing
{
    using System.Collections.Generic;
    using Core.Dto;
    using Core.Enums;
    using Core.PostProcessing;

    public class PostProcessingStrategyContext : IPostProcessingStrategyContext
    {
        private readonly Dictionary<ProcessorType, IPostProcessingStrategy> _strategies;

        public PostProcessingStrategyContext(Dictionary<ProcessorType, IPostProcessingStrategy> strategies)
        {
            _strategies = strategies;
        }

        public void Execute(PostProcessingDto model)
        {
            _strategies[model.ProcessorType].ProcessResults(model);
        }
    }
}