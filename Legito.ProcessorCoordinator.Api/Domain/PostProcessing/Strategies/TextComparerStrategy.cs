﻿namespace Legito.ProcessorCoordinator.Api.Domain.PostProcessing.Strategies
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Core.Database;
    using Core.Dto;
    using Core.Entities;
    using Core.PostProcessing;

    public class TextComparerStrategy : IPostProcessingStrategy
    {
        private readonly IProcessorCoordinatorContext _processorCoordinatorContext;
        private readonly IMapper _mapper;
        

        public TextComparerStrategy(IProcessorCoordinatorContext context,
            IMapper mapper)
        {
            _processorCoordinatorContext = context;
            _mapper = mapper;
        }

        public void ProcessResults(PostProcessingDto model)
        {
            var examStatus = _processorCoordinatorContext.ExamProcessingStatuses.FirstOrDefault(x => x.ExamId == model.ExamId);

            if (examStatus == null)
            {
                throw new NullReferenceException($"Exam with id {model.ExamId} was not found.");
            }

            examStatus.IsTextComparisonCompleted = true;

            if (!model.IsCheatingFound)
            {
                _processorCoordinatorContext.SaveChanges();
                return;
            }

            var processingResults = new List<ProcessingResult>();

            foreach (var group in model.ProcessingResults)
            {
                foreach (var result in group)
                {
                    var mappedResult = _mapper.Map<ProcessingResult>(result);

                    var duplicates = result.DuplicateAnswerIds.Select(id => new DuplicateAnswer
                    {
                        Id = id,
                        ProcessingResult = mappedResult
                    }).ToList();

                    mappedResult.DuplicateAnswers = duplicates;
                    mappedResult.ExamId = model.ExamId;

                    processingResults.Add(mappedResult);
                }
            }

            _processorCoordinatorContext.ProcessingResults.AddRange(processingResults);
            _processorCoordinatorContext.SaveChanges();
        }
    }
}