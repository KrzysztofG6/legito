﻿namespace Legito.ProcessorCoordinator.Api.Domain.PostProcessing.Strategies
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Core.Database;
    using Core.Dto;
    using Core.Entities;
    using Core.PostProcessing;

    public class WikipediaScannerStrategy : IPostProcessingStrategy
    {
        private readonly IProcessorCoordinatorContext _processorCoordinatorContext;
        private readonly IMapper _mapper;

        public WikipediaScannerStrategy(IProcessorCoordinatorContext processorCoordinatorContext,
            IMapper mapper)
        {
            _processorCoordinatorContext = processorCoordinatorContext;
            _mapper = mapper;
        }

        public void ProcessResults(PostProcessingDto model)
        {
            var examStatus = _processorCoordinatorContext.ExamProcessingStatuses.FirstOrDefault(x => x.ExamId == model.ExamId);

            if (examStatus == null)
            {
                throw new NullReferenceException($"Exam with id {model.ExamId} was not found.");
            }

            examStatus.IsWikipediaScanCompleted = true;

            if (!model.IsCheatingFound)
            {
                _processorCoordinatorContext.SaveChanges();
                return;
            }

            var processingResultsMapped = model.WikipediaAnalyzeResults.Select(wikipediaAnalyzeResult => _mapper.Map<WikipediaProcessingResult>(wikipediaAnalyzeResult)).ToList();

            foreach (var wikipediaProcessingResult in processingResultsMapped)
            {
                wikipediaProcessingResult.ExamId = model.ExamId;
            }

            _processorCoordinatorContext.WikipediaProcessingResults.AddRange(processingResultsMapped);
            _processorCoordinatorContext.SaveChanges();
        }
    }
}