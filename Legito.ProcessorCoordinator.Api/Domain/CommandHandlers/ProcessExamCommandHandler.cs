﻿namespace Legito.ProcessorCoordinator.Api.Domain.CommandHandlers
{
    using System.Linq;
    using Commands;
    using Core.Enums;
    using Core.Models;
    using Events;
    using KSolution.Cqrs.Core;
    using Queries;

    public class ProcessExamCommandHandler : ICommandHandler<ProcessExamCommand>
    {
        private readonly IMediator _mediator;

        public ProcessExamCommandHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public void Handle(ProcessExamCommand command)
        {
            var getExamsQuery = new GetExamDataQuery(command.ExamId);
            var exams = this._mediator.Fetch<GetExamDataQuery, Exam>(getExamsQuery);
            
            var textProcessingEvent = new SendAnswersToTextProcessorsEvent(new Exam(exams.ExamId, exams.Answers.Where(x => x.Type == AnswerType.Text).ToList()));
            _mediator.Publish(textProcessingEvent);
        }
    }
}