﻿namespace Legito.ProcessorCoordinator.Api.Domain.CommandHandlers
{
    using System.Linq;
    using Commands;
    using KSolution.Cqrs.Core;

    public class CheckExamProcessingStatusCommandHandler : ICommandHandler<CheckExamProcessingStatusCommand>
    {
        private readonly IMediator _mediator;

        public CheckExamProcessingStatusCommandHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public void Handle(CheckExamProcessingStatusCommand command)
        {
            var boolFields = command.ExamProcessingStatus.GetType().GetProperties().Where(x => x.PropertyType == typeof(bool));

            foreach (var processingStatus in boolFields)
            {
                var result = processingStatus.GetValue(command.ExamProcessingStatus, null);

                bool.TryParse(result.ToString(), out var isProcessingFinished);

                if (!isProcessingFinished)
                {
                    return;
                }
            }

            _mediator.Send(new SendProcessingResultsToBackOfficeCommand(command.ExamProcessingStatus.ExamId));

            _mediator.Send(new SetFinishedProcessingByExamIdCommand(command.ExamProcessingStatus.ExamId));
        }
    }
}