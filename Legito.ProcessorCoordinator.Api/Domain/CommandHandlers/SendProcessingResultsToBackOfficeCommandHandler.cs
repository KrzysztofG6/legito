﻿namespace Legito.ProcessorCoordinator.Api.Domain.CommandHandlers
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;
    using AutoMapper;
    using Commands;
    using Core.Dto;
    using Core.Entities;
    using KSolution.Cqrs.Core;
    using KSolution.ServiceDiscovery.Core.Enum;
    using KSolution.ServiceDiscovery.Core.Services;
    using Newtonsoft.Json;
    using Queries;

    public class SendProcessingResultsToBackOfficeCommandHandler : ICommandHandler<SendProcessingResultsToBackOfficeCommand>
    {
        private readonly IMediator _mediator;
        private readonly HttpClient _httpClient;
        private readonly IConsulService _consulService;
        private readonly IMapper _mapper;
        private readonly ITokenProviderService _tokenProviderService;

        public SendProcessingResultsToBackOfficeCommandHandler(
            IMediator mediator,
            HttpClient httpClient,
            IConsulService consulService,
            IMapper mapper,
            ITokenProviderService tokenProviderService)
        {
            _mediator = mediator;
            _httpClient = httpClient;
            _consulService = consulService;
            _mapper = mapper;
            _tokenProviderService = tokenProviderService;
        }

        public void Handle(SendProcessingResultsToBackOfficeCommand command)
        {
            var textComparerResults = _mediator.Fetch<GetProcessingResultsByExamIdQuery, IList<ProcessingResult>>(new GetProcessingResultsByExamIdQuery(command.ExamId));
            var wikipediaProcessingResult = _mediator.Fetch<GetWikipediaProcessingResultsQuery, List<WikipediaProcessingResult>>(new GetWikipediaProcessingResultsQuery(command.ExamId));

            var dto = new ProcessingResultDto
            {
                TextComparerResults = _mapper.Map<List<TextComparerResultsDto>>(textComparerResults),
                WikipediaResults = _mapper.Map<List<WikipediaProcessingResultDto>>(wikipediaProcessingResult)
            };

            _httpClient.SetBearerToken(_tokenProviderService.GetTokenForApi(ServicesLookup.ProcessCoordinatorApi, ServicesLookup.BackofficeApi).Result);
            var response = _httpClient.PostAsync($"http://{_consulService.GetServiceAddress(ServicesLookup.BackofficeApi)}/api/Processing/Result", new StringContent(JsonConvert.SerializeObject(dto), Encoding.UTF8, "application/json")).Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Exam data has not been recived by backoffice API");
            }
        }
    }
}