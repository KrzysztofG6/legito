﻿namespace Legito.ProcessorCoordinator.Api.Domain.CommandHandlers
{
    using Commands;
    using Core.Database;
    using Core.Entities;
    using KSolution.Cqrs.Core;

    public class CreateExamProcessingStatusCommandHandler : ICommandHandler<CreateExamProcessingStatusCommand>
    {
        private readonly IProcessorCoordinatorContext _processorCoordinatorContext;

        public CreateExamProcessingStatusCommandHandler(IProcessorCoordinatorContext processorCoordinatorContext)
        {
            this._processorCoordinatorContext = processorCoordinatorContext;
        }

        public void Handle(CreateExamProcessingStatusCommand command)
        {
            _processorCoordinatorContext.ExamProcessingStatuses.Add(new ExamProcessingStatus(command.ExamId));
            _processorCoordinatorContext.SaveChanges();
        }
    }
}