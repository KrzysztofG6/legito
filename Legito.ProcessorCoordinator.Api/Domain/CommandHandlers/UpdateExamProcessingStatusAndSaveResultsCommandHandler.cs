﻿namespace Legito.ProcessorCoordinator.Api.Domain.CommandHandlers
{
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Commands;
    using Core.Database;
    using Core.Entities;
    using Core.PostProcessing;
    using KSolution.Cqrs.Core;

    public class UpdateExamProcessingStatusAndSaveResultsCommandHandler : ICommandHandler<UpdateExamProcessingStatusAndSaveResultsCommand>
    {
        private readonly IPostProcessingStrategyContext _postProcessingStrategyContext;
        private readonly IMapper _mapper;
        private readonly IProcessorCoordinatorContext _processorCoordinatorContext;

        public UpdateExamProcessingStatusAndSaveResultsCommandHandler(
            IPostProcessingStrategyContext postProcessingStrategyContext,
            IMapper mapper,
            IProcessorCoordinatorContext processorCoordinatorContext)
        {
            _postProcessingStrategyContext = postProcessingStrategyContext;
            _mapper = mapper;
            _processorCoordinatorContext = processorCoordinatorContext;
        }

        public void Handle(UpdateExamProcessingStatusAndSaveResultsCommand command)
        {
            _postProcessingStrategyContext.Execute(command.PostProcessing);
        }
    }
}