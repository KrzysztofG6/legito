﻿namespace Legito.ProcessorCoordinator.Api.Domain.CommandHandlers
{
    using System.Net.Http;
    using System.Text;
    using Commands;
    using KSolution.Cqrs.Core;
    using KSolution.ServiceDiscovery.Core.Enum;
    using KSolution.ServiceDiscovery.Core.Services;
    using Newtonsoft.Json;

    public class SetFinishedProcessingByExamIdCommandHandler : ICommandHandler<SetFinishedProcessingByExamIdCommand>
    {
        private readonly HttpClient _httpClient;
        private readonly IConsulService _consulService;
        private readonly ITokenProviderService _tokenProviderService;

        public SetFinishedProcessingByExamIdCommandHandler(HttpClient httpClient,
            IConsulService consulService, 
            ITokenProviderService tokenProviderService)
        {
            _httpClient = httpClient;
            _consulService = consulService;
            _tokenProviderService = tokenProviderService;
        }

        public void Handle(SetFinishedProcessingByExamIdCommand command)
        {
            var postContent = JsonConvert.SerializeObject(command);

            _httpClient.SetBearerToken(_tokenProviderService.GetTokenForApi(ServicesLookup.BackofficeApi, ServicesLookup.ClientApi).Result);
            var response = _httpClient.PutAsync($"http://{_consulService.GetServiceAddress(ServicesLookup.ClientApi)}/api/Exam/SetEndOfProcessing", new StringContent(postContent, Encoding.UTF8, "application/json")).Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"Request to client api failed on setting processing finish in exam {command.ExamId} respone: {response}");
            }
        }
    }
}