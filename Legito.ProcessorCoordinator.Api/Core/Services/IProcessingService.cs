﻿namespace Legito.ProcessorCoordinator.Api.Core.Services
{
    using System;

    public interface IProcessingService
    {
        void ProcessExam(Guid examId);
    }
}