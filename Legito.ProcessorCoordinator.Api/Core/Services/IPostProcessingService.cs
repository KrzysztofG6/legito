﻿namespace Legito.ProcessorCoordinator.Api.Core.Services
{
    using Dto;

    public interface IPostProcessingService
    {
        void ProcessExamResults(PostProcessingDto results);
    }
}