﻿namespace Legito.ProcessorCoordinator.Api.Core.Services
{
    public interface IHealthCheckService
    {
        bool IsDatabaseConnectionEstablished();
    }
}