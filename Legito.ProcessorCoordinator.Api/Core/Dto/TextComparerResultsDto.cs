﻿namespace Legito.ProcessorCoordinator.Api.Core.Dto
{
    using System;
    using System.Collections.Generic;

    public class TextComparerResultsDto
    {
        public Guid ExamId { get; set; }

        public Guid StudentId { get; set; }

        public Guid QuestionId { get; set; }

        public ICollection<Guid> DuplicateAnswers { get; set; }
    }
}