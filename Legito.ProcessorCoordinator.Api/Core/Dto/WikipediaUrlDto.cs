﻿namespace Legito.ProcessorCoordinator.Api.Core.Dto
{
    using System.Collections.Generic;

    public class WikipediaUrlDto
    {
        public WikipediaUrlDto()
        {
            Phrases = new List<string>();
        }

        public string Url { get; set; }

        public double Percentage { get; set; }

        public List<string> Phrases { get; set; }

        public string WikiTextNormalized { get; set; }
    }
}