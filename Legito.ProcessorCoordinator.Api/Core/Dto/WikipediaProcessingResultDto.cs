﻿namespace Legito.ProcessorCoordinator.Api.Core.Dto
{
    using System;
    using System.Collections.Generic;
    using Models;

    public class WikipediaProcessingResultDto
    {
        public WikipediaProcessingResultDto()
        {
            this.UrlsWithScore = new List<WikipediaUrlDto>();
        }

        public Guid Id { get; set; }

        public Guid ExamId { get; set; }

        public Guid AnswerId { get; set; }

        public Guid StudentId { get; set; }

        public List<WikipediaUrlDto> UrlsWithScore { get; set; }
    }
}