﻿namespace Legito.ProcessorCoordinator.Api.Core.Dto
{
    using System;
    using System.Collections.Generic;
    using Enums;
    using Models;

    public class PostProcessingDto
    {
        public Guid ExamId { get; set; }

        public ProcessorType ProcessorType { get; set; }

        public bool IsCheatingFound { get; set; }

        public IEnumerable<List<TextAnswerComparison>> ProcessingResults { get; set; }

        public IEnumerable<WikipediaAnalyzeResult> WikipediaAnalyzeResults { get; set; }
    }
}