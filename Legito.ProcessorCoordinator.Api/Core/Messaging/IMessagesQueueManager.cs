﻿namespace Legito.ProcessorCoordinator.Api.Core.Messaging
{
    public interface IMessagesQueueManager<in T>
    {
        void Publish(string queueName, T answers);
    }
}