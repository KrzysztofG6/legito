﻿namespace Legito.ProcessorCoordinator.Api.Core.Models
{
    using System;
    using Enums;

    public class Answer
    {
        public Guid Id { get; set; }

        public Guid StudentId { get; set; }

        public Guid QuestionId { get; set; }

        public string StudentAnswer { get; set; }

        public AnswerType Type { get; set; }
    }
}