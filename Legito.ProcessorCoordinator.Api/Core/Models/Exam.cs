﻿namespace Legito.ProcessorCoordinator.Api.Core.Models
{
    using System;
    using System.Collections.Generic;

    public class Exam
    {
        public Exam(
            Guid examId,
            List<Answer> answers)
        {
            this.ExamId = examId;
            this.Answers = answers;
        }

        public Guid ExamId { get; private set; }

        public List<Answer> Answers { get; private set; }
    }
}