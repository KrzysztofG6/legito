﻿namespace Legito.ProcessorCoordinator.Api.Core.Models
{
    using System;

    public class StudentAnswer
    {
        public Guid Id { get; set; }

        public Guid StudentId { get; set; }
        
        public Guid QuestionId { get; set; }

        public string Answer { get; set; }
    }
}