﻿namespace Legito.ProcessorCoordinator.Api.Core.Models
{
    using System;
    using System.Collections.Generic;

    public class TextAnswerComparison
    {
        public StudentAnswer SearchAgainst { get; set; }

        public List<Guid> DuplicateAnswerIds { get; set; }
    }
}