﻿namespace Legito.ProcessorCoordinator.Api.Core.Models
{
    using System;
    using System.Collections.Generic;
    using Dto;

    public class WikipediaAnalyzeResult
    {
        public Guid AnswerId { get; set; }

        public Guid StudentId { get; set; }

        public List<WikipediaUrlDto> UrlsWithScore { get; set; }
    }
}