﻿namespace Legito.ProcessorCoordinator.Api.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class WikipdiaUrlWithScore
    {
        [Key]
        public Guid Id { get; set; }

        public string Url { get; set; }

        public double Percentge { get; set; }

        public string WikiTextNormalized { get; set; }

        public string Phrases { get; set; }

        public WikipediaProcessingResult WikipediaProcessingResult { get; set; }
    }
}