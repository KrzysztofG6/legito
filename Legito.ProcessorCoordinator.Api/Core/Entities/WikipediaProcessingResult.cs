﻿namespace Legito.ProcessorCoordinator.Api.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class WikipediaProcessingResult
    {
        [Key]
        public Guid Id { get; set; }

        public Guid ExamId { get; set; }

        public Guid AnswerId { get; set; }

        public Guid StudentId { get; set; }

        public List<WikipdiaUrlWithScore> UrlsWithScore { get; set; }
    }
}