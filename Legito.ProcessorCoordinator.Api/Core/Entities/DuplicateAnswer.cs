﻿namespace Legito.ProcessorCoordinator.Api.Core.Entities
{
    using System;

    public class DuplicateAnswer
    {
        public Guid Id { get; set; }

        public ProcessingResult ProcessingResult { get; set; }
    }
}