﻿namespace Legito.ProcessorCoordinator.Api.Core.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class ExamProcessingStatus
    {
        public ExamProcessingStatus(Guid examId)
        {
            this.ExamId = examId;
            this.IsTextComparisonCompleted = false;
            this.IsWikipediaScanCompleted = false;
        }

        public ExamProcessingStatus()
        {
        }

        [Key]
        public Guid ExamId { get; set; }

        public bool IsTextComparisonCompleted { get; set; }

        public bool IsWikipediaScanCompleted { get; set; }
    }
}