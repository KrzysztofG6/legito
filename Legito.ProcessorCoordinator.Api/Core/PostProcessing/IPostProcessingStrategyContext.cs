﻿namespace Legito.ProcessorCoordinator.Api.Core.PostProcessing
{
    using Dto;

    public interface IPostProcessingStrategyContext
    {
        void Execute(PostProcessingDto model);
    }
}