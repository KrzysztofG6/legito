﻿namespace Legito.ProcessorCoordinator.Api.Core.PostProcessing
{
    using Dto;

    public interface IPostProcessingStrategy
    {
        void ProcessResults(PostProcessingDto model);
    }
}