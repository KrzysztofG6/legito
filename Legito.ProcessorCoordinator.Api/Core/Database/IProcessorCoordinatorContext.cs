﻿namespace Legito.ProcessorCoordinator.Api.Core.Database
{
    using System.Threading;
    using System.Threading.Tasks;
    using Entities;
    using Microsoft.EntityFrameworkCore;

    public interface IProcessorCoordinatorContext
    {
        DbSet<ExamProcessingStatus> ExamProcessingStatuses { get; set; }

        DbSet<ProcessingResult> ProcessingResults { get; set; }

        DbSet<DuplicateAnswer> DuplicateAnswers { get; set; }

        DbSet<WikipediaProcessingResult> WikipediaProcessingResults { get; set; }

        DbSet<WikipdiaUrlWithScore> WikipdiaUrlsWithScore { get; set; }

        int SaveChanges();
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        bool IsDatabaseReachable();
    }
}