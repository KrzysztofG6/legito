﻿namespace Legito.ProcessorCoordinator.Api.Core.Enums
{
    public enum ProcessorType
    {
        TextComparer = 1,
        WikipediaScaner = 2
    }
}