﻿namespace Legito.ProcessorCoordinator.Api.Core.Enums
{
    public enum AnswerType
    {
        Text = 1,
        Closed = 2
    }
}