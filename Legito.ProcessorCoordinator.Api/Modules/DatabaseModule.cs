﻿namespace Legito.ProcessorCoordinator.Api.Modules
{
    using Autofac;
    using Domain.Database;
    using KSolution.ServiceDiscovery.Core.Constants;
    using Microsoft.EntityFrameworkCore;

    public class DatabaseModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            var contextOptionsBuilder = new DbContextOptionsBuilder<ProcessorCoordinatorContext>();
            contextOptionsBuilder.UseSqlServer("Server=" + ServiceDiscoveryConstants.DockerHostMachineIpAddress + "\\SQL2017;Database=ProcessorCoordinator;User Id=Dev;Password=dev12345!;");

            builder.Register(x => new ProcessorCoordinatorContext(contextOptionsBuilder.Options))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}