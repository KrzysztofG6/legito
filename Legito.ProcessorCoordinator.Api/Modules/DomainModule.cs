﻿namespace Legito.ProcessorCoordinator.Api.Modules
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using Autofac;
    using AutoMapper;
    using Core.Enums;
    using Core.Messaging;
    using Core.PostProcessing;
    using DnsClient;
    using Domain.Mapper;
    using Domain.Messaging;
    using Domain.PostProcessing;
    using Domain.PostProcessing.Strategies;
    using KSolution.ServiceDiscovery.Core.Constants;
    using KSolution.ServiceDiscovery.Services;
    using Microsoft.Extensions.Configuration;
    using Serilog;

    public class DomainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            builder.RegisterGeneric(typeof(MessagesQueueManager<>))
                .As(typeof(IMessagesQueueManager<>))
                .InstancePerDependency();
            
            builder.RegisterAssemblyTypes(ThisAssembly)
                .Where(x => x.IsAssignableTo<IPostProcessingStrategy>())
                .AsSelf();

            builder.Register(x => new Dictionary<ProcessorType, IPostProcessingStrategy>
                    {
                        {ProcessorType.TextComparer, x.Resolve<TextComparerStrategy>()},
                        {ProcessorType.WikipediaScaner, x.Resolve<WikipediaScannerStrategy>()}
                    })
                .As(typeof(Dictionary<ProcessorType, IPostProcessingStrategy>))
                .SingleInstance();

            builder.RegisterType<PostProcessingStrategyContext>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.Register(x => new MapperFactory().Create())
                .As<IMapper>()
                .SingleInstance();

            builder.RegisterType<HttpClient>()
                .AsSelf()
                .SingleInstance();

            builder.Register(x => new LookupClient(IPAddress.Parse(ServiceDiscoveryConstants.DockerHostMachineIpAddress), 8600)
                    {
                        EnableAuditTrail = false,
                        UseCache = true,
                        MinimumCacheTimeout = TimeSpan.FromSeconds(1)
                    })
                .As<IDnsQuery>()
                .SingleInstance();

            builder.RegisterType<ConsulService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.Register(x => new LoggerConfiguration()
                    .MinimumLevel.Warning()
                    .ReadFrom.Configuration(configuration).CreateLogger())
                .As<ILogger>()
                .InstancePerLifetimeScope();
        }
    }
}