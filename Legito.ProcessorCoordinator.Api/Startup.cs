﻿namespace Legito.ProcessorCoordinator.Api
{
    using System;
    using Autofac;
    using Autofac.Extensions.DependencyInjection;
    using Domain.Database;
    using KSolution.Cqrs.Modules;
    using KSolution.ServiceDiscovery.Core.Enum;
    using KSolution.ServiceDiscovery.Core.Extensions;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Middleware;
    using Modules;
    using Serilog;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(opt =>
                {
                    opt.Authority = "http://localhost:5000";
                    opt.RequireHttpsMetadata = false;

                    opt.ApiName = ServicesLookup.ProcessCoordinatorApi.Description();
                });

            services.AddDbContext<ProcessorCoordinatorContext>(options =>
                options.UseSqlServer(this.Configuration.GetConnectionString("ProcessorCoordinator")));

            services.AddServiceDiscovery(Configuration.GetSection("ServiceDiscovery"));

            services.AddTransient<ILogger>(x => new LoggerConfiguration()
                .MinimumLevel.Warning()
                .ReadFrom.Configuration(Configuration).CreateLogger());

            var containerBuilder = new ContainerBuilder();
            containerBuilder.Populate(services);

            containerBuilder.RegisterModule<CqrsModule>();
            containerBuilder.RegisterModule<DatabaseModule>();
            containerBuilder.RegisterModule<DomainModule>();
            containerBuilder.RegisterModule<ServicesModule>();

            var container = containerBuilder.Build();
            return new AutofacServiceProvider(container);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            app.UseMiddleware(typeof(ErrorHandlingMiddleware));
            app.UseConsulServiceRegistration();

            app.UseMvc();
        }
    }
}
