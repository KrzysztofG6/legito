﻿namespace Legito.ProcessorCoordinator.Api.Services
{
    using Core.Database;
    using Core.Services;
    public class HealthCheckService : IHealthCheckService
    {
        private readonly IProcessorCoordinatorContext _processorCoordinatorContext;

        public HealthCheckService(IProcessorCoordinatorContext processorCoordinatorContext)
        {
            _processorCoordinatorContext = processorCoordinatorContext;
        }

        public bool IsDatabaseConnectionEstablished()
        {
            return _processorCoordinatorContext.IsDatabaseReachable();
        }
    }
}