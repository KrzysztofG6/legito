﻿namespace Legito.ProcessorCoordinator.Api.Services
{
    using System;
    using Core.Services;
    using Domain.Commands;
    using KSolution.Cqrs.Core;

    public class ProcessingService : IProcessingService
    {
        private readonly IMediator _mediator;

        public ProcessingService(IMediator mediator)
        {
            _mediator = mediator;
        }

        public void ProcessExam(Guid examId)
        {
            var createStatusCommand = new CreateExamProcessingStatusCommand(examId);
            _mediator.Send(createStatusCommand);

            var processCommand = new ProcessExamCommand(examId);
            _mediator.Send(processCommand);
        }
    }
}