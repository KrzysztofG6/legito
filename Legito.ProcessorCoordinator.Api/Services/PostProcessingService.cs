﻿namespace Legito.ProcessorCoordinator.Api.Services
{
    using Core.Dto;
    using Core.Entities;
    using Core.Services;
    using Domain.Commands;
    using Domain.Queries;
    using KSolution.Cqrs.Core;

    public class PostProcessingService : IPostProcessingService
    {
        private readonly IMediator _mediator;

        public PostProcessingService(IMediator mediator)
        {
            _mediator = mediator;
        }

        public void ProcessExamResults(PostProcessingDto results)
        {
            _mediator.Send(new UpdateExamProcessingStatusAndSaveResultsCommand(results));

            var examProcessingStatus = _mediator.Fetch<GetExamProcessingStatusByExamIdQuery, ExamProcessingStatus>(new GetExamProcessingStatusByExamIdQuery(results.ExamId));

            _mediator.Send(new CheckExamProcessingStatusCommand(examProcessingStatus));
        }
    }
}