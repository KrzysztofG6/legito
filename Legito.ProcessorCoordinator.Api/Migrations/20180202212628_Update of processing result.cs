﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Legito.ProcessorCoordinator.Api.Migrations
{
    public partial class Updateofprocessingresult : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ExamId",
                schema: "pca",
                table: "ProcessingResults",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExamId",
                schema: "pca",
                table: "ProcessingResults");
        }
    }
}
