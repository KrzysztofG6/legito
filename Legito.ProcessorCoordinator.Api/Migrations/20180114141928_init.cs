﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Legito.ProcessorCoordinator.Api.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "pca");

            migrationBuilder.CreateTable(
                name: "ExamProcessingStatus",
                schema: "pca",
                columns: table => new
                {
                    ExamId = table.Column<Guid>(nullable: false),
                    IsTextComparisonCompleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamProcessingStatus", x => x.ExamId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExamProcessingStatus",
                schema: "pca");
        }
    }
}
