﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Legito.ProcessorCoordinator.Api.Migrations
{
    public partial class Wikipediaurlswithscores : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SimilarUrlsEncoded",
                schema: "pca",
                table: "WikipediaProcessingResults");

            migrationBuilder.CreateTable(
                name: "WikipdiaUrlsWithScore",
                schema: "pca",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Percentge = table.Column<double>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    WikipediaProcessingResultId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WikipdiaUrlsWithScore", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WikipdiaUrlsWithScore_WikipediaProcessingResults_WikipediaProcessingResultId",
                        column: x => x.WikipediaProcessingResultId,
                        principalSchema: "pca",
                        principalTable: "WikipediaProcessingResults",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WikipdiaUrlsWithScore_WikipediaProcessingResultId",
                schema: "pca",
                table: "WikipdiaUrlsWithScore",
                column: "WikipediaProcessingResultId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WikipdiaUrlsWithScore",
                schema: "pca");

            migrationBuilder.AddColumn<string>(
                name: "SimilarUrlsEncoded",
                schema: "pca",
                table: "WikipediaProcessingResults",
                nullable: true);
        }
    }
}
