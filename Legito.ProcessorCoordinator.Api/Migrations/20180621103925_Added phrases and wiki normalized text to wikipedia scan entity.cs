﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Legito.ProcessorCoordinator.Api.Migrations
{
    public partial class Addedphrasesandwikinormalizedtexttowikipediascanentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Phrases",
                schema: "pca",
                table: "WikipdiaUrlsWithScore",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WikiTextNormalized",
                schema: "pca",
                table: "WikipdiaUrlsWithScore",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Phrases",
                schema: "pca",
                table: "WikipdiaUrlsWithScore");

            migrationBuilder.DropColumn(
                name: "WikiTextNormalized",
                schema: "pca",
                table: "WikipdiaUrlsWithScore");
        }
    }
}
