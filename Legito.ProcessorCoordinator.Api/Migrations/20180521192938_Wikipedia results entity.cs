﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Legito.ProcessorCoordinator.Api.Migrations
{
    public partial class Wikipediaresultsentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsWikipediaScanCompleted",
                schema: "pca",
                table: "ExamProcessingStatus",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "WikipediaProcessingResults",
                schema: "pca",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AnswerId = table.Column<Guid>(nullable: false),
                    ExamId = table.Column<Guid>(nullable: false),
                    SimilarUrlsEncoded = table.Column<string>(nullable: true),
                    StudentId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WikipediaProcessingResults", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WikipediaProcessingResults",
                schema: "pca");

            migrationBuilder.DropColumn(
                name: "IsWikipediaScanCompleted",
                schema: "pca",
                table: "ExamProcessingStatus");
        }
    }
}
