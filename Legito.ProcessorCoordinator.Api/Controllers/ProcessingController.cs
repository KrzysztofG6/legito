﻿namespace Legito.ProcessorCoordinator.Api.Controllers
{
    using System;
    using Core.Services;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Produces("application/json")]
    [Route("api/Processing")]
    [Authorize]
    public class ProcessingController : Controller
    {
        private readonly IProcessingService _processingService;

        public ProcessingController(IProcessingService processingService)
        {
            _processingService = processingService;
        }

        [HttpGet("Exam/{examId}")]
        public IActionResult Exam(Guid examId)
        {
            _processingService.ProcessExam(examId);

            return Ok();
        }
    }
}