﻿namespace Legito.ProcessorCoordinator.Api.Controllers
{
    using System.Reflection;
    using Core.Services;
    using Microsoft.AspNetCore.Mvc;
    using Serilog;

    [Produces("application/json")]
    [Route("api/HealthCheck")]
    public class HealthCheckController : Controller
    {
        private readonly IHealthCheckService _healthCheckService;
        private readonly ILogger _logger;

        public HealthCheckController(
            IHealthCheckService healthCheckService,
            ILogger logger)
        {
            _healthCheckService = healthCheckService;
            _logger = logger;
        }

        [HttpGet("Ping")]
        public IActionResult Ping()
        {
            var isDatabaseConnectionAlive = _healthCheckService.IsDatabaseConnectionEstablished();

            if (!isDatabaseConnectionAlive)
            {
                _logger.Fatal($"Database of {Assembly.GetExecutingAssembly().FullName} is dead!!");
                return BadRequest("Database is dead");
            }

            return Ok("Pong");
        }
    }
}