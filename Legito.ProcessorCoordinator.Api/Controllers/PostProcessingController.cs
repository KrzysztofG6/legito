﻿namespace Legito.ProcessorCoordinator.Api.Controllers
{
    using Core.Dto;
    using Core.Services;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Produces("application/json")]
    [Route("api/PostProcessing")]
    public class PostProcessingController : Controller
    {
        private readonly IPostProcessingService _postProcessingService;

        public PostProcessingController(IPostProcessingService postProcessingService)
        {
            _postProcessingService = postProcessingService;
        }

        [Authorize]
        [HttpPost("Answers")]
        public ActionResult Answers([FromBody]PostProcessingDto model)
        {
            _postProcessingService.ProcessExamResults(model);
            
            return Ok();
        }
    }
}