﻿namespace Legito.TextComparer.Core
{
    public interface ISimilarityComparer
    {
        double CompareStrings(string str1, string str2);
    }
}