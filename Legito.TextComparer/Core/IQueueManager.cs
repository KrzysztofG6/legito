﻿namespace Legito.TextComparer.Core
{
    public interface IQueueManager
    {
        void Listen();
    }
}