﻿namespace Legito.TextComparer.Core.Model
{
    using System;
    using System.Collections.Generic;

    public class AnswerComparison
    {
        public AnswerComparison()
        {
            DuplicateAnswerIds = new List<Guid>();
        }

        public Answer SearchAgainst { get; set; }

        public List<Guid> DuplicateAnswerIds { get; set; }
    }
}