﻿namespace Legito.TextComparer.Core.Model
{
    using System;

    public class Answer
    {
        public Guid Id { get; set; }

        public Guid StudentId { get; set; }
        
        public Guid QuestionId { get; set; }

        public string StudentAnswer { get; set; }
    }
}