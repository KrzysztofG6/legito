﻿namespace Legito.TextComparer.Core
{
    using System.Collections.Generic;
    using Model;

    public interface IProcessorService
    {
        IEnumerable<List<AnswerComparison>> ProcessText(StudentPapers papers);
    }
}