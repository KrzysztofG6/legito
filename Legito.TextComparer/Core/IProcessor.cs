﻿namespace Legito.TextComparer.Core
{
    using System.Collections.Generic;
    using Model;

    public interface IProcessor
    {
        IEnumerable<List<AnswerComparison>> ProcessAnswers(StudentPapers papers);
    }
}