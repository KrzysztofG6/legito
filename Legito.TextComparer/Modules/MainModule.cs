﻿namespace Legito.TextComparer.Modules
{
    using System;
    using System.Net;
    using System.Net.Http;
    using Autofac;
    using DnsClient;
    using Domain;
    using Messaging;
    using Services;

    public class MainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<SimilarityComparer>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<TextProcessor>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<ProcessorService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<QueueManager>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<HttpClient>()
                .AsSelf()
                .SingleInstance();
            
            builder.Register(x => new LookupClient(IPAddress.Parse("192.168.65.2"), 8600))
                .As<IDnsQuery>()
                .SingleInstance();
        }
    }
}