﻿namespace Legito.TextComparer.Domain
{
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using Core;

    public class SimilarityComparer : ISimilarityComparer
    {
        public double CompareStrings(string str1, string str2)
        {
            var pairs1 = WordLetterPairs(str1.ToUpper());
            var pairs2 = WordLetterPairs(str2.ToUpper());

            int intersection = 0;
            int union = pairs1.Count + pairs2.Count;

            foreach (var pair in pairs1)
            {
                for (var j = 0; j < pairs2.Count; j++)
                {
                    if (pair == pairs2[j])
                    {
                        intersection++;
                        pairs2.RemoveAt(j);

                        break;
                    }
                }
            }

            return (2.0 * intersection) / union;
        }
        
        private List<string> WordLetterPairs(string str)
        {
            var allPairs = new List<string>();

            var words = Regex.Split(str, @"\s");

            foreach (var word in words)
            {
                if (string.IsNullOrEmpty(word)) continue;
                var pairsInWord = LetterPairs(word);

                allPairs.AddRange(pairsInWord);
            }

            return allPairs;
        }

        private string[] LetterPairs(string str)
        {
            var numPairs = str.Length - 1;

            var pairs = new string[numPairs];

            for (var i = 0; i < numPairs; i++)
            {
                pairs[i] = str.Substring(i, 2);
            }

            return pairs;
        }
    }
}