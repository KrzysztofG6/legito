﻿namespace Legito.TextComparer.Domain
{
    using System.Collections.Generic;
    using System.Linq;
    using Core;
    using Core.Model;

    public class TextProcessor : IProcessor
    {
        private readonly ISimilarityComparer _similarityComparer;

        public TextProcessor(ISimilarityComparer similarityComparer)
        {
            this._similarityComparer = similarityComparer;
        }

        public IEnumerable<List<AnswerComparison>> ProcessAnswers(StudentPapers papers)
        {
            var groupedAnswers = GroupAnswersByQuestions(papers);

            return ValidateAnswers(groupedAnswers);
        }

        private IEnumerable<List<AnswerComparison>> ValidateAnswers(IEnumerable<List<Answer>> groupedAnswers)
        {
            var results = new List<List<AnswerComparison>>();

            foreach (var group in groupedAnswers)
            {
                var comparisonCheckList = new List<AnswerComparison>();

                foreach (var verifyAnswer in group)
                {
                    if (!comparisonCheckList.Any())
                    {
                        comparisonCheckList.Add(new AnswerComparison
                        {
                            SearchAgainst = verifyAnswer
                        });

                        continue;
                    }

                    var isDuplicateFound = false;

                    foreach (var comparisonAnswer in comparisonCheckList)
                    {
                        var comparisonScore = _similarityComparer.CompareStrings(verifyAnswer.StudentAnswer, comparisonAnswer.SearchAgainst.StudentAnswer);

                        if (comparisonScore > 0.75)
                        {
                            comparisonAnswer.DuplicateAnswerIds.Add(verifyAnswer.Id);
                            isDuplicateFound = true;
                            break;
                        }
                    }

                    if (!isDuplicateFound)
                    {
                        comparisonCheckList.Add(new AnswerComparison
                        {
                            SearchAgainst = verifyAnswer
                        });
                    }
                }
                // do not add collection if it has no copies in it
                comparisonCheckList.RemoveAll(x => !x.DuplicateAnswerIds.Any());

                if (comparisonCheckList.Count != 0)
                {
                    results.Add(comparisonCheckList);
                    
                }
            }

            // filter out answers that do not have copies
            return results.Select(x => x.Where(y => y.DuplicateAnswerIds.Any()).ToList()) ;
        }

        private IEnumerable<List<Answer>> GroupAnswersByQuestions(StudentPapers papers)
        {
            var questionIds = papers.Answers.Select(x => x.QuestionId).Distinct();

            var questionAnswers = new List<List<Answer>>();

            foreach (var questionId in questionIds)
            {
                var answers = papers.Answers.Where(x => x.QuestionId == questionId);

                questionAnswers.Add(answers.ToList());
            }
            return questionAnswers;
        }
    }
}