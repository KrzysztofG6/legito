﻿namespace Legito.TextComparer.Services
{
    using System.Collections.Generic;
    using Core;
    using Core.Model;

    public class ProcessorService : IProcessorService
    {
        private readonly IProcessor processor;

        public ProcessorService(IProcessor processor)
        {
            this.processor = processor;
        }

        public IEnumerable<List<AnswerComparison>> ProcessText(StudentPapers papers)
        {
            return this.processor.ProcessAnswers(papers);
        }
    }
}