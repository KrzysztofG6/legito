﻿namespace Legito.TextComparer
{
    using Autofac;
    using Core;
    using Modules;

    class Program
    {
        static void Main(string[] args)
        {
            var container = BuildContainer();

            using (var scope = container.BeginLifetimeScope())
            {
                var queueListener = scope.Resolve<IQueueManager>();

                queueListener.Listen();
            }
        }

        private static IContainer BuildContainer()
        {
            var containerbuilder = new ContainerBuilder();
            containerbuilder.RegisterModule<MainModule>();
            return containerbuilder.Build();
        }
    }
}
